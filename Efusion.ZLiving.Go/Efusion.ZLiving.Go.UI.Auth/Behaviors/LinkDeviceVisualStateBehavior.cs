﻿using Efusion.ZLiving.Go.UI.Auth.States;
using Efusion.ZLiving.Go.UI.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Auth.Behaviors
{
    public class LinkDeviceVisualStateBehavior : Behavior<Button>
    {
        public static readonly BindableProperty LinkDeviceStateProperty =
        BindableProperty.CreateAttached("LinkDeviceState", typeof(LinkDeviceState), typeof(LinkDeviceVisualStateBehavior), LinkDeviceState.NotLinked, propertyChanged: OnStateChanged);

        private static void OnStateChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var page = (bindable as Element).GetZLivingPage();
            BuildIt.Forms.VisualStateManager.GoToState(page ?? bindable as Element, Convert.ToString(newValue));
        }

        public static LinkDeviceState GetLinkDeviceState(BindableObject view)
        {
            return (LinkDeviceState)view.GetValue(LinkDeviceStateProperty);
        }

        public static void SetLinkDeviceState(BindableObject view, LoginState value)
        {
            view.SetValue(LinkDeviceStateProperty, value);
        }
    }
}
