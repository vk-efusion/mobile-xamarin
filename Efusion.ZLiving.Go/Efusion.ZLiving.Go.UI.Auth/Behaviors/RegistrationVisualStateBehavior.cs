﻿using Efusion.ZLiving.Go.UI.Auth.States;
using System;
using Xamarin.Forms;
using Efusion.ZLiving.Go.UI.Common.Extensions;

namespace Efusion.ZLiving.Go.UI.Auth.Behaviors
{
    public class RegistrationVisualStateBehavior : Behavior<Button>
    {
        public static readonly BindableProperty RegistrationStateProperty =
        BindableProperty.CreateAttached("RegistrationState", typeof(RegistrationState), typeof(RegistrationVisualStateBehavior), RegistrationState.NotRegistered, propertyChanged: OnRegistrationStateChanged);

        private static void OnRegistrationStateChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var page = (bindable as Element).GetZLivingPage();
            BuildIt.Forms.VisualStateManager.GoToState(page ?? bindable as Element, Convert.ToString(newValue));
        }

        public static RegistrationState GetRegistrationState(BindableObject view)
        {
            return (RegistrationState)view.GetValue(RegistrationStateProperty);
        }

        public static void SetRegistrationState(BindableObject view, RegistrationState value)
        {
            view.SetValue(RegistrationStateProperty, value);
        }
    }
}
