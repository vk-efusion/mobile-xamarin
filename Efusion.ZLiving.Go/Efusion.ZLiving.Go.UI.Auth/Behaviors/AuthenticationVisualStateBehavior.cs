﻿using Efusion.ZLiving.Go.UI.Common.Enums;
using System;
using Xamarin.Forms;
using Efusion.ZLiving.Go.UI.Common.Extensions;

namespace Efusion.ZLiving.Go.UI.Auth.Behaviors
{
    public class AuthenticationVisualStateBehavior : Behavior<Button>
    {        
        public static readonly BindableProperty AuthenticationStateProperty =
        BindableProperty.CreateAttached("AuthenticationState", typeof(AuthenticationState), typeof(AuthenticationVisualStateBehavior), AuthenticationState.Anonymous, propertyChanged: OnAuthenticationStateChanged);

        private static void OnAuthenticationStateChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var page = (bindable as Element).GetZLivingPage();            
            BuildIt.Forms.VisualStateManager.GoToState(page ?? bindable as Element, Convert.ToString(newValue));
        }

        public static AuthenticationState GetAuthenticationState(BindableObject view)
        {
            return (AuthenticationState)view.GetValue(AuthenticationStateProperty);
        }

        public static void SetAuthenticationState(BindableObject view, AuthenticationState value)
        {
            view.SetValue(AuthenticationStateProperty, value);
        }      
    }
}
