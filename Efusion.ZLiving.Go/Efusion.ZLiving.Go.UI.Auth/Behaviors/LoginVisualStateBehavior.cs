﻿using Efusion.ZLiving.Go.UI.Auth.States;
using Efusion.ZLiving.Go.UI.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Auth.Behaviors
{
    public class LoginVisualStateBehavior : Behavior<Button>
    {
        public static readonly BindableProperty LoginStateProperty =
        BindableProperty.CreateAttached("LoginState", typeof(LoginState), typeof(LoginVisualStateBehavior), LoginState.NotLoged, propertyChanged: OnStateChanged);

        private static void OnStateChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var page = (bindable as Element).GetZLivingPage();
            BuildIt.Forms.VisualStateManager.GoToState(page ?? bindable as Element, Convert.ToString(newValue));
        }

        public static RegistrationState GetLoginState(BindableObject view)
        {
            return (RegistrationState)view.GetValue(LoginStateProperty);
        }

        public static void SetLoginState(BindableObject view, LoginState value)
        {
            view.SetValue(LoginStateProperty, value);
        }
    }
}
