﻿using Efusion.ZLiving.Go.APIs.Auth.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Auth.States;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Prism.Commands;
using Prism.Navigation;
using System;

namespace Efusion.ZLiving.Go.UI.Auth.ViewModels
{
    public class LinkDevicePageViewModel : BaseViewModel
    {
        private string devicePIN;
        private string message;
        private LinkDeviceState linkDeviceState;
        public string Message { get => message; set => SetProperty(ref message, value); }
        public override string ViewModelName => "link-device-page-vm";
        public string DevicePIN { get => devicePIN; set => SetProperty(ref devicePIN,value); }        
        public DelegateCommand LinkDeviceCommand { get; set; }
        public LinkDeviceState LinkDeviceState { get => linkDeviceState; set => SetProperty(ref linkDeviceState, value); }

        public LinkDevicePageViewModel(FooterViewModel footerViewModel, ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : base(footerViewModel, sessionManager, navigationService,settings)
        {            
            this.LinkDeviceCommand = new DelegateCommand(LinkDevice);
          //  this.DevicePIN = "zalh625"; // for testing, will remove
        }

        public async void LinkDevice() {
            try
            {
                this.IsBusy = true;
                await this.SessionManager.LinkDevice(this.DevicePIN);
                this.LinkDeviceState = LinkDeviceState.Linked;
            }
            catch (UserNotFoundException ex) {
                this.Message = ex.Message;
                this.LinkDeviceState = LinkDeviceState.LinkError;
            }
            catch (Exception ex)
            {
                this.Message = "PIN Not Found";
                this.LinkDeviceState = LinkDeviceState.LinkError;
                this.TrackError(ex, "link-device", "Exception trying to link a device", $"pin={DevicePIN}");
            }
            finally
            {
                this.IsBusy = false;
            }
        }
    }
}
