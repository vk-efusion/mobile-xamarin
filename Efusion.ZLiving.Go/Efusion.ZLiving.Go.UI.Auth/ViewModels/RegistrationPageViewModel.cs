﻿using Efusion.ZLiving.Go.APIs.Auth.Exceptions;
using Efusion.ZLiving.Go.APIs.Auth.Services;
using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Auth.States;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Microsoft.AppCenter.Crashes;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Windows.Input;

namespace Efusion.ZLiving.Go.UI.Auth.ViewModels
{
    public class RegistrationPageViewModel : BaseViewModel
    {
        private string errorMessage;
        private string email;
        private string name;
        private string password;
        private string passwordRepeted;
        private bool isEmailValid;
        private bool isPasswordValid;
        private bool isPasswordRepetedValid;
        private RegistrationState registrationState;
        

        public IAuthenticationService AuthenticationService { get; set; }
        public ICommand RegistrationCommand { get; set; }
        public ICommand RegistrationWithGoogleCommand { get; set; }
        public ICommand RegistrationWithFacebookCommand { get; set; }
        public ICommand LoginCommand { get; set; }

        public string PasswordRepeted { get => passwordRepeted; set => SetProperty(ref passwordRepeted, value); }
        public string Email { get => email; set => SetProperty(ref email, value); }
        public string Name { get => name; set => SetProperty(ref name, value); }
        public string ErrorMessage { get => errorMessage; set => SetProperty(ref errorMessage, value); }
        public string Password { get => password; set => SetProperty(ref password, value); }

        public bool IsPasswordRepetedValid { get => isPasswordRepetedValid; set { SetProperty(ref isPasswordRepetedValid, value); RaisePropertyChanged("IsFormValid"); } }
        public bool IsEmailValid { get => isEmailValid; set { SetProperty(ref isEmailValid, value); RaisePropertyChanged("IsFormValid"); } }
        public bool IsPasswordValid { get => isPasswordValid; set { SetProperty(ref isPasswordValid, value); RaisePropertyChanged("IsFormValid"); } }

        public RegistrationState RegistrationState { get => registrationState; set => SetProperty(ref registrationState, value); }


        public RegistrationPageViewModel(FooterViewModel footerViewModel, IAuthenticationService authenticationService, INavigationService navigationService, ISessionManager sessionManager, IBaseSettings settings) : base(footerViewModel,sessionManager, navigationService, settings)
        {
            this.AuthenticationService = authenticationService;
            this.RegistrationCommand = new DelegateCommand(Register);
            this.RegistrationWithFacebookCommand = new DelegateCommand(RegisterWithFacebook);
            this.RegistrationWithGoogleCommand = new DelegateCommand(RegisterWithGoogle);
            this.LoginCommand = new DelegateCommand(Login);

            //this.password = String.Empty;
            this.password = "123456";
            this.passwordRepeted = String.Empty;
            this.passwordRepeted = "123456";
            this.email = "mferrari@gmail.com";
            this.name = "Maira Daniela Ferrari";
             
        }


        public async void RegisterWithFacebook()
        {
            try
            {
                this.IsBusy = true;

                await this.SessionManager.LogInWithFacebook();
                await this.NavigationService.NavigateAsync("menu");
            }
            catch (RegistrationException ex)
            {
                this.ErrorMessage = ex.Message;
                this.RegistrationState = RegistrationState.RegistrationError;
            }
            catch (PendingActivationException)
            {
                await this.AuthenticationService.ResendActivation(this.Email);
                this.RegistrationState = RegistrationState.ActivationPending;
            }
            finally {
                this.IsBusy = false;
            }
        }

        public async void RegisterWithGoogle()
        {
            await this.SessionManager.LogInWithGoogle();
        }

        public async void Login() {
            await this.NavigationService.NavigateAsync("login");
        }

        public async void Register()
        {
            try
            {
                this.IsBusy = true;

                if (String.IsNullOrWhiteSpace(this.Email) || String.IsNullOrWhiteSpace(this.Password) || String.IsNullOrWhiteSpace(this.Name))
                {
                    this.ErrorMessage = "Email, Password and Name are mandatory.";
                    this.RegistrationState = RegistrationState.RegistrationError;
                    return;
                }

                if (String.IsNullOrWhiteSpace(this.Password) || String.IsNullOrWhiteSpace(this.PasswordRepeted) || (!this.Password.Equals(this.PasswordRepeted)))
                {
                    this.ErrorMessage = "Password should be the same in both fields";
                    this.RegistrationState = RegistrationState.RegistrationError;
                    return;
                }

                try
                {
                    await this.AuthenticationService.Register(this.Name, this.Email, this.Password);
                    this.RegistrationState = RegistrationState.Registered;
                }
                catch (RegistrationException ex)
                {
                    this.ErrorMessage = ex.Message;
                    this.RegistrationState = RegistrationState.RegistrationError;
                }
                catch (PendingActivationException)
                {
                    await this.AuthenticationService.ResendActivation(this.Email);
                    this.RegistrationState = RegistrationState.ActivationPending;
                }
            }
            catch (Exception e)
            {
                this.ErrorMessage = "An unexpected exception has occured. Plese try again later.";
                this.RegistrationState = RegistrationState.UnhandledException;
                Crashes.TrackError(new Exception("android-environment-unhandled-exception", e));
            }
            finally {
                this.IsBusy = false;
            };
        }        
    }
}
