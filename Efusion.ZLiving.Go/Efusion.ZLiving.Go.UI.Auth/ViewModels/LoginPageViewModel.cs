﻿using Efusion.ZLiving.Go.APIs.Auth.Exceptions;
using Efusion.ZLiving.Go.APIs.Auth.Services;
using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Auth.States;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Microsoft.AppCenter.Crashes;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Windows.Input;

namespace Efusion.ZLiving.Go.UI.Auth.ViewModels
{
    public class LoginPageViewModel: BaseViewModel
    {
        private string email;
        private string password;
        private LoginState loginState;
        private string message;

        public ICommand RegistrationCommand { get; set; }
        public ICommand LoginCommand { get; set; }        
        public ICommand ForgotPasswordCommand { get; set; }
        public string Password { get => password; set => SetProperty( ref password, value); }
        public string Email { get => email; set => SetProperty(ref email, value); }
        public LoginState LoginState { get => loginState; set => SetProperty(ref loginState, value); }
        public string Message { get => message; set => SetProperty(ref message, value); }
        public IAuthenticationService AuthenticationService { get; set; }

        public LoginPageViewModel(FooterViewModel footerViewModel, IAuthenticationService authenticationService, ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : base(footerViewModel, sessionManager, navigationService,settings)
        {           
            this.RegistrationCommand = new DelegateCommand(Register);
            this.ForgotPasswordCommand = new DelegateCommand(ForgotPassword);
            this.AuthenticationService = authenticationService;
            this.LoginCommand = new DelegateCommand(Login);
            this.Email = "alex@efusion.tv"; // for testing, will remove
            this.Password = "test"; // for testing, will remove            
            this.LoginState = (sessionManager.AuthenticationState == Common.Enums.AuthenticationState.Authenticated) ? LoginState.Loged : LoginState.NotLoged;
    }

        public async void Register() {
            await this.NavigationService.NavigateAsync("registration");            
        }

        public async void Login()
        {
            try
            {
                this.IsBusy = true;

                if (String.IsNullOrWhiteSpace(this.Email) || String.IsNullOrWhiteSpace(this.Password))
                {
                    throw new UserNotFoundException();
                }

                if (await this.SessionManager.LogIn(this.email, this.password) == Common.Enums.AuthenticationState.Authenticated)
                {
                    this.LoginState = LoginState.Loged;
                    await this.NavigationService.NavigateAsync("menu");
                }
            }
            catch (UnauthorizedAccessException)
            {
                this.Message = "Invalid username or password";                
                this.LoginState = LoginState.LoginError;
                this.SessionManager.LogOut();
            }
            catch (PendingActivationException)
            {
                this.Message = "Email already in our system, but not activated. We re-sent you an email with activation link. Please find it in your inbox, don't forget to check Junk folder as well.";
                this.SessionManager.LogOut();
                this.LoginState = LoginState.ActivationPending;
            }
            catch (UserNotFoundException)
            {                
                this.Message = "Invalid username or password";
                this.LoginState = LoginState.LoginError;
                this.SessionManager.LogOut();
            }
            catch (Exception ex)
            {                
                this.Message = "An unexpected exception has occured. Plese try again later.";
                this.LoginState = LoginState.UnexpectedException;
                Crashes.TrackError(new Exception("unexpected-exception", ex));
            }
            finally {
                this.IsBusy = false;
            };
        }

        public async void ForgotPassword() {

            try
            {
                this.IsBusy = true;
                if (String.IsNullOrWhiteSpace(this.Email)) {
                    throw new UserNotFoundException();
                }

                await this.AuthenticationService.SendForgottenPassword(this.Email);
                this.LoginState = LoginState.ForgottenPasswordSent;
                this.Message = "We sent you an email with a reset password link. Please find it in your inbox, don't forget to check Junk folder as well.";
            }
            catch (UserNotFoundException)
            {
                this.LoginState = LoginState.LoginError;
                this.Message = "User not found in our system. Please enter a valid email.";
            }
            catch (Exception ex)
            {
                this.LoginState = LoginState.UnexpectedException;
                this.Message = "An unexpected exception has occured. Plese try again later.";
                Crashes.TrackError(new Exception("unexpected-exception", ex));
            }
            finally
            {
                this.IsBusy = false;
            };
        }      

    }
}
