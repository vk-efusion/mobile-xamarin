﻿using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.Auth.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LinkDevicePage : ZLivingBasePage
    {        
        public LinkDevicePage(ISessionManager sessionManager) : base(sessionManager)
        {
            InitializeComponent();            
        }
    }
}