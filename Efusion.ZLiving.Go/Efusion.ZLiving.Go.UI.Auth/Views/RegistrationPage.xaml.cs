﻿using BuildIt.States;
using BuildIt.States.Interfaces;
using Efusion.ZLiving.Go.UI.Auth.States;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.Auth.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistrationPage : ZLivingBasePage
	{
        
        public RegistrationPage(ISessionManager sessionManager) : base(sessionManager)
        {
            InitializeComponent();                    
        }

        public override void ConfigureStateManagerGroups(IStateManager stateManager)
        {
            base.ConfigureStateManagerGroups(stateManager);
            stateManager.Group<RegistrationState>().DefineAllStates();
        }
    }
}