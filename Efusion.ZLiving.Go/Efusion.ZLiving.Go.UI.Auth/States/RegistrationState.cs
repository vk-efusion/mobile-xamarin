﻿                            using Efusion.ZLiving.Go.UI.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.UI.Auth.States
{
    public enum RegistrationState
    {
        NotRegistered,        
        RegistrationError,
        ActivationPending,
        Registered,
        UnhandledException
    }
}
