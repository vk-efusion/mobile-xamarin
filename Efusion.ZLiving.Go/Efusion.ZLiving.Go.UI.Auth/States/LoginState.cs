﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.UI.Auth.States
{
    public enum LoginState
    {
        NotLoged,        
        ActivationPending,        
        ForgottenPasswordSent,
        LoginError,
        Loged,
        UnexpectedException
    }
}
