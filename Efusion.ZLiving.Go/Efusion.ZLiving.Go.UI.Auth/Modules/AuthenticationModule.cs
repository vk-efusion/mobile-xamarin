﻿using Efusion.ZLiving.Go.APIs.Auth.Services;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.UI.Auth.Views;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Managers;
using Prism.Ioc;
using Prism.Modularity;


namespace Efusion.ZLiving.Go.UI.Auth.Modules
{
    public class AuthenticationModule : IModule
    {
        
        public void OnInitialized(IContainerProvider containerProvider)
        {
            
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {            
            containerRegistry.RegisterSingleton<IRestClient, JsonRestClient>();
            containerRegistry.RegisterSingleton<IAuthenticationService, AuthenticationService>();
            containerRegistry.RegisterSingleton<ISessionManager, SessionManager>();

            containerRegistry.RegisterForNavigation<RegistrationPage>("registration");
            containerRegistry.RegisterForNavigation<LinkDevicePage>("link-device");
            containerRegistry.RegisterForNavigation<LoginPage>("login");            
        }
    }
}
