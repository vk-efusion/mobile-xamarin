﻿using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Efusion.ZLiving.Go.UI.VOD.Test.Models
{
    public class VideoTest
    {
        [Fact( DisplayName = "Video - Initialization Test")]
        public void InitializationTest()
        {
            MockINavigationService navMock = new MockINavigationService();
            Video video = new Video(navMock.Object);
            Assert.Equal("fullscreen-video", video.NavigationPage);
            
            video.Id = "video-id";
            video.Title = "video-title";
            video.PlayListID = "pId";

            NavigationParameters navParams = video.NavigationParameters;

            Assert.NotNull(navParams);
            Assert.True(video.ModalNavigation);
            Assert.NotNull(video.NavigationService);
            Assert.True(navParams.ContainsKey("id"));
            Assert.True(navParams.ContainsKey("title"));
            Assert.Equal(video.Id, navParams["id"]);
            Assert.Equal(video.Title, navParams["title"]);
            Assert.Equal("pId", video.PlayListID);
        }
    }
}
