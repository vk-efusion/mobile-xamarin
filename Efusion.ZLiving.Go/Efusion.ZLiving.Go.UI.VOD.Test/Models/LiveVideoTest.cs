﻿using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Efusion.ZLiving.Go.UI.VOD.Test.Models
{
    public class LiveVideoTest
    {

        [Fact(DisplayName = "Live Video - Initialization Test")]
        public void InitializationTest()
        {
            MockINavigationService navMock = new MockINavigationService();
            LiveVideo video = new LiveVideo(navMock.Object);

            Assert.Equal("fullscreen-video", video.NavigationPage);

            video.Id = "video-id";
            video.Title = "video-title";
            video.Start = "start";
            video.Duration = "duration";

            NavigationParameters navParams = video.NavigationParameters;

            Assert.NotNull(navParams);
            Assert.True(video.ModalNavigation);
            Assert.NotNull(video.NavigationService);
            Assert.True(navParams.ContainsKey("id"));
            Assert.True(navParams.ContainsKey("title"));
            Assert.Equal(video.Id, navParams["id"]);
            Assert.Equal(video.Title, navParams["title"]);
            Assert.Equal("start", video.Start);
            Assert.Equal("duration", video.Duration);
        }
    }
}
