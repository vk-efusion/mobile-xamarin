﻿using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Prism.Navigation;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Efusion.ZLiving.Go.UI.VOD.Test.Models
{
    public class PlaylistTest
    {

        [Fact(DisplayName = "Playlist - Initialization Test")]
        public void InitializationTest()
        {
            MockINavigationService navMock = new MockINavigationService();
            navMock.MockNavigateAsync("category", new NavigationParameters($"id=playlistId&title=PlaylistTitle"));

            Playlist playlist = new Playlist(navMock.Object)
            {
                Name = "PlaylistTitle",
                Id = "playlistId"
            };

            Assert.NotNull(playlist.NavigationService);
            Assert.NotNull(playlist.ViewAllCommannd);
            Assert.False(playlist.IsLive);
            playlist.Keywords.Add("live");
            Assert.True(playlist.IsLive);
            playlist.Keywords.Clear();
            Assert.False(playlist.IsLive);
            playlist.Keywords.Add("catch");
            Assert.True(playlist.IsLive);
            playlist.Keywords.Add("live");
            Assert.True(playlist.IsLive);

            dynamic keywords = new List<string>() { "list1", "list2" };

            playlist.AddKeywords(keywords);
            Assert.Contains("list1", playlist.Keywords);
            Assert.Contains("list2", playlist.Keywords);
            Assert.Equal(4,playlist.Keywords.Count);
            playlist.AddKeywords(null);
            Assert.Equal(4, playlist.Keywords.Count);
            playlist.ViewAll();
            navMock.VerifyAll();
        }

    }
}
