﻿using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Efusion.ZLiving.Go.UI.VOD.Test.Models
{
    public class ProgrammingGuideTest
    {
        [Fact(DisplayName = "Programming Guide - Initialization Test")]
        public void InitializationTest()
        {
            MockINavigationService navMock = new MockINavigationService();
            ProgrammingGuide pGuide= new ProgrammingGuide(navMock.Object);
            Assert.Equal("programming-guide", pGuide.NavigationPage);

            pGuide.Id = "pGuide-id";
            pGuide.Name = "pGuide-name";
            pGuide.LivePlayListID = "LivepId";

            NavigationParameters navParams = pGuide.NavigationParameters;

            Assert.NotNull(navParams);
            Assert.False(pGuide.ModalNavigation);
            Assert.NotNull(pGuide.NavigationService);
            Assert.True(navParams.ContainsKey("id"));
            Assert.True(navParams.ContainsKey("title"));
            Assert.Equal(pGuide.Id, navParams["id"]);
            Assert.Equal(pGuide.Name, navParams["title"]);
            Assert.Equal("LivepId", pGuide.LivePlayListID);
        }
    }
}
