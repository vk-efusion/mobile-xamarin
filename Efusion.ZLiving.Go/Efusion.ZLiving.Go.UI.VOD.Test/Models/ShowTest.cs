﻿using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Efusion.ZLiving.Go.UI.VOD.Test.Models
{
    public class ShowTest
    {
        [Fact(DisplayName = "Show - Initialization Test")]
        public void InitializationTest()
        {
            MockINavigationService navMock = new MockINavigationService();
            Show show = new Show(navMock.Object);
            Assert.Equal("show-details", show.NavigationPage);

            show.Id = "show-id";
            
            NavigationParameters navParams = show.NavigationParameters;

            Assert.NotNull(navParams);
            Assert.False(show.ModalNavigation);
            Assert.NotNull(show.NavigationService);
            Assert.True(navParams.ContainsKey("id"));            
            Assert.Equal(show.Id, navParams["id"]);            
        }
    }
}
