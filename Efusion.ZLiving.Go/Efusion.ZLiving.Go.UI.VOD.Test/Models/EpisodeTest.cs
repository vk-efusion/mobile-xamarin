﻿using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Efusion.ZLiving.Go.UI.VOD.Test.Models
{
    public class EpisodeTest
    {
        [Fact(DisplayName = "Episode - Initialization Test")]
        public void InitializationTest()
        {
            MockINavigationService navMock = new MockINavigationService();
            Episode episode = new Episode(navMock.Object);
            Assert.Equal("episode", episode.NavigationPage);

            episode.Id = "episode-id";
            episode.ShowID = "show-title";
            
            NavigationParameters navParams = episode.NavigationParameters;

            Assert.NotNull(navParams);
            Assert.False(episode.ModalNavigation);
            Assert.NotNull(episode.NavigationService);
            Assert.True(navParams.ContainsKey("id"));
            Assert.True(navParams.ContainsKey("show-id"));
            Assert.Equal(episode.Id, navParams["id"]);
            Assert.Equal(episode.ShowID, navParams["show-id"]);            
        }

    }
}
