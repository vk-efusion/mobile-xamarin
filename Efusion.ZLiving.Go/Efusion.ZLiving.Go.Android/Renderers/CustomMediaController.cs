﻿using Android.Content;
using Android.Content.PM;
using Android.Views;
using Android.Widget;
using Plugin.CurrentActivity;
using Prism.Navigation;

namespace Efusion.ZLiving.Go.Droid.Renderers
{
    public class CustomMediaController : MediaController
    {
        public ImageViewNavigation FullScreenImage { get; set; }                
        public Context MediaContext { get; set; }

        public NavigationParameters NavigationParameters
        {
            get => FullScreenImage.NavigationParameters; set => this.FullScreenImage.NavigationParameters = value; 
        }

        public CustomMediaController(Context context, INavigationService navigationService, NavigationParameters navigationParameters) : base(context)
        {
            this.MediaContext = context;
            this.CreateFullScreenImage(context, navigationService, navigationParameters);
        }

        public override void SetAnchorView(View view)
        {
            base.SetAnchorView(view);
            this.AddFullScreenImage();         
        }

        private void AddFullScreenImage() {

            LayoutParams frameParams = new LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent)
            {
                Gravity = GravityFlags.Right | GravityFlags.Top
            };

            frameParams.TopMargin = 35;            
            AddView(this.FullScreenImage, frameParams);
        }


        private View CreateFullScreenImage(Context context, INavigationService navigationService, NavigationParameters navigationParameters)
        {
            this.FullScreenImage = new ImageViewNavigation(context, navigationService, navigationParameters);

            if (CrossCurrentActivity.Current.Activity.RequestedOrientation == ScreenOrientation.Portrait) {
                FullScreenImage.SetBackgroundResource(Resource.Drawable.full_screen_40);
            }
            else {
                FullScreenImage.SetBackgroundResource(Resource.Drawable.normal_screen_40);
            }

            FullScreenImage.SetOnClickListener (new FullImageOnclickListener());
            return FullScreenImage;
        }

        public class FullImageOnclickListener : Java.Lang.Object, View.IOnClickListener
        {
            public async void OnClick(View v)
            {
                ImageViewNavigation image = v as ImageViewNavigation;

                if (CrossCurrentActivity.Current.Activity.RequestedOrientation == ScreenOrientation.Portrait) {
                    v.SetBackgroundResource(Resource.Drawable.normal_screen_40);
                    CrossCurrentActivity.Current.Activity.RequestedOrientation = ScreenOrientation.Landscape;
                    await image.Navigate("fullscreen-video");
                }
                else {
                    CrossCurrentActivity.Current.Activity.RequestedOrientation = ScreenOrientation.Portrait;
                    v.SetBackgroundResource(Resource.Drawable.full_screen_40);
                    CrossCurrentActivity.Current.Activity.OnBackPressed();
                }
            }
        }              

    }
}