﻿using System;
using System.ComponentModel;
using System.IO;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using Efusion.ZLiving.Go.Droid.Renderers;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.Models;
using Efusion.ZLiving.Go.UI.Common.Views;
using Plugin.CurrentActivity;
using Prism.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ARelativeLayout = Android.Widget.RelativeLayout;

[assembly: ExportRenderer(typeof(VideoPlayer), typeof(VideoPlayerRenderer))]
namespace Efusion.ZLiving.Go.Droid.Renderers
{
    public class VideoPlayerRenderer : ViewRenderer<VideoPlayer, ARelativeLayout>
    {
        private VideoView videoView;
        private TextView titleView;
        private ImageView closeImage;
        private CustomMediaController mediaController;    // Used to display transport controls
        private bool isPrepared;


        public VideoPlayerRenderer(Context context) : base(context)
        {

        }


        private void CreateVideo(string title)
        {
            // Save the VideoView for future reference
            videoView = new VideoView(Context);

            // Put the VideoView in a RelativeLayout
            ARelativeLayout relativeLayout = new ARelativeLayout(Context);
            relativeLayout.SetGravity(GravityFlags.FillHorizontal);
            relativeLayout.AddView(videoView);

            // Center the VideoView in the RelativeLayout
            ARelativeLayout.LayoutParams layoutParams = new ARelativeLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent);
            layoutParams.AddRule(LayoutRules.CenterInParent);
            layoutParams.SetMargins(0, 0, 0, 0);
            videoView.LayoutParameters = layoutParams;
            videoView.Prepared += OnVideoViewPrepared;
            SetNativeControl(relativeLayout);

            titleView = new TextView(Context)
            {
                Text = title
            };

            TextViewCompat.SetTextAppearance(titleView, Resource.Style.VideoTitle);

            ARelativeLayout.LayoutParams titleParams = new ARelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
            titleParams.AddRule(LayoutRules.AlignParentLeft);
            titleParams.AddRule(LayoutRules.AlignParentTop);

            titleParams.TopMargin = 30;
            titleParams.LeftMargin = 30;
            titleParams.RightMargin = 150;
            titleView.LayoutParameters = titleParams;
            relativeLayout.AddView(titleView);

            if (Element.FullScreen)
            {
                ARelativeLayout.LayoutParams closeParams = new ARelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                closeParams.AddRule(LayoutRules.AlignParentRight);
                closeParams.AddRule(LayoutRules.AlignParentTop);

                closeImage = new ImageView(Context);
                closeParams.RightMargin = 20;
                closeImage.LayoutParameters = closeParams;
                closeImage.SetBackgroundResource(Resource.Drawable.close_icon_40);
                closeImage.SetOnClickListener(new BackImageOnclickListener());
                relativeLayout.AddView(closeImage);
            }
        }

        private void EnterFullScreen(string title)
        {
            this.CreateVideo(title);
            CrossCurrentActivity.Current.Activity.RequestedOrientation = ScreenOrientation.Landscape;
        }

        private void EnterPortrait()
        {
            this.CreateVideo(String.Empty);
            CrossCurrentActivity.Current.Activity.RequestedOrientation = ScreenOrientation.Portrait;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<VideoPlayer> args)
        {
            base.OnElementChanged(args);

            if (args.NewElement != null)
            {
                if (Control == null)
                {
                    if (args.NewElement.FullScreen)
                    {
                        this.EnterFullScreen(args.NewElement.Title);
                    }
                    else
                    {
                        this.EnterPortrait();
                    }

                }

                SetAreTransportControlsEnabled();
                SetTitle();
                SetSource();
                SetVideoId();

                args.NewElement.UpdateStatus += OnUpdateStatus;
                args.NewElement.PlayRequested += OnPlayRequested;
                args.NewElement.PauseRequested += OnPauseRequested;
                args.NewElement.StopRequested += OnStopRequested;
            }

            if (args.OldElement != null)
            {
                args.OldElement.UpdateStatus -= OnUpdateStatus;
                args.OldElement.PlayRequested -= OnPlayRequested;
                args.OldElement.PauseRequested -= OnPauseRequested;
                args.OldElement.StopRequested -= OnStopRequested;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (Control != null && videoView != null)
            {
                videoView.Prepared -= OnVideoViewPrepared;
            }

            if (Element != null)
            {
                Element.UpdateStatus -= OnUpdateStatus;
            }

            base.Dispose(disposing);
        }

        void OnVideoViewPrepared(object sender, EventArgs args)
        {
            isPrepared = true;
            ((IVideoPlayerController)Element).Duration = TimeSpan.FromMilliseconds(videoView.Duration);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            base.OnElementPropertyChanged(sender, args);

            if (args.PropertyName == VideoPlayer.AreTransportControlsEnabledProperty.PropertyName)
            {
                SetAreTransportControlsEnabled();
            }
            else if (args.PropertyName == VideoPlayer.SourceProperty.PropertyName)
            {
                SetSource();
            }
            else if (args.PropertyName == VideoPlayer.PositionProperty.PropertyName)
            {
                if (Math.Abs(videoView.CurrentPosition - Element.Position.TotalMilliseconds) > 1000)
                {
                    videoView.SeekTo((int)Element.Position.TotalMilliseconds);
                }
            }
            else if (args.PropertyName == VideoPlayer.TitleProperty.PropertyName)
            {
                this.SetTitle();
            }
            else if (args.PropertyName == VideoPlayer.VideoIdProperty.PropertyName)
            {
                this.SetVideoId();
            }

        }

        void SetAreTransportControlsEnabled()
        {
            if (Element.AreTransportControlsEnabled)
            {
                mediaController = new CustomMediaController(Context, Element.NavigationService, new NavigationParameters($"title={Element.Title}&id={Element.VideoId}&position={Element.Position.Ticks}"));
                mediaController.SetAnchorView(videoView);
                videoView.SetMediaController(mediaController);
            }
            else
            {
                videoView.SetMediaController(null);

                if (mediaController != null)
                {
                    mediaController.SetMediaPlayer(null);
                    mediaController = null;
                }
            }
        }

        private void SetTitle()
        {

            if (mediaController != null)
            {
                mediaController.NavigationParameters = new NavigationParameters($"title={Element.Title}&id={Element.VideoId}&position={Element.Position.Ticks}");
            }

            if (Element.FullScreen)
            {
                this.titleView.Text = Element.Title;
            }
        }

        private void SetVideoId()
        {
            if (mediaController != null)
            {
                mediaController.NavigationParameters = new NavigationParameters($"title={Element.Title}&id={Element.VideoId}&position={Element.Position.Ticks}");
            }
        }

        private void SetSource()
        {
            isPrepared = false;
            bool hasSetSource = false;

            if (Element.Source is UriVideoSource)
            {
                string uri = (Element.Source as UriVideoSource).Uri;

                if (!String.IsNullOrWhiteSpace(uri))
                {
                    videoView.SetVideoURI(Android.Net.Uri.Parse(uri));
                    hasSetSource = true;
                }
            }
            else if (Element.Source is FileVideoSource)
            {
                string filename = (Element.Source as FileVideoSource).File;

                if (!String.IsNullOrWhiteSpace(filename))
                {
                    videoView.SetVideoPath(filename);
                    hasSetSource = true;
                }
            }
            else if (Element.Source is ResourceVideoSource)
            {
                string package = Context.PackageName;
                string path = (Element.Source as ResourceVideoSource).Path;

                if (!String.IsNullOrWhiteSpace(path))
                {
                    string filename = Path.GetFileNameWithoutExtension(path).ToLowerInvariant();
                    string uri = "android.resource://" + package + "/raw/" + filename;
                    videoView.SetVideoURI(Android.Net.Uri.Parse(uri));
                    hasSetSource = true;
                }
            }

            if (hasSetSource && Element.AutoPlay)
            {
                videoView.Start();
            }
        }

        // Event handler to update status
        void OnUpdateStatus(object sender, EventArgs args)
        {
            VideoStatus status = VideoStatus.NotReady;

            if (isPrepared)
            {
                status = videoView.IsPlaying ? VideoStatus.Playing : VideoStatus.Paused;
            }

            ((IVideoPlayerController)Element).Status = status;

            // Set Position property
            TimeSpan timeSpan = TimeSpan.FromMilliseconds(videoView.CurrentPosition);
            ((IElementController)Element).SetValueFromRenderer(VideoPlayer.PositionProperty, timeSpan);

            if (mediaController != null)
            {
                mediaController.NavigationParameters = new NavigationParameters($"title={Element.Title}&id={Element.VideoId}&position={videoView.CurrentPosition}");
            }

            this.titleView.Visibility = (mediaController == null) ? ViewStates.Gone : ((mediaController.IsShown) ? ViewStates.Visible : ViewStates.Gone);
            if (closeImage != null)
            {
                this.closeImage.Visibility = this.titleView.Visibility;
            }
        }

        // Event handlers to implement methods
        void OnPlayRequested(object sender, EventArgs args)
        {
            videoView.Start();
        }

        void OnPauseRequested(object sender, EventArgs args)
        {
            videoView.Pause();
        }

        void OnStopRequested(object sender, EventArgs args)
        {
            videoView.StopPlayback();
        }
    }

    public class BackImageOnclickListener : Java.Lang.Object, Android.Views.View.IOnClickListener
    {
        public void OnClick(Android.Views.View v)
        {
            CrossCurrentActivity.Current.Activity.RequestedOrientation = ScreenOrientation.Portrait;
            CrossCurrentActivity.Current.Activity.OnBackPressed();
        }
    }
}

