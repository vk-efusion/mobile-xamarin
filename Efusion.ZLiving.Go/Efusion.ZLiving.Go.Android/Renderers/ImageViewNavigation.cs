﻿using Android.Content;
using Android.Widget;
using Prism.Navigation;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.Droid.Renderers
{
    public class ImageViewNavigation: ImageView
    {
        public INavigationService NavigationService { get; set; }
        public NavigationParameters NavigationParameters { get; set; }

        public ImageViewNavigation(Context context, INavigationService navigationService, NavigationParameters navigationParameters) : base(context)
        {
            this.NavigationService = navigationService;
            this.NavigationParameters = navigationParameters;
        }

        public async Task Navigate(string page) {
            await this.NavigationService.NavigateAsync(page, NavigationParameters, true);
        }
    }
}