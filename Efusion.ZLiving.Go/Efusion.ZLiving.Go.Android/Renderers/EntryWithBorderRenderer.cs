﻿using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Efusion.ZLiving.Go.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Entry), typeof(EntryWithBorderRenderer))]
namespace Efusion.ZLiving.Go.Droid.Renderers
{
    public class EntryWithBorderRenderer : EntryRenderer
    {
        public EntryWithBorderRenderer(Context context):base(context) {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                var nativeEditText = (global::Android.Widget.EditText)Control;
                float border = 55;
                float[] outerR = new float[8];
                outerR[0] = border;
                outerR[1] = border;
                outerR[2] = border;
                outerR[3] = border;
                outerR[4] = border;
                outerR[5] = border;
                outerR[6] = border;
                outerR[7] = border;

                RectF inset = new RectF(6, 6, 6, 6);
                float[] innerR = new float[] { 12, 12, 0, 0, 12, 12, 0, 0 };


                var shape = new ShapeDrawable(new Android.Graphics.Drawables.Shapes.RoundRectShape(outerR, null, null));
               // shape.Paint.Color = new Android.Graphics.Color(ContextCompat.GetColor(this.Context, Resource.Color.colorZLivingOrange));                
                shape.Paint.Color = Xamarin.Forms.Color.White.ToAndroid();
                shape.Paint.StrokeWidth = 10;
                shape.Paint.SetStyle(Paint.Style.Stroke);                 
                nativeEditText.Background = shape;
                
            }
        }
    }
}