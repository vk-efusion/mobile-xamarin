﻿using Android.Content;
using Android.Views;
using Efusion.ZLiving.Go.Droid.Renderers;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ZLivingScrollView), typeof(ZlivingScrollViewRenderer))]
namespace Efusion.ZLiving.Go.Droid.Renderers
{
    public class ZlivingScrollViewRenderer : ScrollViewRenderer
    {
        public ZlivingScrollViewRenderer(Context context) : base(context)
        {
            
        }

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            var element = e.NewElement as ZLivingScrollView;
            element?.Render();
        }


    }
}