﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using CarouselView.FormsPlugin.Android;
using System.Threading.Tasks;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using Plugin.CurrentActivity;
using Xamarin.Facebook;
using Prism;
using Prism.Ioc;
using Efusion.ZLiving.Go.Droid.Managers;
using Efusion.ZLiving.Go.UI.Common.Contracts;

namespace Efusion.ZLiving.Go.Droid
{
    [Activity(Label = "Efusion.ZLiving.Go", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;            
            
            AppDomain.CurrentDomain.UnhandledException += (sender, e) => {
                Crashes.TrackError(new Exception("android--app-domain-unhandled-exception", (e.ExceptionObject is Exception) ? (Exception)e.ExceptionObject : new Exception(JsonConvert.SerializeObject(e.ExceptionObject)))); };
            TaskScheduler.UnobservedTaskException += (sender, e) => {
                Crashes.TrackError(new Exception("android-unobserved-task-exception", e.Exception)); };
            AndroidEnvironment.UnhandledExceptionRaiser += (sender, e) => {
                Crashes.TrackError(new Exception("android-environment-unhandled-exception", e.Exception)); };

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
 
            CarouselViewRenderer.Init();
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(true);
            CrossCurrentActivity.Current.Init(this, bundle);
            LoadApplication(new App(new AndroidInitializer()));
       
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
            this.RequestedOrientation = ScreenOrientation.Portrait;
        }

        public class AndroidInitializer : IPlatformInitializer
        {            
            public void RegisterTypes(IContainerRegistry containerRegistry)
            {
                containerRegistry.RegisterSingleton<IFacebookManager, FacebookManager>();
                containerRegistry.RegisterSingleton<IGoogleManager, GoogleManager>();
            }
        }
    }
}

