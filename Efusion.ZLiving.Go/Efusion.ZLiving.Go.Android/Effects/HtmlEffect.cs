﻿using System;
using Android.OS;
using Android.Text;
using Android.Widget;
using Efusion.ZLiving.Go.Droid.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("Efusion.ZLiving.Go.UI.Common.Effects")]
[assembly: ExportEffect(typeof(HtmlEffect), "HtmlEffect")]
namespace Efusion.ZLiving.Go.Droid.Effects
{
    public class HtmlEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            Label label = Element as Label;
            String text = label.Text ?? String.Empty;

            if (Build.VERSION.SdkInt >= BuildVersionCodes.N)
            {
                (Control as TextView)?.SetText(Html.FromHtml(text, FromHtmlOptions.ModeLegacy), TextView.BufferType.Spannable);
            }
            else
            {
                (Control as TextView)?.SetText(Html.FromHtml(text), TextView.BufferType.Spannable);
            }
        }

        protected override void OnDetached()
        {

        }
    }
}