﻿using Android.Content;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Views;
using Android.Views.InputMethods;
using Efusion.ZLiving.Go.Droid.Effects;
using Efusion.ZLiving.Go.UI.Common.Extensions;
using Efusion.ZLiving.Go.UI.Common.Views;
using FFImageLoading.Forms;
using Plugin.CurrentActivity;
using Prism.Commands;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using static Android.Views.View;

//[assembly: ResolutionGroupName("Efusion.ZLiving.Go.UI.Common.Effects")]
[assembly: ExportEffect(typeof(SearchEffect), "SearchEffect")]
namespace Efusion.ZLiving.Go.Droid.Effects
{
    public class SearchEffect : PlatformEffect
    {
        public static Toolbar GetToolbar() => (CrossCurrentActivity.Current?.Activity as MainActivity)?.FindViewById<Toolbar>(Resource.Id.toolbar);        
        public SearchView SearchView { get; set; }
        public ICommand TapGestureCommand { get; set; }
        public TapGestureRecognizer SearchGeasture { get; set; }

        public SearchEffect() {       
            this.TapGestureCommand = new DelegateCommand(TransformToolbar);
        }

        public void TransformToolbar()
        {
            Toolbar toolBar = GetToolbar();

            if (toolBar == null || Element == null) { return; }

            IMenuItem searchMenuItem = toolBar.Menu?.FindItem(Resource.Id.action_search);

            if (searchMenuItem != null) {
                toolBar.Menu.Clear();
            }

            GetToolbar().InflateMenu(Resource.Menu.searchmenu);

            SearchView = toolBar.Menu?.FindItem(Resource.Id.action_search)?.ActionView?.JavaCast<SearchView>();
            SearchView.FocusedByDefault = true;
            SearchView.QueryTextChange += SearchView_QueryTextChange;
            SearchView.QueryTextSubmit += SearchView_QueryTextSubmit;
            SearchView.SetOnQueryTextFocusChangeListener(new SearchViewOnFocus());
            SearchView.QueryHint = (Element as ZLivingBasePage)?.SearchPlaceHolderText;
            SearchView.ImeOptions = (int)ImeAction.Search;
            SearchView.InputType = (int)InputTypes.TextVariationNormal;

            Display display = CrossCurrentActivity.Current.Activity.WindowManager.DefaultDisplay;
            Android.Graphics.Point size = new Android.Graphics.Point();
            display.GetSize(size);
            int width = size.X; // CrossCurrentActivity.Current.Activity.Resources.(Resource.Drawable.search_logo);

            SearchView.MaxWidth = width - 400;       //int.MaxValue -> Hack to go full width - http://stackoverflow.com/questions/31456102/searchview-doesnt-expand-full-width
            SearchView.SetIconifiedByDefault(false);
            SearchView.SubmitButtonEnabled = true;

            Android.Widget.ImageView searchViewIcon = SearchView.FindViewById<Android.Widget.ImageView>(Resource.Id.search_mag_icon);
            ViewGroup linearLayoutSearchView = (ViewGroup)searchViewIcon.Parent;
            linearLayoutSearchView.RemoveView(searchViewIcon);
            IMenuItem backItem = toolBar.Menu?.FindItem(Resource.Id.action_back);
            backItem?.SetOnMenuItemClickListener(new BackMenuItemListener());

        }

        protected override void OnAttached()
        {            
            this.SearchGeasture = new TapGestureRecognizer
            {
                Command = this.TapGestureCommand
            };

            (Element as CachedImage)?.GestureRecognizers.Add(SearchGeasture);
        }

        
        protected override void OnDetached()
        {
            if (this.SearchView != null)
            {
                SearchView.QueryTextChange -= SearchView_QueryTextChange;
                SearchView.QueryTextSubmit -= SearchView_QueryTextSubmit;                
            }

            (Element as CachedImage)?.GestureRecognizers.Remove(SearchGeasture);
            GetToolbar()?.Menu?.Clear();
        }

        private void SearchView_QueryTextSubmit(object sender, SearchView.QueryTextSubmitEventArgs e)
        {
            if (e == null) { return; }

            ZLivingBasePage searchPage = Element?.GetZLivingPage();

            if (searchPage == null) { return; }

            searchPage.SearchText = e.Query;
            searchPage.SearchCommand?.Execute(e.Query);
            e.Handled = true;
        }

        private void SearchView_QueryTextChange(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            ZLivingBasePage searchPage = Element?.GetZLivingPage();

            if (searchPage == null) { return; }

            searchPage.SearchText = e?.NewText;
        }
        
    }

    public class BackMenuItemListener : Java.Lang.Object, IMenuItemOnMenuItemClickListener
    {    
        public bool OnMenuItemClick(IMenuItem item)
        {
            SearchEffect.GetToolbar()?.Menu?.Clear();
            return true;
        }
    }

    public class SearchViewOnFocus : Java.Lang.Object, IOnFocusChangeListener
    {        
        public void OnFocusChange(Android.Views.View v, bool hasFocus)
        {
            if (hasFocus)
            {
                ShowInputMethod(v.FindFocus());
            }
        }

        private void ShowInputMethod(Android.Views.View view)
        {
            InputMethodManager imm = (InputMethodManager) CrossCurrentActivity.Current.Activity.GetSystemService(Context.InputMethodService);
            imm?.ShowSoftInput(view, 0);            
        }
    }
}