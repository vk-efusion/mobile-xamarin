﻿using Android.Graphics.Drawables;
using Android.Widget;
using Efusion.ZLiving.Go.Droid.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

//[assembly: ResolutionGroupName("Efusion.ZLiving.Go.UI.Common.Effects")]
[assembly: ExportEffect(typeof(TitleGradientBackgroundEffect), "TitleGradientBackgroundEffect")]
namespace Efusion.ZLiving.Go.Droid.Effects
{
    public class TitleGradientBackgroundEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            (Control as TextView)?.SetBackground(new GradientDrawable(GradientDrawable.Orientation.RightLeft, new int[] { Color.FromHex("F37330").ToAndroid(), Color.FromHex("FCB537").ToAndroid() }));
            Control.SetPadding(20, 0, 0, 0);
        }

        protected override void OnDetached()
        {
            
        }
    }
}