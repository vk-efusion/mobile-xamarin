﻿using System;
using System.Collections.Generic;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Models;
using Org.Json;
using Plugin.CurrentActivity;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Auth;
using System.Threading.Tasks;
using System.Runtime.Remoting.Contexts;
using Newtonsoft.Json.Linq;
using Efusion.ZLiving.Go.UI.Common.Enums;

//[assembly: Dependency(typeof(FacebookManager))]
namespace Efusion.ZLiving.Go.Droid.Managers
{
    public class FacebookManager : Java.Lang.Object, IFacebookManager
    {
        public Func<SocialNetworkUser, string, Task> OnLoginComplete { get; set; }
        public Func<AuthenticationState> OnLogoutRequested { get; set; }        

        public async Task Login(Func<SocialNetworkUser, string, Task> onLoginComplete, Func<AuthenticationState> onLogoutRequested ,string clientId,string authoriseUrl,string redirectUrl )
        {
            this.OnLoginComplete = onLoginComplete;
            this.OnLogoutRequested = onLogoutRequested;
            
            var account = await LoginAsync(clientId, "public_profile,email", new Uri(authoriseUrl), new Uri(redirectUrl), CrossCurrentActivity.Current.AppContext , true);

            if (account != null)
            {
              string email = await GetFacebookUserInfoAsync(account);
              await OnCompleted(account,email);
            }

        }

        public AuthenticationState Logout()
        {
            return OnLogoutRequested.Invoke();
        }


        public async Task<string> GetFacebookUserInfoAsync(Account account)
        {
            try
            {
                var request = new OAuth2Request("GET", new Uri("https://graph.facebook.com/me?fields=email"), null, account);
                var userInfoResult = await request.GetResponseAsync();
                var obj = JObject.Parse(userInfoResult.GetResponseText());

                if (obj != null)
                {
                    return obj.GetValue("email").ToString();
                }
            }
            catch (Exception ex)
            {
                // say what went wrong here.
            }
            return null;
        }

        public async Task OnCompleted(Account account, string email)
        {       
           await OnLoginComplete?.Invoke(new SocialNetworkUser(account.Properties["state"], account.Properties["access_token"], String.Empty, String.Empty, email, String.Empty), string.Empty);
        }
        

        public static Task<Account> LoginAsync(string appId, string requestedScope, Uri authoriseUrl, Uri redirectUrl, Android.Content.Context context, bool allowCancel)
        {
            var tcs = new TaskCompletionSource<Account>();

            var auth = new OAuth2Authenticator(appId, requestedScope, authoriseUrl, redirectUrl)
            {
                AllowCancel = allowCancel,                
            };

            auth.Completed += (sender, e) => tcs.SetResult(e.IsAuthenticated ? e.Account : null);

            auth.Error += (sender, e) => tcs.SetException(e.Exception);
            
            context.StartActivity(auth.GetUI(context));
            return tcs.Task;
        }

      
    }

}
