﻿using System;
using System.Threading.Tasks;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.Models;
using Newtonsoft.Json.Linq;
using Plugin.CurrentActivity;
using Xamarin.Auth;

namespace Efusion.ZLiving.Go.Droid.Managers
{
    public class GoogleManager : IGoogleManager
    {
        public Func<SocialNetworkUser, string, Task> OnLoginComplete { get; set; }
        public Func<AuthenticationState> OnLogoutRequested { get; set; }

        public async Task Login(Func<SocialNetworkUser, string, Task> onLoginComplete, Func<AuthenticationState> onLogoutRequested , string clientId, string authoriseUrl, string redirectUrl)
        {
            this.OnLoginComplete = onLoginComplete;
            this.OnLogoutRequested = onLogoutRequested;

            var account = await LoginAsync(clientId, "https://www.googleapis.com/auth/userinfo.email", new Uri(authoriseUrl), new Uri(redirectUrl), CrossCurrentActivity.Current.AppContext, true);

            if (account != null)
            {
                string email = await GetGoogleUserInfoAsync(account);
                await OnCompleted(account, email);
            }
        }


        public AuthenticationState Logout()
        {
            return OnLogoutRequested.Invoke();
        }


        public static Task<Account> LoginAsync(string appId, string requestedScope, Uri authoriseUrl, Uri redirectUrl, Android.Content.Context context, bool allowCancel)
        {
            var tcs = new TaskCompletionSource<Account>();

            var auth = new OAuth2Authenticator(appId, requestedScope, authoriseUrl, redirectUrl, null, true)
            {
                AllowCancel = allowCancel,
            };

            auth.Completed += (sender, e) => tcs.SetResult(e.IsAuthenticated ? e.Account : null);

            auth.Error += (sender, e) => tcs.SetException(e.Exception);

            context.StartActivity(auth.GetUI(context));
            return tcs.Task;
        }

        public async Task OnCompleted(Account account, string email)
        {
            await OnLoginComplete?.Invoke(new SocialNetworkUser(account.Properties["state"], account.Properties["access_token"], String.Empty, String.Empty, email, String.Empty), string.Empty);
        }


        public async Task<string> GetGoogleUserInfoAsync(Account account)
        {
            try
            {
                var request = new OAuth2Request("GET", new Uri("https://www.googleapis.com/userinfo/email?on"), null, account);
                var userInfoResult = await request.GetResponseAsync();
                var obj = JObject.Parse(userInfoResult.GetResponseText());

                if (obj != null)
                {
                    return obj.GetValue("email").ToString();
                }
            }
            catch (Exception ex)
            {
                // say what went wrong here.
            }
            return null;
        }


    }
}