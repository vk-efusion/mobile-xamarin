﻿using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using RestSharp;
using Xunit;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Exceptions
{
    public class RestHTTPExceptionTest
    {

        [Fact(DisplayName = "Initialization Test")]
        public void InitializationTest() {

            IRestResponse response = new MockIRestResponse().Object;
            RestHTTPException exception = new RestHTTPException("message", response);

            Assert.Equal("message", exception.Message);
            Assert.Equal(response, exception.Response);

            exception = new RestHTTPException(response);
            Assert.Contains("Efusion.ZLiving.Go.APIs.Common.Exceptions.RestHTTPException", exception.Message);
            Assert.Same(response, exception.Response);
        }
    }
}
