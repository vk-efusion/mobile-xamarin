﻿using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Xunit;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Services
{
    public class JsonBaseServiceTest : IClassFixture<SettingsFixture>
    {
        public SettingsFixture Settings { get; set; }

        public JsonBaseServiceTest(SettingsFixture settings)
        {
            this.Settings = settings;
        }
    
        [Fact(DisplayName = "Initialization Test")]
        public void InitializationTest()
        {
            IRestClient client = new MockRestClient().Object;
            JsonBaseService jsonService = new JsonBaseService(client, Settings);

            Assert.Equal("base-service",jsonService.ServiceName);
            Assert.Same(client, jsonService.Client);
            Assert.Same(Settings, jsonService.Settings);            
        }
    }
}
