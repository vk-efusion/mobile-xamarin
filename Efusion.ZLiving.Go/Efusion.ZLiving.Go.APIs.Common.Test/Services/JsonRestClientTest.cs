using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Reflection;
using Xunit;
using Xunit.Sdk;

namespace Efusion.ZLiving.Go.APIs.Common.Test
{

    public class ExecuteHTTPRequestDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            throw new NotImplementedException();
        }
    }

    public class JsonRestClientTest : IClassFixture<SettingsFixture>
    {
        public SettingsFixture Settings { get; set; }

        public static IEnumerable<object[]> ExecuteHttpRequestTestData =>
       new List<object[]>
       {
            new object[] { new List<(string, string, ParameterType)>() { ("email", "email@email.com",ParameterType.GetOrPost), ("name","FirstName LastName",ParameterType.GetOrPost) },
                           new List<(string, string)>() { ("test", "testValue") },                           
                          },
            new object[] { null ,
                           new List<(string, string)>() { ("test", "testValue") },                           
                          },
            new object[] { new List<(string, string, ParameterType)>() { ("email", "email@email.com",ParameterType.GetOrPost), ("name","FirstName LastName",ParameterType.GetOrPost) },
                           null
                         },
            new object[] { new List<(string, string, ParameterType)>() { ("email", "email@email.com",ParameterType.GetOrPost), ("name","FirstName LastName",ParameterType.GetOrPost) },
                           new List<(string, string)>() { ("test", "testValue") }                           
                         },
            new object[] { new List<(string, string, ParameterType)>() { ("email", "email@email.com",ParameterType.GetOrPost), ("name","FirstName LastName",ParameterType.GetOrPost) },
                           null},
            new object[] { null, null},            
       };

        public JsonRestClientTest(SettingsFixture settings)
        {
            this.Settings = settings;
        }

        [Theory(DisplayName = "ExecuteHTTPRequest - Throws Exception Validation")]
        [MemberData(nameof(ExecuteHttpRequestTestData))]
        public void ExecuteHTTPRequest_ExceptionResponse_Test(List<(string, string, ParameterType)> parameters, List<(string, string)> headers)
        {
            MockJsonRestClient jsonClient = new MockJsonRestClient(Settings);
            //Mocking expectations
            RequestMocks mocks = jsonClient.InitializeResponse(false, "everythingOk",  headers, parameters);

            //Getting Result
            Exception ex = Record.Exception(() => jsonClient.Object.ExecuteHTTPRequest("test_base_endpoint", "test_endpoint", RestSharp.Method.GET, headers, parameters));
            mocks.MockRestSharpRestClient.VerifyAll();
            Assert.IsType<RestHTTPException>(ex);
            Assert.Same((ex as RestHTTPException).Response, mocks.MockIRestResponse.Object);
        }

        [Theory(DisplayName = "ExecuteHTTPRequest - Throws Exception Validation")]
        [MemberData(nameof(ExecuteHttpRequestTestData))]
        public async void ExecuteHTTPRequestAsync_ExceptionResponse_Test(List<(string, string, ParameterType)> parameters, List<(string, string)> headers)
        {
            MockJsonRestClient jsonClient = new MockJsonRestClient(Settings);
            //Mocking expectations
            RequestMocks mocks = jsonClient.InitializeResponse(false, "everythingOk", headers, parameters);

            //Getting Result
            Exception ex = await Record.ExceptionAsync(() => jsonClient.Object.ExecuteHTTPRequest("test_base_endpoint", "test_endpoint",  RestSharp.Method.GET, headers, parameters));
            mocks.MockRestSharpRestClient.VerifyAll();
            Assert.IsType<RestHTTPException>(ex);
            Assert.Same((ex as RestHTTPException).Response, mocks.MockIRestResponse.Object);
        }

        [Theory(DisplayName = "ExecuteHTTPRequest - Create Correct Request Test")]
        [MemberData(nameof(ExecuteHttpRequestTestData))]
        public void ExecuteHTTPRequest_SuccessfulResponse_Test(List<(string, string, ParameterType)> parameters, List<(string, string)> headers)
        {
            MockJsonRestClient jsonClient = new MockJsonRestClient(Settings);
            //Mocking expectations
            RequestMocks mocks = jsonClient.InitializeResponse(true, "everythingOk", headers, parameters);

            //Getting Result
            dynamic dynamicResponse = jsonClient.Object.ExecuteHTTPRequest("test_base_endpoint", "test_endpoint",  RestSharp.Method.GET, headers, parameters);
            mocks.MockRestSharpRestClient.VerifyAll();
            Assert.NotNull(dynamicResponse);
            Assert.NotNull(dynamicResponse.Message);
            Assert.Equal("everythingOk", Convert.ToString(dynamicResponse.Message));

            //Verify mock calls                        
            jsonClient.VerifyAll();
        }

        [Theory(DisplayName = "ExecuteHTTPRequestAsync - Create Correct Request Test")]
        [MemberData(nameof(ExecuteHttpRequestTestData))]
        public async void ExecuteHTTPRequestAsync_SuccessfulResponse_Test(List<(string, string, ParameterType)> parameters, List<(string, string)> headers)
        {
            MockJsonRestClient jsonClient = new MockJsonRestClient(Settings);
            //Mocking expectations
            RequestMocks mocks = jsonClient.InitializeResponseAsync(true, "everythingOk", headers, parameters);

            //Getting Result
            dynamic dynamicResponse = await jsonClient.Object.ExecuteHTTPRequestAsync("test_base_endpoint", "test_endpoint", RestSharp.Method.GET, headers, parameters);
            mocks.MockRestSharpRestClient.VerifyAll();
            Assert.NotNull(dynamicResponse);
            Assert.NotNull(dynamicResponse.Message);
            Assert.Equal("everythingOk", Convert.ToString(dynamicResponse.Message));

            //Verify mock calls                        
            jsonClient.VerifyAll();
        }

        [Fact(DisplayName = "Get New Client - Initialization Test")]
        public void GetNewClient_Initialization_Test()
        {
            JsonRestClient client = new JsonRestClient();
            Assert.NotNull(Settings.GetSetting("zype_base_endpoint"));
            RestClient internalRestClient =  client.GetNewClient(Settings.GetSetting("zype_base_endpoint"));
            Assert.NotNull(internalRestClient);
            Assert.Equal(Settings.GetSetting("zype_base_endpoint"), internalRestClient.BaseUrl.AbsoluteUri);
        }

    }
}
