﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Mocks
{
    public class ParametersComparer : IEqualityComparer<(string key, string value, ParameterType type)>
    {
        public bool Equals((string key, string value, ParameterType type) x, (string key, string value, ParameterType type) y)
        {
            return (x.key == y.key) && (x.value == y.value) && (x.type == y.type);
        }

        public int GetHashCode((string key, string value, ParameterType type) obj)
        {
            int hash = 13;
            hash = (hash * 7) + ((obj.key != null) ? obj.key.GetHashCode() : 0);
            hash = (hash * 7) + ((obj.value != null) ? obj.value.GetHashCode() : 0);
            hash = (hash * 7) + obj.type.GetHashCode();
            return hash;
        }
    }    

}
