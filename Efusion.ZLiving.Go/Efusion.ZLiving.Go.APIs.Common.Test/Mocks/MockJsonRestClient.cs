﻿using Efusion.ZLiving.Go.APIs.Common.Services;
using Moq;
using RestSharp;
using System.Collections.Generic;
using System.Dynamic;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Mocks
{

    public class MockJsonRestClient : Mock<JsonRestClient>
    {
        public SettingsFixture Settings { get; set; }

        public MockJsonRestClient(SettingsFixture settings) {

            this.Settings = settings;
        }

        public RequestMocks InitializeResponse(bool result, string contenText, IList<(string, string)> headers, IList<(string, string, ParameterType)> parameters) {

            MockRestSharpRestClient rsRestClient = new MockRestSharpRestClient();
            MockIRestResponse response = new MockIRestResponse();

            //Response will be successful
            response.MockIsSuccessful(result);

            //Mocking Response Content
            dynamic responseContent = new ExpandoObject();
            responseContent.Message = contenText;
            response.MockContent(responseContent);

            //Mock RestSharpClient
            this.MockGetNewClient(rsRestClient.Object);

            var requestHeaders = new List<(string, string)>(headers?? new List<(string,string)>())
            {
                ("accept", "application/json"),
                ("content-type", "application/json")
            };

            rsRestClient.MockExecute("test_base_endpoint", "test_endpoint", RestSharp.Method.GET, requestHeaders, parameters, response.Object);
            return new RequestMocks(rsRestClient, response);
        }


        public RequestMocks InitializeResponseAsync(bool result, string contenText,IList<(string, string)> headers, IList<(string, string, ParameterType)> parameters)
        {

            MockRestSharpRestClient rsRestClient = new MockRestSharpRestClient();
            MockIRestResponse response = new MockIRestResponse();

            //Response will be successful
            response.MockIsSuccessful(result);

            //Mocking Response Content
            dynamic responseContent = new ExpandoObject();
            responseContent.Message = contenText;
            response.MockContent(responseContent);

            //Mock RestSharpClient
            this.MockGetNewClient(rsRestClient.Object);

            var requestHeaders = new List<(string, string)>(headers ?? new List<(string, string)>())
            {
                ("accept", "application/json"),
                ("content-type", "application/json")
            };

            rsRestClient.MockExecuteAsync("test_base_endpoint", "test_endpoint", RestSharp.Method.GET, requestHeaders, parameters, response.Object);
            return new RequestMocks(rsRestClient, response);
        }

        public void MockGetNewClient(RestClient output)
        {
            Setup(x => x.GetNewClient(It.IsAny<string>())).Returns(output);
        }
    }


    public class RequestMocks
    {

        public MockRestSharpRestClient MockRestSharpRestClient { get; set; }
        public MockIRestResponse MockIRestResponse { get; set; }

        public RequestMocks(MockRestSharpRestClient mockRestSharpRestClient, MockIRestResponse mockIRestResponse)
        {
            this.MockIRestResponse = mockIRestResponse;
            this.MockRestSharpRestClient = mockRestSharpRestClient;
        }
    }
}
