﻿using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Moq;
using RestSharp;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Linq;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Mocks
{
    public class MockRestClient : Mock<Common.Services.IRestClient>
    {

        public void MockExecuteHTTPRequestAsync(string endpoint, Task<dynamic> output)
        {
            Setup(x => x.ExecuteHTTPRequestAsync(
            It.IsAny<string>(),
            It.Is<string>(i => i == endpoint),
            It.IsAny<Method>(),
            It.IsAny<IList<(string Key, string Value)>>(),
            It.IsAny<IList<(string Key, string Value, ParameterType ParamType)>>(),
            It.IsAny<string>()
            )).Returns(output);
        }

        public void MockExecuteHTTPRequestAsyncException(string endpoint, HttpStatusCode statusCode, dynamic responseContent = null)
        {
            MockIRestResponse restResponse = new MockIRestResponse();
            restResponse.MockStatus(statusCode);
            restResponse.MockContent(responseContent);
            RestHTTPException exception = new RestHTTPException(restResponse.Object);

            Setup(x => x.ExecuteHTTPRequestAsync(
            It.IsAny<string>(),
            It.Is<string>(i => i == endpoint),
            It.IsAny<Method>(),
            It.IsAny<IList<(string Key, string Value)>>(),
            It.IsAny<IList<(string Key, string Value, ParameterType ParamType)>>(),
            It.IsAny<string>()
            )).Throws(exception);

        }

        public void MockExecuteHTTPRequestException(string endpoint, HttpStatusCode statusCode, dynamic responseContent = null)
        {
            MockIRestResponse restResponse = new MockIRestResponse();
            restResponse.MockStatus(statusCode);
            restResponse.MockContent(responseContent);
            RestHTTPException exception = new RestHTTPException(restResponse.Object);

            Setup(x => x.ExecuteHTTPRequest(
            It.IsAny<string>(),
            It.Is<string>(i => i == endpoint),
            It.IsAny<Method>(),
            It.IsAny<IList<(string Key, string Value)>>(),
            It.IsAny<IList<(string Key, string Value, ParameterType ParamType)>>(),
            It.IsAny<string>()
            )).Throws(exception);

        }


        public void MockExecuteHTTPRequestAsync(string baseEndpoint, string endpoint, Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters, Task<dynamic> output)
        {
            Setup(x => x.ExecuteHTTPRequestAsync(
                It.Is<string>(i => i == baseEndpoint),
                It.Is<string>(i => i == endpoint),
                It.Is<Method>(i => i == method),
                It.Is<IList<(string Key, string Value)>>(i => VerifyHeaders(i, headers)),
                It.Is<IList<(string Key, string Value, ParameterType ParamType)>>(i => VerifyParameters(i, parameters)),
                It.IsAny<string>()
                )).Returns(output);
        }
     
        public void MockExecuteHTTPRequest(string baseEndpoint, string endpoint, Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters, dynamic output)
        {
            Setup(x => x.ExecuteHTTPRequest(
                It.Is<string>(i => i == baseEndpoint),
                It.Is<string>(i => i == endpoint),                
                It.Is<Method>(i => i == method),
                It.Is<IList<(string Key, string Value)>>(i => VerifyHeaders(i, headers)),
                It.Is<IList<(string Key, string Value, ParameterType ParamType)>>(i => VerifyParameters(i, parameters)),
                It.IsAny<string>()
                )).Returns((object)output);
        }

        private bool VerifyHeaders(IList<(string Key, string Value)> used, IList<(string Key, string Value)> expected)
        {
            IList<(string Key, string Value)> headersUsed = used ?? new List<(string Key, string Value)>();
            IList<(string Key, string ValueParamType)> headersExpected = expected ?? new List<(string Key, string Value)>();

            foreach ((string key, string value) in headersExpected)
            {
                if (!headersUsed.Contains((key, value)))
                {
                    return false;
                }
            } 

            return true;
        }

        private bool VerifyParameters(IList<(string Key, string Value, ParameterType ParamType)> used, IList<(string Key, string Value, ParameterType ParamType)> expected)
        {
            IList<(string Key, string Value, ParameterType ParamType)> paramsUsed = used ?? new List<(string Key, string Value, ParameterType ParamType)>();
            IList<(string Key, string Value, ParameterType ParamType)> paramsExpected = expected ?? new List<(string Key, string Value, ParameterType ParamType)>();

            //Intersection should be everything
            IEnumerable<(string Key, string Value, ParameterType ParamType)> intersection = paramsUsed.Intersect(paramsExpected, new ParametersComparer());

            return (intersection.Count() == paramsUsed.Count);

        }        
    }
}
