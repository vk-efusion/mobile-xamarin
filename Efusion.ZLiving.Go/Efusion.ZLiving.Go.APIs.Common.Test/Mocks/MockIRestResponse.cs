﻿using Moq;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Mocks
{
    public class MockIRestResponse : Mock<IRestResponse>
    {

        public void MockIsSuccessful(bool isSuccessful)
        {
            SetupGet(x => x.IsSuccessful).Returns(isSuccessful);
        }

        public void MockStatus(HttpStatusCode statusCode)
        {
            SetupProperty(x => x.StatusCode, statusCode);
        }

        public void MockContent(dynamic content)
        {
            string c = (content == null) ? String.Empty : JsonConvert.SerializeObject(content);
            SetupProperty(x => x.Content, c);
        }
    }
}
