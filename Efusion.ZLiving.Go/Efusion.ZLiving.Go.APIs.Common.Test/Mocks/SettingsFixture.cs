﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Mocks
{
    public class SettingsFixture : IDisposable, IBaseSettings, IGlobalDefaultSettings
    {
        public IDictionary<string, string> DefaultSettings { get; set; }
        
        public SettingsFixture()
        {
            this.DefaultSettings = new Dictionary<string, string>
            {                
                { "zliving_base_endpoint", "http://test.zlivinggo-api.azurewebsites.net/" },
                { "zliving_stg_base_endpoint", "http://test.zlivinggo-api-stg.azurewebsites.net/" },
                { "zype_base_endpoint", "https://test.api.zype.com/" },
                { "zype_player_base_endpoint", "https://test.player.zype.com/" },
                { "login_base_endpoint", "https://test.login.zype.com/" },
                { "facebook_base_endpoint", "https://test.www.facebook.com/" },                
                { "ios_app_key", "test_8oWgLQQUcRX3ZIsNBqMIfDlkdBWBBh-b7ljRJD_Ww7EFalWnPnd4FpVqS6UfrZ9P" },
                { "android_app_key", "test_8oWgLQQUcRX3ZIsNBqMIfDlkdBWBBh-b7ljRJD_Ww7EFalWnPnd4FpVqS6UfrZ9P" },
                { "api_key", "test_BOV2F4UPXvAG7f-Zf1nr0iAMScGLyu7gVA58atMX9T8veeziWfdkYzynqsMDvCn2" },
                { "root_playlist_id", "test_5a5d71b272289b154601c695" },
                { "recommendations_endpoint", "test_recommendations" },
                { "link_device_endpoint", "test_pin/link" },
                { "acquire_link_device_pin_endpoint", "pin/acquire" },
                { "playlists_endpoint", "test_playlists" },
                { "player_endpoint", "test_embed" },
                { "videos_endpoint", "test_videos" },
                { "zobject_endpoint", "test_zobjects" },
                { "login_endpoint", "test_oauth/token" },
                { "facebook_login_endpoint", "test_dialog/oauth" },
                { "registration_endpoint", "test_prospect/register" },
                { "registration_status_endpoint", "test_prospect/status" },
                { "resend_activation_endpoint", "test_prospect/register/resend" },
                { "forgot_password_endpoint", "test_forgot-password" },
                { "change_password_endpoint", "test_change-password" },
                { "consumer_endpoint", "test_consumers" },
                { "app_center_droid_key", "test_fb8ec16e-6381-4e46-ac88-7bf8c1ad1e84" },
                { "app_center_ios_key", "test_f06caa5d-eb39-438b-b948-294b64566b47" },
                { "client_id", "test_7e50c4fc642d211fc4f949e64a96f27349e5c35e2499a02f0b08129b2c491592" },
                { "client_secret", "test_ed539e83e775b7f187be8f9fb498ce1f1f6be33fc7dcd11481ec38af3afec0fe" },
                { "facebook_client_id", "644934455688098" },
                { "facebook_authorise_url", "https://m.facebook.com/dialog/oauth/" },
                { "facebook_redirect_url", "https://www.facebook.com/connect/login_success.html" },
                { "google_client_id", "459131839547-l4fk2anc1frlgsb6gcdael50cfuk1qtk.apps.googleusercontent.com"},
                { "google_authorise_url", "https://accounts.google.com/o/oauth2/auth" },
                { "google_redirect_url", "com.googleusercontent.apps.l4fk2anc1frlgsb6gcdael50cfuk1qtk-459131839547:/oauth2redirect" },
                { "google_profile_url", "https://www.googleapis.com/oauth2/v4/token" },
            };
        }

        public void Dispose()
        {
            
        }

        public string GetDefaultSetting(string key)
        {
            if (!this.DefaultSettings.ContainsKey(key))
                return String.Empty;
            
                return this.DefaultSettings[key];            
        }

        public void SetDefaultSetting(string key, string value)
        {
            this.DefaultSettings[key] = value;
        }

        public string GetSetting(string key)
        {
            if (!this.DefaultSettings.ContainsKey(key))
                return String.Empty;
            
                return this.DefaultSettings[key];            
        }

        public void SetSetting(string key, string value)
        {
            this.DefaultSettings[key] = value;
        }
    }
}
