﻿using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Mocks
{
    public class MockRestSharpRestClient: Mock<RestClient>
    {
        public void MockExecute(string baseEndpoint, string endpoint, Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters, IRestResponse output)
        {
            Setup(x => x.Execute(It.Is<RestRequest>(i => VerifyRequest(i,baseEndpoint,endpoint,method,headers,parameters)))
            ).Returns(output);
        }

        public void MockExecuteAsync(string baseEndpoint, string endpoint,Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters, IRestResponse output)
        {
            Setup(x => x.ExecuteTaskAsync(It.Is<RestRequest>(i => VerifyRequest(i, baseEndpoint, endpoint, method, headers, parameters)))
            ).Returns(Task.FromResult<IRestResponse>(output));
        }

        public bool VerifyRequest(RestRequest request, string baseEndpoint, string endpoint, Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters) {

            if (request == null) {
                return false;
            }

            //    bool baseEndpointOk = request.Resource.Equals((appKey == null) ? $"{endpoint}" : $"{endpoint}?app_key={appKey}");
            bool baseEndpointOk = request.Resource.Equals(endpoint);
            bool methodOk = request.Method.Equals(method);

            IList<(string Key, string Value, ParameterType ParamType)> newParams = parameters?? new List<(string Key, string Value, ParameterType ParamType)>();

            if (headers != null) {

                foreach ((string key, string value) in headers) {
                    newParams.Add((key, value, ParameterType.HttpHeader));
                }
            } 

            bool parametersOk = VerifyParameters(request.Parameters, newParams);

            return baseEndpointOk && methodOk && parametersOk;
        }
       

        private bool VerifyParameters(IList<Parameter> used, IList<(string Key, string Value, ParameterType ParamType)> expected)
        {
            IList<(string Key, string Value, ParameterType ParamType)> paramsUsed = (used == null) ? new List<(string Key, string Value, ParameterType ParamType)>() : used.Select<Parameter, (string, string, ParameterType)>(x => (x.Name, x.Value as String, x.Type)).ToList();
            IList<(string Key, string Value, ParameterType ParamType)> paramsExpected = expected ?? new List<(string Key, string Value, ParameterType ParamType)>();

            if (paramsUsed.Count != paramsExpected.Count) {
                return false;
            }

            //Intersection should be everything
            IEnumerable<(string Key, string Value, ParameterType ParamType)> intersection = paramsUsed.Intersect(paramsExpected, new ParametersComparer());            
            return (intersection.Count() == paramsUsed.Count);
        }
       
    }
}
