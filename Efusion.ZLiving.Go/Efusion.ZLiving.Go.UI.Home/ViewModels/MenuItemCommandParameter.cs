﻿using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.UI.Home.ViewModels
{
    public class MenuItemCommandParameter
    {
        public String Key { get; set; } = null;
        public NavigationParameters NavigationParameters { get;set;}
    }
}
