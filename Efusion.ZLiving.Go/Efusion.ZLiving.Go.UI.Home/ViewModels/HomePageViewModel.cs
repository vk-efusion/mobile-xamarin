﻿using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Efusion.ZLiving.Go.UI.Content.ViewModels;
using Efusion.ZLiving.Go.UI.VOD.ViewModels;
using Prism.Navigation;
using System.Threading.Tasks;
using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Common.Contracts;

namespace Efusion.ZLiving.Go.UI.Home.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        private SlidingBannerViewModel banner;
        private PlaylistsViewModel playlists;        
        public SlidingBannerViewModel Banner { get => banner; set => SetProperty(ref banner, value); }
        public PlaylistsViewModel Playlists { get => playlists; set => SetProperty(ref playlists, value); }


        public HomePageViewModel(FooterViewModel footerViewModel, SlidingBannerViewModel bannerViewModel, PlaylistsViewModel playlistsViewModel, INavigationService navigationService, ISessionManager sessionManager, IBaseSettings settings) : base(footerViewModel, sessionManager, navigationService,settings)
        {
            this.Banner = bannerViewModel;
            this.Playlists = playlistsViewModel;
        }

        public async override void OnNavigatingTo(NavigationParameters parameters)
        {
            this.IsBusy = true;
            base.OnNavigatingTo(parameters);
            await Task.WhenAll(this.Banner.Load(), this.Playlists.LoadRootPlaylist());
            this.IsBusy = false;
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {            
            base.OnNavigatedTo(parameters);            
        }
    }
}
