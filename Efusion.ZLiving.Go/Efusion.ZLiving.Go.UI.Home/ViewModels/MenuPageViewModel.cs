﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Content.Services;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Microsoft.AppCenter.Crashes;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Home.ViewModels
{
    public class MenuPageViewModel: BaseViewModel
    {
        
        public IContentService ContentService { get; set; }
        public IVideoOnDemandService VideoOnDemandService { get; set; }
        public ICommand NavigationCommand { get; }
        public ICommand SignInCommand { get; }
        public ICommand SignOutCommand { get; }

        public ObservableCollection<MenuItem> MenuItems { get; } = new ObservableCollection<MenuItem>();        

        public MenuPageViewModel(ISessionManager sessionManager, IContentService contentService, IVideoOnDemandService vodService, INavigationService navigationService, IBaseSettings settings) : base(sessionManager, navigationService,settings)
        {
            this.ContentService = contentService;
            this.VideoOnDemandService = vodService;
            this.NavigationCommand = new Command<MenuItemCommandParameter>(NavigateCommand);
            this.SignInCommand = new Command(LogInCommand);
            this.SignOutCommand = new Command(LogOutCommand);            
        }

        
        public async override void OnNavigatingTo(NavigationParameters parameters) {
            base.OnNavigatingTo(parameters);          
            await Load();
        }


        public async Task Load()
        {
            try
            {
                this.IsBusy = true;

                bool catchUpFound = false;
                this.MenuItems.Clear();
                this.MenuItems.Add(new MenuItem() { Text = "Home", CommandParameter = new MenuItemCommandParameter() { Key = "home" }, Command = NavigationCommand });

                if (this.SessionManager.AuthenticationState.Equals(Common.Enums.AuthenticationState.Authenticated))
                {
                    MenuItems.Add(new MenuItem() { Text = "Link Device", CommandParameter = new MenuItemCommandParameter() { Key = "link-device" }, Command = NavigationCommand });
                }

                dynamic playlists = await this.VideoOnDemandService.GetRootPlaylistsAsync();
                NavigationParameters np;

                foreach (var playlist in playlists.response)
                {
                    np = new NavigationParameters
                {
                    { "keywords", playlist._keywords },
                    { "title", Convert.ToString(playlist.title)},
                    { "id", Convert.ToString(playlist._id)}
                };


                    MenuItems.Add(new MenuItem() { Text = Convert.ToString(playlist.title), CommandParameter = new MenuItemCommandParameter() { Key = "category", NavigationParameters = np }, Command = NavigationCommand });

                    if (this.SessionManager.AuthenticationState.Equals(Common.Enums.AuthenticationState.Authenticated) & !catchUpFound)
                    {
                        foreach (var keyword in playlist._keywords)
                        {
                            var key = Convert.ToString(keyword);
                            if (key.Equals("live") | key.Equals("catch"))
                            {
                                np = new NavigationParameters
                            {
                                { "title", "Recommendations"},
                                { "id", "recommendations"}
                            };

                                MenuItems.Add(new MenuItem() { Text = "Recommendations", CommandParameter = new MenuItemCommandParameter() { Key = "category", NavigationParameters = np }, Command = NavigationCommand });
                                catchUpFound = true;
                                break;
                            }
                        }
                    }

                }

                MenuItems.Add(new MenuItem() { Text = "Contact Us", CommandParameter = new MenuItemCommandParameter() { Key = "content", NavigationParameters = new NavigationParameters("title=Contact Us&key=contact-us") }, Command = NavigationCommand });
                MenuItems.Add(new MenuItem() { Text = "About Us", CommandParameter = new MenuItemCommandParameter() { Key = "content", NavigationParameters = new NavigationParameters("title=About Us&key=about-us") }, Command = NavigationCommand });
                MenuItems.Add(new MenuItem() { Text = "Terms And Conditions", CommandParameter = new MenuItemCommandParameter() { Key = "content", NavigationParameters = new NavigationParameters("title=Terms and Conditions&key=terms-and-conditions") }, Command = NavigationCommand });
                MenuItems.Add(new MenuItem() { Text = "FAQ", CommandParameter = new MenuItemCommandParameter() { Key = "content", NavigationParameters = new NavigationParameters("title=FAQ&key=faq") }, Command = NavigationCommand });

                this.PageState = Common.States.PageState.Success;
            }
            catch (Exception ex) {
                //this.Message = "An unexpected exception has occured. Plese try again later.";
                //this.PageState = Common.States.PageState.UnexpectedException;
                Crashes.TrackError(new Exception("unexpected-exception", ex));
            }
            finally {
                this.IsBusy = false;
            }
        }


        public async void LogInCommand()
        {            
            await this.NavigationService.NavigateAsync("login");
        }

        public async void LogOutCommand()
        {
            this.SessionManager.LogOut();
            await this.Load();
        }

        public async void NavigateCommand(MenuItemCommandParameter parameter) {

            await this.NavigationService.NavigateAsync(parameter.Key, parameter.NavigationParameters);            
        }     
        
        
    }
}
