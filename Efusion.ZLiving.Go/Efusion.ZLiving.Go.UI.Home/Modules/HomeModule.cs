﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.Content.Services;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Managers;
using Efusion.ZLiving.Go.UI.Home.Views;
using Prism.Ioc;
using Prism.Modularity;

namespace Efusion.ZLiving.Go.UI.Home.Modules
{
    public class HomeModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
      
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IBaseSettings, Settings>();
            containerRegistry.RegisterSingleton<IRestClient, JsonRestClient>();
            containerRegistry.RegisterSingleton<IVideoOnDemandService, VideoOnDemandService>();
            containerRegistry.RegisterSingleton<IContentService, ContentService>();
            containerRegistry.RegisterSingleton<ISessionManager, SessionManager>();

            containerRegistry.RegisterForNavigation<MenuPage>("menu");
            containerRegistry.RegisterForNavigation<HomePage>("home");
        }
    }
}
