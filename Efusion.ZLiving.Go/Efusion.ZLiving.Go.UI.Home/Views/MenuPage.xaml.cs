﻿using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.Home.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : ZLivingBasePage
    {
        
        public MenuPage (ISessionManager sessionManager):base(sessionManager,false)
		{
			InitializeComponent ();            
        }
               
    }
}