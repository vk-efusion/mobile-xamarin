﻿using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.Home.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ZLivingBasePage
    {
        public HomePage(ISessionManager sessionManager) : base(sessionManager, false)
        {
            InitializeComponent();        
        }        
    }
}