﻿using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.EPG.Services
{
    public interface IElectronicProgrammingGuideService
    {
        Task<dynamic> GetCatchUpPlaylist(string id, int page = 1);
        Task<dynamic> GetProgrammingGuide(string pgId, int page = 1);

    }
}
