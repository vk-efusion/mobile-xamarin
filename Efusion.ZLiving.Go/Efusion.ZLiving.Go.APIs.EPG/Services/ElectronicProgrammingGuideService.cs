﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.EPG.Services
{
    public class ElectronicProgrammingGuideService : ZLivingGoBaseService, IElectronicProgrammingGuideService
    {
        public override string ServiceName => "electronic-programming-guide";

        public ElectronicProgrammingGuideService(Common.Services.IRestClient jsonClient, IBaseSettings settings) : base(jsonClient, settings)
        {

        }

        public async Task<dynamic> GetCatchUpPlaylist(string id,int page = 1)
        {            
            var parameters = new List<(string, string, ParameterType)>()
            {
              ("parent_id",id,ParameterType.QueryString),
              ("sort","priority",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              ("per_page","5",ParameterType.QueryString),
              ("page",page.ToString(),ParameterType.QueryString),
              this.GetAuthenticationParameter()
            };

            dynamic httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"),  Method.GET, null, parameters);
            return httpResponse;            
        }

        public async Task<dynamic> GetProgrammingGuide(string pgId, int page = 1)
        {            
            var parameters = new List<(string, string, ParameterType)>()
            {
              ("per_page","15",ParameterType.QueryString),
              ("page",page.ToString(),ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              this.GetAuthenticationParameter()
            };

            string playlistsEndpoint = Settings.GetSetting("playlists_endpoint");
            dynamic httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{playlistsEndpoint}/{pgId}/videos", Method.GET, null, parameters);
            return httpResponse;
        }
    }
}
