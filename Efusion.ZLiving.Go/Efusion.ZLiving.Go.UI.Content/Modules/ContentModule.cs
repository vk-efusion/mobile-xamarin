﻿using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.Content.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Managers;
using Efusion.ZLiving.Go.UI.Content.Views;
using Prism.Ioc;
using Prism.Modularity;

namespace Efusion.ZLiving.Go.UI.Content.Modules
{
    public class ContentModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IRestClient, JsonRestClient>();
            containerRegistry.RegisterSingleton<ISessionManager, SessionManager>();
            containerRegistry.RegisterSingleton<IContentService, ContentService>();
            containerRegistry.RegisterForNavigation<ZContentPage>("content");
        }
    }
}
