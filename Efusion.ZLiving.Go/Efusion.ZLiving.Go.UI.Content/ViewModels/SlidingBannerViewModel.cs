﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Content.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Efusion.ZLiving.Go.UI.VOD.Models;
using FFImageLoading.Forms;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Content.ViewModels
{
    public class SlidingBannerViewModel : BaseViewModel
    {
        private int position;

        public INavigation Navigation { get; set; }
        public IContentService ContentService { get; set; }
        public ObservableCollection<View> Source { get; } = new ObservableCollection<View>();
        public int Position { set => SetProperty(ref this.position, value); get => position; }

        public SlidingBannerViewModel(IContentService contentService, INavigationService navigationService, ISessionManager sessionManager, IBaseSettings settings) : base(sessionManager, navigationService, settings) {
            this.ContentService = contentService;

            Device.StartTimer(TimeSpan.FromSeconds(4), (Func<bool>)(() =>
            {
                this.Position = (this.Position + 1) % ((this.Source.Count == 0) ? 1 : this.Source.Count);
                return true;
            }));

        }

        public async Task Load()
        {
            this.Position = 0;
            this.Source.Clear();

            dynamic slider = await this.ContentService.GetBannerContent();
            CachedImage image = null;


            foreach (var sliderItem in slider)
            {
                foreach (var picture in sliderItem.pictures)
                {
                    if (Convert.ToString(picture.title) == "fhd")
                    {
                        image = new CachedImageIdetity()
                        {
                            Source = ImageSource.FromUri(new Uri(Convert.ToString(picture.url))),
                            Aspect = Aspect.AspectFill,
                            BackgroundColor = Color.Black,
                            DownsampleToViewSize = true,
                            Identity = Convert.ToString(sliderItem._id),
                            VideoId = (sliderItem.video_ids.Count > 0 )? Convert.ToString(sliderItem.video_ids[0]) : String.Empty
                        };

                        image.GestureRecognizers.Add(this.GetTapGestureRecognizer());
                        this.Source.Add(image);
                        break;
                    }
                }
            }
        }
        

        private TapGestureRecognizer GetTapGestureRecognizer() {

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();

            tapGestureRecognizer.Tapped += (s, e) => {
                var imageSender = (CachedImageIdetity)s;

                Video video = new Video(this.NavigationService)
                {
                    Id = imageSender.VideoId,
                    PlayListID = imageSender.Identity,                 
                };

                video.ViewResource();
            };

            return tapGestureRecognizer;
        }
    }

    public class CachedImageIdetity : CachedImage
    {
        public string Identity { get; set; }
        public string VideoId { get; set; }
    }
}
