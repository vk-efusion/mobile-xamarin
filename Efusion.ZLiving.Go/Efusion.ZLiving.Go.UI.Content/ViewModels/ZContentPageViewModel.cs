﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Content.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Prism.Navigation;
using System;

namespace Efusion.ZLiving.Go.UI.Content.ViewModels
{
    public class ZContentPageViewModel : BaseViewModel
    {
        private string content;        
        private string key;

        public IContentService ContentService { get; set; }

        public string Content { get => content; private set => SetProperty(ref content, value); }        
        public string Key { get => key; private set => SetProperty(ref key, value); }

        public ZContentPageViewModel(FooterViewModel footerViewModel, IContentService contentService, INavigationService navigationService, ISessionManager sessionManager, IBaseSettings settings) : base(footerViewModel, sessionManager, navigationService, settings)
        {
            this.ContentService = contentService;
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            this.Title = parameters.GetValue<string>("title");
            this.Key = parameters.GetValue<string>("key");
            dynamic content = this.ContentService.GetContent(Key);
            this.Content =  Convert.ToString(content.Content);
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
           

        }
    }
}
