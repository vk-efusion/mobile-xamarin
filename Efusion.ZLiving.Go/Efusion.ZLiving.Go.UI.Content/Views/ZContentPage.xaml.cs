﻿using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.Content.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ZContentPage : ZLivingBasePage
	{        
		public ZContentPage (ISessionManager sessionManager):base(sessionManager)
		{
			InitializeComponent ();            
		}
	}
}