﻿using Efusion.ZLiving.Go.APIs.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.VOD.Services
{
    public interface IVideoOnDemandService
    {        
        Task<dynamic> GetVideos(string playlistId, int page = 1);
        Task<dynamic> GetVideo(string videoId);
        Task<dynamic> GetPlayer(string videoId);
        Task<dynamic> GetPlaylistsAsync(string parentId, int page = 1);
        dynamic GetPlaylists(string parentId, int page = 1);
        dynamic GetRootPlaylists(int page = 1);
        Task<dynamic> GetRootPlaylistsAsync(int page = 1);
        Task<dynamic> GetRecommendations(int page = 1);
        dynamic TrackPosition();
        Task<dynamic> GetShow(string showId);
        Task<dynamic> SearchVideos(string query, int page = 1);
        Task<dynamic> GetContinueWatchingVideos();        
    }
}
