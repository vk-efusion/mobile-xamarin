﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Enums;
using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.VOD.Exceptions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.VOD.Services
{
    public class VideoOnDemandService : ZLivingGoBaseService, IVideoOnDemandService
    {
        public override string ServiceName => "video-on-demand";

        public VideoOnDemandService(Common.Services.IRestClient jsonClient,IBaseSettings settings) : base(jsonClient, settings)
        {

        }

        public async Task<dynamic> GetRecommendations(int page = 1)
        {
            //var parameters = new List<(string Key, string Value, ParameterType ParamType)>()
            //{
            //  ("access_token", userToken, ParameterType.QueryString)
            //};

            //return this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zliving_base_endpoint"), Settings.GetSetting("recommendations_endpoint"), Settings.GetSetting("app_key"), Method.GET, null, parameters);

            return await this.GetVideos("5a5eaa663195971531000849",page); // Will be replaced with the recommendations API when it starts working
        }

        public async Task<dynamic> GetPlayer(string videoId) {

            if (String.IsNullOrEmpty(videoId)) {                
                var ex = new ArgumentNullException("videoId");
                this.TrackError(ex, "get-video", "VideoId shouldn't be null or empty");
                throw ex;
            }
            
            dynamic httpResponse = null;

            try
            {
                var parameters = new List<(string, string, ParameterType)>()
                {
                     this.GetAuthenticationParameter(),                
                };

                httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{Settings.GetSetting("player_endpoint")}/{videoId}.json", Method.GET, null, parameters, this.UserAgent);
            }
            catch (RestHTTPException restException)
            {
                if ((restException.Response.StatusCode == System.Net.HttpStatusCode.Forbidden) || (restException.Response.StatusCode == System.Net.HttpStatusCode.BadRequest))
                {
                    dynamic errorMessage = JsonConvert.DeserializeObject(restException.Response.Content);
                    string strMessage = Convert.ToString(errorMessage.message);
                    strMessage = (strMessage == "content_rule_blackout") ? "Cannot play video due to a content rule blackout" : strMessage;
                    var playerEx = new PlayerNotAllowedException(strMessage);
                    this.TrackError(playerEx, "get-player", strMessage, $"video-id={videoId}");
                    throw playerEx;
                }

                this.TrackError(restException, "get-player", restException.Response.StatusCode.ToString(), $"videoId={videoId}");
                throw restException;                
            }

            if (Convert.ToInt32(httpResponse.response.body.outputs.Count) == 0)
            {
                var ex = new ResourceNotFoundException($"Player Not Found for Video: {videoId}");
                this.TrackError(ex, "get-player", "Player Not Found", $"video-id={videoId}");
                throw ex;
            }

            //return httpResponse;
              return httpResponse.response.body.outputs[0];
        }

        public async Task<dynamic> GetVideo(string videoId)
        {
            if (String.IsNullOrEmpty(videoId)){
                var ex = new ArgumentNullException("videoId");
                this.TrackError(ex, "get-video", "VideoId shouldn't be null or empty");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("active","true",ParameterType.QueryString),
              this.GetAuthenticationParameter()
            };

            dynamic httpResponse = null;
            try {
                string videosEndpoint = Settings.GetSetting("videos_endpoint");
                httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{videosEndpoint}/{videoId}", Method.GET, null, parameters);
                httpResponse.response.player = await this.GetPlayer(videoId);
                return httpResponse.response;
            }
            catch (RestHTTPException restException) {
                this.TrackError(restException, "get-video", restException.Response.StatusCode.ToString(), $"videoId={videoId}");
                throw restException;
            }            
        }
    
        public async Task<dynamic> GetVideos(string playlistId, int page = 1) {

            if (String.IsNullOrEmpty(playlistId))
            {                
                var ex = new ArgumentNullException("playlistId");
                this.TrackError(ex, "get-videos", "playlistId shouldn't be null or empty");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {                            
              ("per_page","5",ParameterType.QueryString),
              ("page",page.ToString(),ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
               this.GetAuthenticationParameter()
            };
            
            try { 
                string playlistEndpoint = Settings.GetSetting("playlists_endpoint");
                dynamic httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{playlistEndpoint}/{playlistId}/videos" , Method.GET, null, parameters);
                return httpResponse;
            }
            catch (RestHTTPException restException) {
                this.TrackError(restException, "get-videos", restException.Response.StatusCode.ToString(), $"playlist-id={playlistId}");
                throw restException;
            }
        }
    
        public async Task<dynamic> GetPlaylistsAsync(string parentId, int page = 1) {

            if (String.IsNullOrEmpty(parentId)) {                
                var ex = new ArgumentNullException("parentId");
                this.TrackError(ex, "get-playlists-async", "parentId shouldn't be null or empty");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("parent_id",parentId,ParameterType.QueryString),
              ("sort","priority",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              ("per_page","5",ParameterType.QueryString),
              ("page",page.ToString(),ParameterType.QueryString),
              this.GetAuthenticationParameter()
            };

            try
            {
                dynamic httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"), Method.GET, null, parameters);
                return httpResponse;
            }        
            catch (RestHTTPException restException) {
                this.TrackError(restException, "get-playlists-async", restException.Response.StatusCode.ToString(), $"parent-id={parentId}&active=true&sort=priority");
                throw restException;
            }
}
        
        public dynamic GetRootPlaylists(int page = 1)
        {
            return GetPlaylists(Settings.GetSetting("root_playlist_id"),page);
        }

        public dynamic GetPlaylists(string parentId, int page = 1)
        {
            if (String.IsNullOrEmpty(parentId))
            {
                var ex = new ArgumentNullException("parentId");
                this.TrackError(ex, "get-playlists", "parentId shouldn't be null or empty");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("parent_id",parentId,ParameterType.QueryString),
              ("sort","priority",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              ("per_page","5",ParameterType.QueryString),
              ("page",page.ToString(),ParameterType.QueryString),
              this.GetAuthenticationParameter()
            };

            try
            {
                dynamic httpResponse = this.Client.ExecuteHTTPRequest(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"), Method.GET, null, parameters);
                return httpResponse;
            }
            catch (RestHTTPException restException)
            {
                this.TrackError(restException, "get-playlists", restException.Response.StatusCode.ToString(), $"parent-id={parentId}&active=true&sort=priority");
                throw restException;
            }
}

        public async Task<dynamic> GetRootPlaylistsAsync(int page = 1)
        {
            return await GetPlaylistsAsync(Settings.GetSetting("root_playlist_id"), page);
        }

        public async Task<dynamic> GetShow(string showId)
        {
            if (String.IsNullOrEmpty(showId)) {                                
                var ex = new ArgumentNullException("showId");
                this.TrackError(ex, "get-show", "showId shouldn't be null or empty");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("id",showId,ParameterType.QueryString),              
              ("active","true",ParameterType.QueryString),
              this.GetAuthenticationParameter() 
            };

            dynamic httpResponse = null;

            try
            {
                httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"), Method.GET, null, parameters);
            }
            catch (RestHTTPException restException)
            {
                this.TrackError(restException, "get-show", restException.Response.StatusCode.ToString(), $"show-id={showId}&active=true");
                throw restException;
            }

            if (Convert.ToInt32(httpResponse.response.Count) == 0) {                
                var ex = new ShowNotFoundException($"Show with ID: {showId} was not found");
                this.TrackError(ex, "get-show", "Show not found", $"show-id={showId}&active=true");
                throw ex;
            }

            return httpResponse.response[0];
        }
        
        public async Task<dynamic> SearchVideos(string query, int page = 1)
        {
            var parameters = new List<(string, string, ParameterType)>()
            {              
              ("active","true",ParameterType.QueryString),
              ("q",query,ParameterType.QueryString),
              ("category[Video Type]","episode",ParameterType.QueryString),
              ("category![Plan]","free",ParameterType.QueryString),
              ("per_page","5",ParameterType.QueryString),
              ("page",page.ToString(),ParameterType.QueryString),
              this.GetAuthenticationParameter()
            };

            string videosEndpoint = Settings.GetSetting("videos_endpoint");
            dynamic httpResponse = null;

            try
            {
              httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{videosEndpoint}", Method.GET, null, parameters);
              return httpResponse;
            }
            catch (RestHTTPException restException)
            {
                this.TrackError(restException, "search-videos", restException.Response.StatusCode.ToString(), $"query={query}&category-video-type=episode&category-plan=not-free");
                throw restException;
            }        
        }

        public async Task<dynamic> GetContinueWatchingVideos()
        {
            if (String.IsNullOrEmpty(Settings.GetSetting("access_token")))
            {
                var ex = new ArgumentNullException("The user has to be logged in order to consume the Continue Watching Videos method.");
                this.TrackError(ex, "get-continue-watching-videos", "access token shouldn't be null or empty");
                throw ex;
            }

            dynamic httpResponse = null;

            try
            {
                var parameters = new List<(string, string, ParameterType)>()
                {
                     this.GetAuthenticationParameter(),
                };
                
                httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zliving_base_endpoint"), $"{Settings.GetSetting("continue_watching")}", Method.GET,null, parameters);
            }
            catch (RestHTTPException restException)
            {
              
                this.TrackError(restException, "get-continue-watching-videos", restException.Response.StatusCode.ToString());
                throw restException;
            }

            return httpResponse;
        }

        public dynamic TrackPosition()
        {
            throw new NotImplementedException();
        }
    }
}
