﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.VOD.Exceptions
{
    public class PlayerNotAllowedException: ApplicationException
    {        
        public PlayerNotAllowedException(string message = null) : base(message) {
        }
    }
}
