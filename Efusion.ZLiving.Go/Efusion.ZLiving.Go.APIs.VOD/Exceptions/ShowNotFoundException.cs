﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.VOD.Exceptions
{
    public class ShowNotFoundException: ApplicationException
    {
        public ShowNotFoundException(string message = null):base(message)
        {

        }
    }
}
