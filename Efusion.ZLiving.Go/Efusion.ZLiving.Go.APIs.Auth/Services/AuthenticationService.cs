﻿using Efusion.ZLiving.Go.APIs.Auth.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Auth.Services
{
    public class AuthenticationService : ZLivingGoBaseService, IAuthenticationService
    {
        public override string ServiceName => "authentication";
        public AuthenticationService(Common.Services.IRestClient jsonClient, IBaseSettings settings) : base(jsonClient,settings) {

        }

        public async Task<dynamic> GetConsumer(string email)
        {
            if (String.IsNullOrEmpty(email)) {
                var ex = new UserNotFoundException();
                this.TrackError(ex, "get-consumer", "email is mandatory");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email",email,ParameterType.QueryString),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            try
            {
                dynamic httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("consumer_endpoint"), Method.GET, null, parameters);

                if (Convert.ToInt32(httpResponse.pagination.pages) == 0) {
                    var ex = new UserNotFoundException();
                    this.TrackError(ex, "get-customer", "User Not Found", $"email={email}");
                    throw ex;
                }

                return httpResponse.response[0];
            }
            catch (RestHTTPException ex)
            {
                if (ex.Response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    this.TrackError(ex, "get-customer", "User Not Found", $"email={email}");
                    throw new UserNotFoundException();
                }

                this.TrackError(ex, "get-customer", ex.Response.StatusCode.ToString(), $"email={email}");
                throw ex;
            }

        }

        public async Task<dynamic> GetRegistrationStatus(string email)
        {
            if (String.IsNullOrEmpty(email))
            {
                var ex = new UserNotFoundException();
                this.TrackError(ex, "get-registration-status", "email is mandatory");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email",email,ParameterType.QueryString),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            try
            {
                return await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zliving_base_endpoint"), Settings.GetSetting("registration_status_endpoint"), Method.GET, null, parameters);                
            }
            catch (RestHTTPException ex) {
                if (ex.Response.StatusCode == System.Net.HttpStatusCode.NotFound) {
                    this.TrackError(ex, "get-registration-status", "User Not Found", $"email={email}");
                    throw new UserNotFoundException();
                }
                this.TrackError(ex, "get-registration-status", ex.Response.StatusCode.ToString(), $"email={email}");
                throw ex;
            }
        }

        public async Task<dynamic> AcquireDevicePin(string deviceId) {

            if (String.IsNullOrEmpty(deviceId))
            {
                var ex = new ArgumentNullException("deviceId");
                this.TrackError(ex, "acquire-device-pin", "deviceId is mandatory");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("linked_device_id",deviceId,ParameterType.GetOrPost),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            dynamic content = null;
            try
            {
                content = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("acquire_link_device_pin_endpoint"), Method.POST, headers, parameters);
            }
            catch (RestHTTPException restException)
            {
                if (restException.Response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    this.TrackError(restException, "acquire-device-pin", "Unauthorized", $"device-id={deviceId}");
                    throw new UnauthorizedAccessException();
                }

                if (restException.Response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    this.TrackError(restException, "acquire-device-pin", "Device Not Found", $"device-id={deviceId}");
                    throw new UserNotFoundException();
                }

                this.TrackError(restException, "acquire-device-pin", restException.Response.StatusCode.ToString(), $"device-id={deviceId}");
                throw restException;
            }

            return content.response;
        }

        public async Task<dynamic> LinkDevice(string devidePIN)
        {
            if (String.IsNullOrEmpty(devidePIN))
            {
                var ex = new ArgumentNullException("devidePIN");
                this.TrackError(ex, "link-device", "devidePIN is mandatory");
                throw ex;
            }

            string consumerId = Settings.GetSetting("consumer_id");

            if (String.IsNullOrWhiteSpace(consumerId)) {
                var ex = new UserNotFoundException("Consumer ID not found");
                this.TrackError(ex, "link-device", "Consumer ID Not Found", $"pin={devidePIN}");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("pin",devidePIN,ParameterType.QueryString),
              ("consumer_id",consumerId,ParameterType.QueryString),
              this.GetAuthenticationParameter()
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            try
            {
                return await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("link_device_endpoint"), Method.PUT, headers, parameters);
            }
            catch (RestHTTPException restException)
            {
                if (restException.Response.StatusCode == System.Net.HttpStatusCode.NotFound){
                    this.TrackError(restException, "link-device", "Pin Not Found", $"pin={devidePIN}");
                    throw new UserNotFoundException("PIN not Found");
                }

                if (Convert.ToInt32(restException.Response.StatusCode) == 422)
                {
                    this.TrackError(restException, "link-device", "422 UNPROCESSABLE ENTITY", $"pin={devidePIN}");
                    dynamic errorContent = JsonConvert.DeserializeObject(restException.Response.Content);
                    string message = Convert.ToString(errorContent.message);                    
                    throw new UserNotFoundException(message);
                }

                this.TrackError(restException, "link-device", restException.Response.StatusCode.ToString(), $"pin={devidePIN}");
                throw restException;
            }
        }

        public async Task<dynamic> LoginDevice(string deviceId, string pin) {

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("client_id",Settings.GetSetting("client_id"),ParameterType.GetOrPost),
              ("client_secret",Settings.GetSetting("client_secret"),ParameterType.GetOrPost),
              ("linked_device_id",deviceId,ParameterType.GetOrPost),
              ("pin",pin,ParameterType.GetOrPost),
              ("grant_type","password",ParameterType.GetOrPost),
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            dynamic content = null;

            try
            {
                content = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("login_base_endpoint"), Settings.GetSetting("login_endpoint"), Method.POST, headers, parameters);
            }
            catch (RestHTTPException restException)
            {
                if (restException.Response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {           
                    this.TrackError(restException, "login-device", "Unauthorized", $"deviceId={deviceId}&pin={pin}");
                    throw new UnauthorizedAccessException();
                }

                if (restException.Response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    this.TrackError(restException, "login-device", "User Not Found", $"deviceId={deviceId}&pin={pin}");
                    throw new UserNotFoundException();
                }

                this.TrackError(restException, "login-device", restException.Response.StatusCode.ToString(), $"deviceId={deviceId}&pin={pin}");
                throw restException;
            }

            return content;


        }

        public async Task<dynamic> Login(string email, string password)
        {
            if (String.IsNullOrEmpty(email))
            {
                var ex = new UserNotFoundException();
                this.TrackError(ex, "login", "email is mandatory");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("client_id",Settings.GetSetting("client_id"),ParameterType.GetOrPost),
              ("client_secret",Settings.GetSetting("client_secret"),ParameterType.GetOrPost),
              ("username",email,ParameterType.GetOrPost),
              ("password",password,ParameterType.GetOrPost),
              ("grant_type","password",ParameterType.GetOrPost),
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            dynamic content = null;
            try
            {
                content = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("login_base_endpoint"), Settings.GetSetting("login_endpoint"), Method.POST, headers, parameters);
            }
            catch (RestHTTPException restException)
            {
                if (restException.Response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {                      
                    this.TrackError(restException, "login", "Unauthorized", $"user={email}");
                    throw new UnauthorizedAccessException();
                }

                if (restException.Response.StatusCode == System.Net.HttpStatusCode.NotFound){
                    this.TrackError(restException, "login", "User Not Found", $"user={email}");
                    throw new UserNotFoundException();
                }

                this.TrackError(restException, "login", restException.Response.StatusCode.ToString(), $"user={email}");
                throw restException;
            }

            return content;
        }
        
        public async Task<dynamic> Register(string name,string email, string password)
        {
            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email",email,ParameterType.GetOrPost),
              ("name",name,ParameterType.GetOrPost),
              ("password",password,ParameterType.GetOrPost),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            dynamic content = null;
            try {
                content = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zliving_base_endpoint"), Settings.GetSetting("registration_endpoint"), Method.GET, headers, parameters);
            }
            catch (RestHTTPException restException) {
                if (restException.Response.StatusCode == System.Net.HttpStatusCode.Conflict)
                {                     
                    this.TrackError(restException, "register", "Customer Already exists", $"user={email}&name={name}");
                    throw new RegistrationException("Customer already exists");
                }

                if (restException.Response.StatusCode == System.Net.HttpStatusCode.BadRequest) {
                    dynamic errorContent = JsonConvert.DeserializeObject(restException.Response.Content);
                    string message = Convert.ToString(errorContent.message);
                    this.TrackError(restException, "register", message, $"user={email}&name={name}");
                    throw new RegistrationException(message);
                }

                this.TrackError(restException, "register", restException.Response.StatusCode.ToString(), $"user={email}&name={name}");
                throw restException;
            }

            return content;
        }

        public async Task SendForgottenPassword(string email) {

            if (String.IsNullOrEmpty(email))
            {
                var ex = new UserNotFoundException();
                this.TrackError(ex, "send-forgotten-password", "email is mandatory");
                throw ex;
            }

            if (String.IsNullOrWhiteSpace(email)) {
                var ex = new UserNotFoundException();
                this.TrackError(ex, "send-forgotten-password", "User Not Found", $"user={email}");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email",email,ParameterType.GetOrPost),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            try {
                await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zliving_stg_base_endpoint"), Settings.GetSetting("forgot_password_endpoint"), Method.POST, headers, parameters);
            }
            catch (RestHTTPException restException) {
                if (restException.Response.StatusCode == System.Net.HttpStatusCode.NotFound) {
                    this.TrackError(restException,  "send-forgotten-password", "User Not Found", $"user={email}");
                    throw new UserNotFoundException();
                }

                this.TrackError(restException, "send-forgotten-password", restException.Response.StatusCode.ToString(), $"user={email}");
                throw restException;
            }
        }

        public async Task<dynamic> ResendActivation(string email)
        {
            if (String.IsNullOrEmpty(email))
            {
                var ex = new UserNotFoundException();
                this.TrackError(ex, "resend-activation", "email is mandatory");
                throw ex;
            }

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email",email,ParameterType.GetOrPost),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            try {
                return await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zliving_base_endpoint"), Settings.GetSetting("resend_activation_endpoint"), Method.POST, headers, parameters);

            }catch(RestHTTPException restException) {
                this.TrackError(restException, "resend-activation", restException.Response.StatusCode.ToString(), $"user={email}");
                throw restException;
            }            
        }

        public async Task<dynamic> ChangePassword(string accessToken, string password)
        {
            var parameters = new List<(string, string, ParameterType)>()
            {
              ("access_token",accessToken,ParameterType.GetOrPost),              
              ("password",password,ParameterType.GetOrPost)              
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            dynamic content = null;
            try
            {
                content = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zliving_stg_base_endpoint"), Settings.GetSetting("change_password_endpoint"), Method.GET, headers, parameters);
            }
            catch (RestHTTPException restException)
            {
                if (restException.Response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(restException.Response.Content);
                    string message = Convert.ToString(errorContent.message);
                    this.TrackError(restException, "change-password",$"Invalid Password: {message}", String.Empty);
                    throw new InvalidPasswordException(message);
                }

                if (restException.Response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    dynamic errorContent = JsonConvert.DeserializeObject(restException.Response.Content);
                    string message = Convert.ToString(errorContent.message);
                    this.TrackError(restException, "change-password", $"Unauthorized: {message}", String.Empty);
                    throw new UnauthorizedAccessException(Convert.ToString(errorContent.message));
                }

                this.TrackError(restException, "change-password", restException.Response.StatusCode.ToString(), String.Empty);
                throw restException;
            }

            return content;
        }
    }
}
