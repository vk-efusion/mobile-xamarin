﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Auth.Services
{
    public interface IAuthenticationService
    {
        Task<dynamic> Register(string name, string email, string password);
        Task<dynamic> ResendActivation(string email);
        Task<dynamic> Login(string email, string password);
        Task<dynamic> LoginDevice(string deviceId, string pin);       
        Task<dynamic> ChangePassword(string accessToken, string password);
        Task SendForgottenPassword(string email);
        Task<dynamic> GetRegistrationStatus(string email);
        Task<dynamic> LinkDevice(string devicePIN);
        Task<dynamic> AcquireDevicePin(string deviceId);
        Task<dynamic> GetConsumer(string email);
    }
}
