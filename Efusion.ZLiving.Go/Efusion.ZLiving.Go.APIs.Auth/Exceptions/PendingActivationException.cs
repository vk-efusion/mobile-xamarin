﻿using System;

namespace Efusion.ZLiving.Go.APIs.Auth.Exceptions
{
    public class PendingActivationException: ApplicationException
    {
        public PendingActivationException(string message = null) : base(message)
        {

        }
    }
}
