﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Auth.Exceptions
{
    public class RegistrationException : ApplicationException
    {
        public RegistrationException(string message = null) : base(message)
        {

        }
    }
}
