﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Auth.Exceptions
{
    public class InvalidPasswordException: ApplicationException
    {
        public InvalidPasswordException(string message = null) : base(message)
        {

        }
    }
}
