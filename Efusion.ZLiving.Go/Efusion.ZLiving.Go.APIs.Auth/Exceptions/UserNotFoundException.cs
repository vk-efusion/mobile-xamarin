﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Auth.Exceptions
{
    public class UserNotFoundException: ApplicationException
    {        
        public UserNotFoundException(string message = null) : base(message) {

        }
    }
}
