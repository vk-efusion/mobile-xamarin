﻿using Efusion.ZLiving.Go.UI.Common.Models;
using Efusion.ZLiving.Go.UI.VOD.Models;
using FFImageLoading.Forms;
using System;

namespace Efusion.ZLiving.Go.UI.VOD.Extensions
{
    public static class ResourceExtensions
    {
        public static Video Populate(this Video video, dynamic zypeVideo) {
            PopulateVideoResource(video, zypeVideo);            
            return video;
        }

        public static Episode Populate(this Episode episode, string showId, dynamic zypeVideo)
        {
            PopulateIResource(episode, zypeVideo);           
            episode.ShowID = showId;
            return episode;
        }

        public static Playlist PopulatePagination(this Playlist playlist, dynamic zypePagination) {

            if (Int32.TryParse(Convert.ToString(zypePagination.current), out int result))
            {
                playlist.Pagination.Current = result;
            }
            else
            {
                playlist.Pagination.Current = 0;
            }

            if (Int32.TryParse(Convert.ToString(zypePagination.previous), out result))
            {
                playlist.Pagination.Previous = result;
            }
            else
            {
                playlist.Pagination.Previous = null;
            }


            if (Int32.TryParse(Convert.ToString(zypePagination.next), out result))
            {
                playlist.Pagination.Next = result;
            }
            else
            {
                playlist.Pagination.Next = null;
            }

            if (Int32.TryParse(Convert.ToString(zypePagination.per_page), out result))
            {
                playlist.Pagination.PerPage = result;
            }
            else
            {
                playlist.Pagination.PerPage = null;
            }

            if (Int32.TryParse(Convert.ToString(zypePagination.pages), out result))
            {
                playlist.Pagination.Pages = result;
            }
            else
            {
                playlist.Pagination.Pages = null;
            }

            return playlist;
        }

        public static Playlist Populate(this Playlist playlist, bool isRoot, dynamic zypePlaylist)
        {
            playlist.Id = Convert.ToString(zypePlaylist._id);
            playlist.Name = Convert.ToString(zypePlaylist.title);
            playlist.IsRoot = isRoot;
            playlist.ItemCount = Convert.ToInt32(zypePlaylist.playlist_item_count);
            playlist.AddKeywords(zypePlaylist._keywords);
         
            return playlist;
        }

        public static Show Populate(this Show show, dynamic zypeShow)
        {
            PopulatePlaylistResource(show, zypeShow);                     
            return show;
        }

        public static LiveVideo Populate(this LiveVideo video,string pgName, dynamic zypeLiveVideo)
        {
            PopulateVideoResource(video, zypeLiveVideo);
            DateTime startTime = DateTime.Parse(Convert.ToString(zypeLiveVideo.custom_attributes.start_time_offset));
            DateTime endTime = DateTime.Parse(Convert.ToString(zypeLiveVideo.custom_attributes.end_time_offset));            
            TimeSpan span = endTime.Subtract(startTime);
            video.Duration = $"{span.TotalMinutes} min";
            video.Start = startTime.ToString("hh:mm tt");
            video.Title = $"{pgName}:{video.Name}";
            return video;
        }

        public static ProgrammingGuide Populate(this ProgrammingGuide pGuide, string livePlaylistId, dynamic zypeProgrammingGuide)
        {
            PopulatePlaylistResource(pGuide, zypeProgrammingGuide);
            pGuide.LivePlayListID = livePlaylistId;          
            return pGuide;
        }

        private static IResource PopulateIResource(IResource resource, dynamic zypeVideo)
        {
            resource.Id = Convert.ToString(zypeVideo._id);
            resource.ImageUrl = (zypeVideo.thumbnails.Count > 0) ? Convert.ToString(zypeVideo.thumbnails[0].url) : "image_unavailable";
            resource.Name = Convert.ToString(zypeVideo.title);            
            return resource;
        }

        private static Video PopulateVideoResource(Video resource, dynamic zypeVideo)
        {
            PopulateIResource(resource, zypeVideo);            
            resource.Title = resource.Name;
            return resource;
        }

        private static IResource PopulatePlaylistResource(IResource resource, dynamic zypePlaylist)
        {
            string imageUrl = "image_unavailable";

            foreach (dynamic image in zypePlaylist.images)
            {
                if (Convert.ToString(image.title).ToLower() == "icon")
                {
                    imageUrl = Convert.ToString(image.url);                    
                    break;
                }
            }
            
            resource.Id = Convert.ToString(zypePlaylist._id);
            resource.Name = Convert.ToString(zypePlaylist.title); ;
            resource.ImageUrl = imageUrl;

            return resource;
        }

    }
}
