﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.EPG.Services;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Managers;
using Efusion.ZLiving.Go.UI.VOD.Views;
using Prism.Ioc;
using Prism.Modularity;

namespace Efusion.ZLiving.Go.UI.VOD.Modules
{
    public class VODModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IBaseSettings, Settings>();
            containerRegistry.RegisterSingleton<IRestClient, JsonRestClient>();
            containerRegistry.RegisterSingleton<IVideoOnDemandService, VideoOnDemandService>();
            containerRegistry.RegisterSingleton<IElectronicProgrammingGuideService, ElectronicProgrammingGuideService>();
            containerRegistry.RegisterSingleton<ISessionManager, SessionManager>();

            containerRegistry.RegisterForNavigation<CategoryPage>("category");
            containerRegistry.RegisterForNavigation<FullScreenVideoPage>("fullscreen-video");            
            containerRegistry.RegisterForNavigation<ShowDetailsPage>("show-details");
            containerRegistry.RegisterForNavigation<EpisodeDetailsPage>("episode");
            containerRegistry.RegisterForNavigation<SearchPage>("search");
        }
    }
}
