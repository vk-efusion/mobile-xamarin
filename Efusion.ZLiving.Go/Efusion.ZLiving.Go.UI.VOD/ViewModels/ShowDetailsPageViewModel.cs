﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Efusion.ZLiving.Go.UI.VOD.Extensions;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using System;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.VOD.ViewModels
{
    public class ShowDetailsPageViewModel : BaseViewModel
    {
        private string imageUrl;
        private string showDescription;
        private string episodesCountDescription;
        private Playlist show;
        public Playlist Show { get => show; set => SetProperty(ref show, value); }
        private IVideoOnDemandService VideoOnDemandService { get; set; }
        public string ImageUrl { get => imageUrl; set => SetProperty(ref imageUrl, value); }
        public string ShowDescription { get => showDescription; set => SetProperty(ref showDescription, value); }
        public string EpisodesCountDescription { get => episodesCountDescription; set => SetProperty(ref episodesCountDescription, value); }
        
        public ShowDetailsPageViewModel(FooterViewModel footerViewModel, IVideoOnDemandService videoOnDemandService, ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : base(footerViewModel, sessionManager, navigationService,settings)
        {
            this.VideoOnDemandService = videoOnDemandService;
        }

        public async override void OnNavigatedTo(NavigationParameters parameters)
        {            
            try
            {
                if (Show == null)
                {
                    base.OnNavigatedTo(parameters);

                    this.IsBusy = true;
                    this.Show = new Playlist(this.NavigationService,null)
                    {
                        Id = parameters.GetValue<string>("id")
                    };

                    var show = await this.VideoOnDemandService.GetShow(this.Show.Id);

                    foreach (var image in show.images)
                    {
                        if (Convert.ToString(image.title).ToLower() == "poster")
                        {
                            this.ImageUrl = Convert.ToString(image.url);
                            break;
                        }
                    }

                    this.EpisodesCountDescription = $"{Convert.ToString(show.playlist_item_count)} episodes";
                    this.ShowDescription = Convert.ToString(show.description);
                    await this.LoadEpisodes();
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(new Exception("unexpected-exception", ex));
            }
            finally
            {

                this.IsBusy = false;
            }
        }

        private async Task<Playlist> LoadEpisodes()
        {            
            this.Show.Clear();
            dynamic videos = await this.VideoOnDemandService.GetVideos(this.Show.Id);
            this.Show.PopulatePagination(videos.pagination as JObject);

            foreach (var video in videos.response) {
                this.Show.Add(new Episode(this.NavigationService).Populate(this.Show.Id, video as JObject));                
            }        
            return this.Show;

        }


    }
}
