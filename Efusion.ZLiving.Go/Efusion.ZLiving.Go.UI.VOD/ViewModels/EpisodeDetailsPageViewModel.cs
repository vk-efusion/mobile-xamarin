﻿using Efusion.ZLiving.Go.APIs.Auth.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.VOD.Exceptions;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Models;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Microsoft.AppCenter.Crashes;
using Prism.Navigation;
using System;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.VOD.ViewModels
{
    public class EpisodeDetailsPageViewModel : BaseViewModel
    {
        private string id;
        private string showId;
        private string fullTitle;
        private UriVideoSource videoUrl;
        private string description;
        private string message;
        private bool showRecommendations;
        private IVideoOnDemandService VideoOnDemandService { get; set; }
        public string Id { get => id; set => SetProperty(ref id, value); }
        public string ShowId { get => showId; set => SetProperty(ref showId, value); }
        public UriVideoSource VideoUrl { get => videoUrl; set => SetProperty(ref videoUrl, value); }
        public string Description { get => description; set => SetProperty(ref description, value); }
        public string FullTitle { get => fullTitle; set => SetProperty(ref fullTitle, value);  }
        public string Message { get => message; set => SetProperty(ref message, value); }
        public bool ShowRecommendations { get => showRecommendations; set => SetProperty(ref showRecommendations, value); }

        private PlaylistsViewModel recommendations;
        public PlaylistsViewModel Recommendations { get => recommendations; set => SetProperty(ref recommendations, value); }

        public EpisodeDetailsPageViewModel(PlaylistsViewModel playlistsViewModel,FooterViewModel footerViewModel, ISessionManager sessionManager, IVideoOnDemandService videoOnDemandService, INavigationService navigationService, IBaseSettings settings) : base(footerViewModel, sessionManager, navigationService, settings)
        {
            this.VideoOnDemandService = videoOnDemandService;
            this.Recommendations = playlistsViewModel;
            this.ShowRecommendations = true;
        }

  
        public async override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);

            try
            {
                if (string.IsNullOrEmpty(Id))
                {
                    this.Id = parameters.GetValue<string>("id");
                    this.ShowId = parameters.GetValue<string>("show-id");

                    var show = this.VideoOnDemandService.GetShow(this.ShowId);
                    var video = this.VideoOnDemandService.GetVideo(this.Id);

                    await Task.WhenAll(show, video);

                    this.FullTitle = $"{Convert.ToString(show.Result.title)}: {Convert.ToString(video.Result.title)}";
                    this.Title = this.FullTitle;

                    this.VideoUrl = new UriVideoSource()
                    { Uri = Convert.ToString(video.Result.player.url) };

                    this.Description = Convert.ToString(video.Result.description);
                    this.PageState = Common.States.PageState.Success;

                    if (this.AuthenticationState == Common.Enums.AuthenticationState.Authenticated)
                        await this.Recommendations.LoadRecommendations(false);
                }

            }
            catch (PlayerNotAllowedException ex)
            {
                this.Message = ex.Message;
                this.PageState = Common.States.PageState.Error;
            }
            catch (UserNotFoundException)
            {               
                this.PageState = Common.States.PageState.Success;
            }
            catch (Exception ex)
            {
                this.Message = "An unexpected exception has occured. Plese try again later.";
                this.PageState = Common.States.PageState.UnexpectedException;
                Crashes.TrackError(new Exception("unexpected-exception", ex));
            }            
            finally
            {
                this.ShowRecommendations = ((this.Recommendations.Source.Count != 0) && (this.Recommendations.Source[0].Count != 0));
            }
        }
     
    }
}
