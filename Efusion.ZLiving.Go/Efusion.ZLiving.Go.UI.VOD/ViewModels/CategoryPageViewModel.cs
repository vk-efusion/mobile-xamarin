﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Microsoft.AppCenter.Crashes;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.VOD.ViewModels
{
    public class CategoryPageViewModel: BaseViewModel
    {
        private PlaylistsViewModel playlists;
        public PlaylistsViewModel Playlists { get => playlists; set => SetProperty(ref playlists, value); }

        public CategoryPageViewModel(FooterViewModel footerViewModel, PlaylistsViewModel playlists, INavigationService navigationService,ISessionManager sessionManager, IBaseSettings settings) : base(footerViewModel,sessionManager, navigationService, settings)
        {
            this.Playlists = playlists;
        }


        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
        }
        
        public async override void OnNavigatedTo(NavigationParameters parameters)
        {
            try
            {
                this.IsBusy = true;
                if (playlists.Source.Count == 0)
                {
                    base.OnNavigatedTo(parameters);
                    string categoryId = String.Empty;

                    if (!parameters.TryGetValue("id", out categoryId))
                    {
                        throw new KeyNotFoundException("To load a Category View Model the \"id\" is needed. Send it as a navigation parameter");
                    }

                    string title = String.Empty;

                    if (!parameters.TryGetValue("title", out title))
                    {
                        throw new KeyNotFoundException("Missing Category Name. Send it as \"title\" in the navigation parameter");
                    }

                    this.Title = title;

                    await this.Load(categoryId, parameters.GetValue<dynamic>("keywords"));
                }
            }
            catch (Exception ex)
            {
                this.TrackError(ex, "OnNavigatedTo", String.Empty);
            }
            finally {
                this.IsBusy = false;
            }
        }

        public async Task Load(string categoryId,dynamic keywords)
        {
            try
            {
                if (categoryId.ToLower().Equals("recommendations"))
                {
                    if(this.AuthenticationState == Common.Enums.AuthenticationState.Authenticated)
                        await this.Playlists.LoadRecommendations(false);
                }
                else {
                    await this.Playlists.Load(categoryId, keywords);
                }
            }
            catch (Exception e) {
                //I added this because The Live and Catch Up fails when clicking in the menu. API not working Will remove later
                Crashes.TrackError(new Exception("android-environment-unexpected-exception", e));
            }
        }
    }
}
