﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.EPG.Services;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Efusion.ZLiving.Go.UI.VOD.Extensions;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Efusion.ZLiving.Go.UI.VOD.ViewModels
{
    public class PlaylistsViewModel : BaseViewModel
    {        
        public IVideoOnDemandService VideoOnDemandService { get; set; }
        public IElectronicProgrammingGuideService ElectronicProgrammingGuideService { get; set; }
        public ObservableCollection<Playlist> Source { get; } = new ObservableCollection<Playlist>();
        public ICommand RefreshCommand { get; set; }
        
        
        public PlaylistsViewModel(FooterViewModel footerViewModel, IElectronicProgrammingGuideService electronicProgrammingGuideService, IVideoOnDemandService videoOnDemandService, INavigationService navigationService, ISessionManager sessionManager, IBaseSettings settings) : base(footerViewModel,sessionManager, navigationService,settings)
        {
            this.VideoOnDemandService = videoOnDemandService;
            this.ElectronicProgrammingGuideService = electronicProgrammingGuideService;
            this.IsBusy = false;
        }

        public async Task Load(string playListId, dynamic keywords)
        {
            this.IsBusy = true;

            try
            {
                this.Source.Clear();
                dynamic playlists = await this.VideoOnDemandService.GetPlaylistsAsync(playListId);

                foreach (var plist in playlists.response)
                {
                    var playlist = new Playlist(this.NavigationService, LoadMoreEpisodes).Populate(false, plist as JObject);
                    playlist.AddKeywords(keywords);
                    await this.LoadEpisodes(playlist);

                    if (playlist.Count > 0) {
                        this.Source.Add(playlist);
                    }
                }
            }
            catch (Exception ex)
            {
                this.TrackError(ex, "Load", String.Empty);
            }
            finally
            {
                this.IsBusy = false;
            }
        }

        public async Task LoadRecommendations(bool isRoot)
        {
            try
            {
                this.IsBusy = true;                
                var recommendationsPlaylist = new Playlist(this.NavigationService, LoadRecommendations)
                {
                    Id = "Recommendations",
                    Name = "Recommendations",
                    IsRoot = isRoot
                };

                await this.LoadRecommendations(recommendationsPlaylist);

                if (recommendationsPlaylist.Count > 0)
                {
                    this.Source.Add(recommendationsPlaylist);
                }
            }
            catch (Exception ex)
            {
                this.TrackError(ex, "LoadRecommendations", String.Empty);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task<Playlist> LoadRecommendations(Playlist recommendationsPlaylist)
        {
            if (!AuthenticationState.Equals(Common.Enums.AuthenticationState.Authenticated))
            {
                throw new UnauthorizedAccessException("User Authenticated needed in order to be able to load recommendations");
            }

            try
            {
                this.IsBusy = true;
                recommendationsPlaylist.IsLoading = true;
                if (recommendationsPlaylist.Pagination.HasMorePages)
                {
                    dynamic recommendationsResponse = await this.VideoOnDemandService.GetRecommendations(recommendationsPlaylist.Pagination.Next.Value);
                    recommendationsPlaylist.PopulatePagination(recommendationsResponse.pagination as JObject);

                    foreach (var video in recommendationsResponse.response)
                    {
                        recommendationsPlaylist.Resources.Add(new Video(this.NavigationService).Populate(video as JObject));
                    }
                }
            }
            catch (Exception ex)
            {
                this.TrackError(ex, "LoadRecommendations", String.Empty);
            }
            finally
            {
                IsBusy = false;
                recommendationsPlaylist.IsLoading = false;
            }

            return recommendationsPlaylist;
        }

        public async Task LoadRootPlaylist()
        {            
            try
            {
                this.IsBusy = true;
                this.Source.Clear();
                dynamic playlists = await this.VideoOnDemandService.GetRootPlaylistsAsync();                

                foreach (var category in playlists.response) {
                    var playlist = new Playlist(this.NavigationService, LoadMoreShows).Populate(true, category as JObject);
                    
                    if (playlist.IsLive) {
                        playlist.LoadMoreAction = LoadMoreProgrammingGuide;
                        await this.LoadProgrammingGuide(playlist);

                        if (playlist.Count > 0)
                        {
                            this.Source.Add(playlist);
                        }

                        if (AuthenticationState.Equals(Common.Enums.AuthenticationState.Authenticated)) {
                            await this.LoadRecommendations(true);
                        }
                    }
                    else{
                        await this.LoadShows(playlist);

                        if (playlist.Count > 0)
                        {
                            this.Source.Add(playlist);
                        }
                    }

                  
                }
            }
            catch (Exception ex)
            {
                this.TrackError(ex, "LoadRootPlaylist", String.Empty);                
            }
            finally
            {
                this.IsBusy = false;
            }
        }


        private async Task<Playlist> LoadMoreShows(Playlist showsList) {            
            try
            {
                IsBusy = true;
                showsList.IsLoading = true;
                await LoadShows(showsList);
            }
            catch (Exception ex)
            {
                this.TrackError(ex, "LoadMoreEpisodes", String.Empty);
            }
            finally
            {
                this.IsBusy = false;
                showsList.IsLoading = false;
            }

            return showsList;
        }

        private async Task<Playlist> LoadMoreEpisodes(Playlist showPlayList) {
            
            try
            {
                IsBusy = true;
                showPlayList.IsLoading = true;
                await LoadEpisodes(showPlayList);
            }
            catch (Exception ex)
            {
                this.TrackError(ex, "LoadMoreEpisodes", String.Empty);
            }
            finally
            {
                this.IsBusy = false;
                showPlayList.IsLoading = false;
            }

            return showPlayList;
        }

        private async Task<Playlist> LoadMoreProgrammingGuide(Playlist livePlaylist)
        {            
            try
            {
                IsBusy = true;
                livePlaylist.IsLoading = true;
                await LoadProgrammingGuide(livePlaylist);
            }
            catch (Exception ex)
            {
                this.TrackError(ex, "LoadMoreEpisodes", String.Empty);
            }
            finally
            {
                this.IsBusy = false;
                livePlaylist.IsLoading = false;
            }

            return livePlaylist;
        }

        private async Task<Playlist> LoadEpisodes(Playlist showPlayList)
        {            
            if (showPlayList.Pagination.HasMorePages)
            {
                dynamic videos = await this.VideoOnDemandService.GetVideos(showPlayList.Id, showPlayList.Pagination.Next.Value);
                showPlayList.PopulatePagination(videos.pagination as JObject);

                foreach (var video in videos.response)
                {
                    showPlayList.Resources.Add(new Episode(this.NavigationService).Populate(showPlayList.Id, video as JObject));
                }
            }

            return showPlayList;
        }

        private async Task<Playlist> LoadProgrammingGuide(Playlist livePlaylist)
        {         
            if (livePlaylist.Pagination.HasMorePages)
            {
                dynamic programmingGuide = await this.ElectronicProgrammingGuideService.GetCatchUpPlaylist(livePlaylist.Id, livePlaylist.Pagination.Next.Value);
                livePlaylist.PopulatePagination(programmingGuide.pagination as JObject);

                foreach (var pGuide in programmingGuide.response)
                {
                    livePlaylist.Resources.Add(new ProgrammingGuide(this.NavigationService).Populate(livePlaylist.Id, pGuide as JObject));
                }            
            }

            return livePlaylist;
        }

        private async Task<Playlist> LoadShows(Playlist showsList)
        {          
            if (showsList.Pagination.HasMorePages)
            {
                dynamic shows = await this.VideoOnDemandService.GetPlaylistsAsync(showsList.Id, showsList.Pagination.Next.Value);
                showsList.PopulatePagination(shows.pagination as JObject);

                foreach (var show in shows.response)
                {
                    showsList.Resources.Add(new Show(this.NavigationService).Populate(show as JObject));
                }
            }         

            return showsList;
        }
      
    }
}
