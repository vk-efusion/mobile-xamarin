﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.VOD.Exceptions;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Models;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Prism.Navigation;
using System;

namespace Efusion.ZLiving.Go.UI.VOD.ViewModels
{
    public class FullScreenVideoPageViewModel: BaseViewModel
    {
        private UriVideoSource source;
        private string message;
        private TimeSpan position;

        public UriVideoSource Source { get => source; set => SetProperty(ref source, value); }
        public string Message { get => message; set => SetProperty(ref message, value); }
        public TimeSpan Position { get => position; set => SetProperty(ref position, value); }

        private IVideoOnDemandService VideoOnDemandService { get; set; }

        public FullScreenVideoPageViewModel(IVideoOnDemandService videoOnDemandService, ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : base(sessionManager, navigationService,settings)
        {
            this.VideoOnDemandService = videoOnDemandService;
            this.Position = new TimeSpan(0);
        }

        public async override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            this.Title = parameters.GetValue<string>("title");
            string id = parameters.GetValue<string>("id");


            if (parameters.TryGetValue("position", out int position))
            {
                this.Position = TimeSpan.FromMilliseconds(position);
            }

            try
            {
                dynamic player = await this.VideoOnDemandService.GetPlayer(id);
                this.Source = new UriVideoSource() { Uri = Convert.ToString(player.url) };
                this.PageState = Common.States.PageState.Success;
            }            
            catch (PlayerNotAllowedException ex)
            {
                this.Message = ex.Message;
                this.PageState = Common.States.PageState.Error;                    ;                
                this.TrackError(ex, "OnNavigatingTo", "Error retrieveing Player", $"video-id={id}");

            } catch(Exception ex) {
                this.Message = "An unexpected exception has occured. Plese try again later.";
                this.PageState = Common.States.PageState.UnexpectedException;                
                this.TrackError(ex, "OnNavigatingTo", "Error retrieveing Player", $"video-id={id}");                
            }                        
        }
    }
}
