﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Efusion.ZLiving.Go.UI.VOD.Extensions;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using System;
using System.Dynamic;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.VOD.ViewModels
{
    public class SearchPageViewModel : BaseViewModel
    {
        private string query;
        private Playlist searchVideos;

        public Playlist SearchVideos { get => searchVideos; set => SetProperty(ref searchVideos, value); }
        public string Query { get => query; set => SetProperty(ref query, value); }
        public IVideoOnDemandService VideoOnDemandService { get; set; }

        public SearchPageViewModel(FooterViewModel footerViewModel, IVideoOnDemandService videoOnDemandService, ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : base(footerViewModel, sessionManager, navigationService, settings)
        {
            VideoOnDemandService = videoOnDemandService;
            this.Query = String.Empty;
            this.SearchVideos = new Playlist(this.NavigationService,LoadMoreVideos);
        }

        public async override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            string query = parameters.GetValue<string>("query");

            if (!Query.Equals(query))
            {
                this.Query = query ?? this.Query;
                this.Title = $"Search: {Query}";                
                await LoadVideos();
            }
        }

        public async Task LoadMoreVideos(Playlist playlist)
        {
            try
            {
                this.IsBusy = true;

                if (playlist.Pagination.HasMorePages)
                {
                    dynamic videos = await this.VideoOnDemandService.SearchVideos(this.Query, playlist.Pagination.Next.Value);
                    playlist.PopulatePagination(videos.pagination as JObject);

                    foreach (dynamic video in videos.response)
                    {
                        playlist.Add(new Video(NavigationService).Populate(video as JObject));
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(new Exception("unexpected-exception", ex));
            }
            finally
            {

                this.IsBusy = false;
            }
        }

        public async Task LoadVideos()
        {
            try
            {
                this.IsBusy = true;
                this.SearchVideos.Clear();
                dynamic videos = await this.VideoOnDemandService.SearchVideos(this.Query);
                this.SearchVideos.PopulatePagination(videos.pagination as JObject);

                foreach (dynamic video in videos.response)
                {
                    this.SearchVideos.Add(new Video(NavigationService).Populate(video as JObject));
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(new Exception("unexpected-exception", ex));
            }
            finally
            {

                this.IsBusy = false;
            }
        }
    }
}
