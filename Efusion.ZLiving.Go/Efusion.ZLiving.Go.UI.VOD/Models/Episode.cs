﻿using Efusion.ZLiving.Go.UI.Common.Models;
using Prism.Navigation;

namespace Efusion.ZLiving.Go.UI.VOD.Models
{
    public class Episode : ResourceBase
    {
        public string ShowID { get; set; }

        public override NavigationParameters NavigationParameters { get => new NavigationParameters($"id={Id}&show-id={ShowID}"); }
        public override string NavigationPage { get => "episode"; }

        public Episode(INavigationService navigationService) : base(navigationService)
        {

        }                
        
    }
}
