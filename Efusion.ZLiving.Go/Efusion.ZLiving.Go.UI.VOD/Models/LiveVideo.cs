﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Navigation;

namespace Efusion.ZLiving.Go.UI.VOD.Models
{
    public class LiveVideo: Video
    {
        public string Start { get; set; } = String.Empty;
        public string Duration { get; set; } = String.Empty;

        public LiveVideo(INavigationService navigationService) : base(navigationService)
        {

        }
    }
}
