﻿using Efusion.ZLiving.Go.UI.Common.Models;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.VOD.Models
{

    public class Playlist : ObservableCollection<IResource>
    {
        public bool IsLoading { get; set; }
        public virtual Func<Playlist, Task> LoadMoreAction { get; set; }
        public bool IsRoot { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public int ItemCount { get; set; }
        public List<string> Keywords { get; } = new List<string>();
        public ICommand ViewAllCommannd { get; }
        public ICommand LoadMoreCommand { get; set; }
        public ObservableCollection<IResource> Resources { get => this; }
        public INavigationService NavigationService { get; set; }        
        public bool IsLive { get => (this.Keywords.Contains("live") || this.Keywords.Contains("catch")); }
        public Pagination Pagination { get; set; } = new Pagination();                

        public Playlist(INavigationService navigationService, Func<Playlist, Task> loadMoreAction)
        {
            this.NavigationService = navigationService;
            this.ViewAllCommannd = new DelegateCommand(ViewAll);
            this.LoadMoreCommand = new DelegateCommand(LoadMore);
            this.LoadMoreAction = loadMoreAction;            
        }

        public async void LoadMore() {
            if ((LoadMoreAction != null) && (!IsLoading)){
                await this.LoadMoreAction(this);                
            }
        }

        public void AddKeywords(dynamic keywords)
        {
            if (keywords != null)
            {
                foreach (var keyword in keywords)
                {
                    this.Keywords.Add(Convert.ToString(keyword));
                }
            }
        }
        
        public async void ViewAll()
        {          
           await NavigationService.NavigateAsync("category", new NavigationParameters($"id={Id}&title={Name}"));
        }     
    }
}
