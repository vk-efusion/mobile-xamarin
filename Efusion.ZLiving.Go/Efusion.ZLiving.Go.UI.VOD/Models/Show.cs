﻿using Efusion.ZLiving.Go.UI.Common.Models;
using Prism.Navigation;

namespace Efusion.ZLiving.Go.UI.VOD.Models
{
    public class Show : ResourceBase
    {
        public Show(INavigationService navigationService) : base(navigationService) {

        }

        public override NavigationParameters NavigationParameters { get => new NavigationParameters($"id={Id}"); }
        public override string NavigationPage { get => "show-details"; }
        
    }
}
