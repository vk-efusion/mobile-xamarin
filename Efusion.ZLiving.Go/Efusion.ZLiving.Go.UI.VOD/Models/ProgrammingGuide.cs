﻿using Efusion.ZLiving.Go.UI.Common.Models;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.UI.VOD.Models
{
    public class ProgrammingGuide: ResourceBase
    {
      
        public string LivePlayListID { get; set; } = String.Empty;
        public override NavigationParameters NavigationParameters { get => new NavigationParameters($"id={Id}&title={Name}"); }
        public override string NavigationPage { get => "programming-guide"; }

        public ProgrammingGuide(INavigationService navigationService) : base(navigationService)
        {

        }

    }
}
