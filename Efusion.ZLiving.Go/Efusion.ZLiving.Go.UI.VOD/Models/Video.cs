﻿using Efusion.ZLiving.Go.UI.Common.Models;
using Prism.Navigation;
using System;

namespace Efusion.ZLiving.Go.UI.VOD.Models
{
    public class Video : ResourceBase
    {
        public string PlayListID { get; set; } = String.Empty;        
        public string Title{ get; set; } = String.Empty;
        public override NavigationParameters NavigationParameters { get => new NavigationParameters($"id={Id}&title={Title}"); }
        public override string NavigationPage { get => "fullscreen-video"; }

        public Video(INavigationService navigationService) : base(navigationService)
        {
            this.ModalNavigation = true;
        }      
    }
}
