﻿using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.VOD.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FullScreenVideoPage : ZLivingBasePage
    {
        public FullScreenVideoPage(ISessionManager sessionManager) : base(sessionManager, false)
        {
            InitializeComponent();            
        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
            
        }
    }
}