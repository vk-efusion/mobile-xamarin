﻿using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.VOD.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EpisodeDetailsPage : ZLivingBasePage
    {
        public EpisodeDetailsPage(ISessionManager sessionManager) : base(sessionManager)
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.VideoPlayerControl.Play();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            this.VideoPlayerControl.Pause();
        }
    }
}