﻿using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.VOD.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchPage : ZLivingBasePage
    {
        public SearchPage(ISessionManager sessionManager) : base(sessionManager)
        {
            InitializeComponent();
        }
	}
}