﻿using Xamarin.Forms.Xaml;
using Efusion.ZLiving.Go.UI.Common.Views;
using Efusion.ZLiving.Go.UI.Common.Contracts;

namespace Efusion.ZLiving.Go.UI.VOD.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowDetailsPage : ZLivingBasePage
    {
        public ShowDetailsPage(ISessionManager sessionManager) : base(sessionManager, false)
        {
            InitializeComponent();
        }
	}
}