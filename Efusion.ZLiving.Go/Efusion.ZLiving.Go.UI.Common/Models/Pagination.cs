﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.UI.Common.Models
{
    public class Pagination
    {
        public int? Current{ get; set; }
        public int? Previous { get; set; }
        public int? Next { get; set; }
        public int? PerPage { get; set; }
        public int? Pages { get; set; }

        public Pagination() {
            this.Current = 0;
            this.Previous = null;
            this.Next = 1;
            this.PerPage = null;
            this.Pages = 0;
        }

        public bool HasMorePages { get { return (Next != null); } }
    }
}
