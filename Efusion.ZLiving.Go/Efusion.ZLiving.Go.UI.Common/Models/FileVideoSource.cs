﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Common.Models
{
    public class FileVideoSource : VideoSource
    {
        public static readonly BindableProperty FileProperty =
                  BindableProperty.Create(nameof(File), typeof(string), typeof(FileVideoSource));

        public string File {
            set { SetValue(FileProperty, value); }
            get { return (string)GetValue(FileProperty); }
        }

        public override string ToString()
        {
            return this.File;
        }
    }
}
