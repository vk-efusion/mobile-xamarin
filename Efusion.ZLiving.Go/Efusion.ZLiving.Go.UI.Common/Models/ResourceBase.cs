﻿using Prism.Commands;
using Prism.Navigation;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Efusion.ZLiving.Go.UI.Common.Models
{
    public abstract class ResourceBase : IResource
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public ICommand ViewResourceCommannd { get; }
        public INavigationService NavigationService { get; set; }
        public abstract NavigationParameters NavigationParameters { get; }
        public abstract string NavigationPage { get; }
        public virtual bool ModalNavigation { get; set; } = false;

        public ResourceBase(INavigationService navigationService)
        {
            this.NavigationService = navigationService;
            this.ViewResourceCommannd = new DelegateCommand(ViewResource);
        }

        public async virtual void ViewResource() {
            await NavigationService.NavigateAsync(this.NavigationPage, this.NavigationParameters,this.ModalNavigation);
        }
    }
        
}
