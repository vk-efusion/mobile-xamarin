﻿using System;
using System.Collections;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;

namespace Efusion.ZLiving.Go.UI.Common.Views
{
    public class ZLivingScrollView: ScrollView
    {
        private double x = 0;

        public static readonly BindableProperty LoadMoreCommandProperty = 
        BindableProperty.CreateAttached("LoadMoreCommand", typeof(ICommand), typeof(ZLivingScrollView), default(ICommand));
             
        public static readonly BindableProperty ItemsSourceProperty =
        BindableProperty.CreateAttached("ItemsSource", typeof(IEnumerable), typeof(ZLivingScrollView), default(IEnumerable));

        public static readonly BindableProperty ItemTemplateProperty =
        BindableProperty.CreateAttached("ItemTemplate", typeof(DataTemplate), typeof(ZLivingScrollView), default(DataTemplate));

        public ZLivingScrollView() {

            this.Scrolled += (sender, e) => LoadMore(sender, e);
        }

        public ICommand LoadMoreCommand
        {
            get { return (ICommand)GetValue(LoadMoreCommandProperty); }
            set { SetValue(LoadMoreCommandProperty, value); }
        }

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        public void Render()
        {
            if (this.ItemTemplate == null || this.ItemsSource == null)
                return;

            var layout = new StackLayout
            {
                Orientation = this.Orientation == ScrollOrientation.Vertical
                ? StackOrientation.Vertical : StackOrientation.Horizontal
            };

            foreach (var item in this.ItemsSource)
            {
                var viewCell = this.ItemTemplate.CreateContent() as ViewCell;                
                viewCell.View.BindingContext = item;
                layout.Children.Add(viewCell.View);
            }

            this.Content = layout;
            
        }

        public void LoadMore(object sender, ScrolledEventArgs e)
        {
            if (!(sender is ScrollView item))
                return;

            // TODO: remove when bug will be fixed
            // https://forums.xamarin.com/discussion/59555/scrollview-position-resets-after-resizing-layout-when-running-on-ios
            // https://bugzilla.xamarin.com/show_bug.cgi?id=33385
            // https://bugzilla.xamarin.com/show_bug.cgi?id=43947

            if (Device.RuntimePlatform == Device.iOS)
            {
                if (Math.Abs(e.ScrollX) < Double.Epsilon && x > 15)
                {
                    item.ScrollToAsync(x, 0, false);
                    return;
                }

                if (Math.Abs(item.ScrollX) > Double.Epsilon)
                    x = item.ScrollX;
            }

            var scrollingSpace = item.ContentSize.Width - item.Width;

            if (scrollingSpace == e.ScrollX)
            {
                if (LoadMoreCommand != null && LoadMoreCommand.CanExecute(null))
                {
                    LoadMoreCommand.Execute(null);
                    this.Render();
                    item.ScrollToAsync(e.ScrollX - 10 , 0, false);
                }
            }
        }


    }
}
