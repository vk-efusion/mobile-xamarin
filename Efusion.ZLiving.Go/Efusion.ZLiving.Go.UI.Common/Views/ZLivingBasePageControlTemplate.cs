﻿using Efusion.ZLiving.Go.UI.Common.Effects;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Common.Views
{
    public class ZLivingBasePageControlTemplate: Grid
    {
        public ZLivingBasePageControlTemplate() {
      
        RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.1, GridUnitType.Star) });
        RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.9, GridUnitType.Star) });
            
            this.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.VerticalOptions = LayoutOptions.FillAndExpand;
            this.Margin = 0;
            this.Padding = 0;
            this.BackgroundColor = Color.Black;
            Label titleLabel = new Label
            {
                LineBreakMode = LineBreakMode.TailTruncation,
                TextColor = Color.White,
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
                FontFamily = "Calibri",
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Margin = 0,
                HeightRequest = 30,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Start
            };

            titleLabel.SetBinding(Label.TextProperty, new TemplateBinding("Title"));
            titleLabel.Effects.Add(new TitleGradientBackgroundEffect());
            
            var contentPresenter = new ContentPresenter();            
            Children.Add(titleLabel, 0, 0);
            Children.Add(contentPresenter, 0, 1);
        }
    }
}
