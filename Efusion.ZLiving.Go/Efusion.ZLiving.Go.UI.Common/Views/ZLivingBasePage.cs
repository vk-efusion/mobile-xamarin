﻿using BuildIt.States;
using BuildIt.States.Interfaces;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.States;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Common.Views
{
    public class ZLivingBasePage : ContentPage
    {
        public ISessionManager SessionManager { get; set; }
        private IStateManager StateManager { get; set; }        
        public bool HasHeaderBar { get; set; }

        public static readonly BindableProperty SearchPlaceHolderTextProperty = BindableProperty.Create(nameof(SearchPlaceHolderText), typeof(string), typeof(ZLivingBasePage), string.Empty);
        public static readonly BindableProperty SearchTextProperty = BindableProperty.Create(nameof(SearchText), typeof(string), typeof(ZLivingBasePage), string.Empty);
        public static readonly BindableProperty SearchCommandProperty = BindableProperty.Create(nameof(SearchCommand), typeof(ICommand), typeof(ZLivingBasePage));

        public string SearchPlaceHolderText { get => GetValue(SearchPlaceHolderTextProperty) as string; set => SetValue(SearchPlaceHolderTextProperty, value); }
        public string SearchText { get => GetValue(SearchTextProperty) as string; set => SetValue(SearchTextProperty, value); }
        public ICommand SearchCommand { get => GetValue(SearchCommandProperty) as ICommand; set => SetValue(SearchCommandProperty, value); }


        public ZLivingBasePage() {            
            SetValue(ViewModelLocator.AutowireViewModelProperty, true);            
            StateManager = new StateManager();
            this.ConfigureStateGroups();
            NavigationPage.SetHasBackButton(this, false);
            NavigationPage.SetHasNavigationBar(this, true);
            this.SearchCommand = new DelegateCommand(Search);
        }
        
        public ZLivingBasePage(ISessionManager sessionManager, bool HasHeaderBar = true) : this()
        {
            this.SessionManager = sessionManager;
            this.HasHeaderBar = HasHeaderBar;

            if (this.HasHeaderBar)
            {
                this.ControlTemplate = new ControlTemplate(typeof(ZLivingBasePageControlTemplate));
            }
            
        }

        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.ChangeVisualState();
            
            
        }

        protected override void ChangeVisualState()
        {
            base.ChangeVisualState();
            BuildIt.Forms.VisualStateManager.GoToState(this, this.SessionManager.AuthenticationState.ToString());
            BuildIt.Forms.VisualStateManager.GoToState(this, (this.BindingContext as BaseViewModel)?.PageState.ToString());
        }

        private void ConfigureStateGroups()
        {
            StateManager.Group<AuthenticationState>().DefineAllStates();            
            StateManager.Group<PageState>().DefineAllStates();
            this.ConfigureStateManagerGroups(this.StateManager);
            BuildIt.Forms.VisualStateManager.Bind(this, this.StateManager);
        }

        /// <summary>
        /// Add the Grops that you want to bind to the page as -> StateManager.Group<EnumGroupsType>().DefineAllStates();
        /// </summary>
        /// <param name="StateManager"></param>
        public virtual void ConfigureStateManagerGroups(IStateManager stateManager)
        {
           
        }

        public void Search() {
            (this.BindingContext as BaseViewModel)?.SearchCommand?.Execute(this.SearchText);
        }
    }
}
