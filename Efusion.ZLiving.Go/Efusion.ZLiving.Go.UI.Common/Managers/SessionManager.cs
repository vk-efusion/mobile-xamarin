﻿using Efusion.ZLiving.Go.APIs.Auth.Exceptions;
using Efusion.ZLiving.Go.APIs.Auth.Services;
using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.Models;
using Prism.Mvvm;
using System;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.Common.Managers
{
    public class SessionManager: BindableBase, ISessionManager
    {       
        public IFacebookManager FacebookManager { get; set; }
        public IGoogleManager GoogleManager { get; set; }
        public IAuthenticationService AuthenticationService { get; set; }
        public IBaseSettings Settings { get; set; }
        
        public SessionManager(IGoogleManager googleManager, IFacebookManager facebookManager, IAuthenticationService authenticationService, IBaseSettings settings)
        {
            this.AuthenticationService = authenticationService;
            this.FacebookManager = facebookManager;
            this.GoogleManager = googleManager;
            this.Settings = settings;
        }

        public async Task<AuthenticationState> LogInWithFacebook()
        {
            await this.FacebookManager.Login(OnLoginComplete, LogOut, Settings.GetSetting("facebook_client_id"), Settings.GetSetting("facebook_authorise_url"), Settings.GetSetting("facebook_redirect_url"));
            return this.AuthenticationState;

        }

        public async Task<AuthenticationState> LogInWithGoogle()
        {
            await this.GoogleManager.Login(OnLoginComplete, LogOut, Settings.GetSetting("google_client_id"),  Settings.GetSetting("google_authorise_url"), Settings.GetSetting("google_redirect_url"));
            return this.AuthenticationState;
        }


        public async Task<AuthenticationState> LogIn(string email,string password)
        {
            var response = await this.AuthenticationService.Login(email, password);
            var consumer = await this.AuthenticationService.GetConsumer(email);

            var token = Convert.ToString(response.access_token);            
            Settings.SetSetting("access_token", token);                                    
            Settings.SetSetting("consumer_id", Convert.ToString(consumer._id));

            this.RaisePropertyChanged("AuthenticationState");
            return this.AuthenticationState;
        }

        public AuthenticationState LogOut()
        {
            Settings.SetSetting("access_token", String.Empty);
            Settings.SetSetting("consumer_id", String.Empty);
            this.RaisePropertyChanged("AuthenticationState");
            return this.AuthenticationState;
        }

        public async Task LinkDevice(string pin)
        {            
            if (!this.AuthenticationState.Equals(AuthenticationState.Authenticated)) {
                throw new UnauthorizedAccessException("You Have to be Authenticated in order to Link a Device");
            }

            await this.AuthenticationService.LinkDevice(pin);
        }

        public AuthenticationState AuthenticationState
        {
            get
            {
                bool accessTokenExists = !String.IsNullOrWhiteSpace(Settings.GetSetting("access_token"));
                return accessTokenExists ? AuthenticationState.Authenticated : AuthenticationState.Anonymous;
            }            
        }

        public string AccessToken {
            get 
                {
                    if (this.AuthenticationState.Equals(AuthenticationState.Anonymous))
                    {
                        throw new UserNotFoundException("Access Token Not Found");
                    }
                    return this.Settings.GetSetting("access_token");
                }
        }

        private async Task OnLoginComplete(SocialNetworkUser socialMeediaUser, string message)
        {
            SocialNetworkUser user = socialMeediaUser ?? throw new UserNotFoundException();
            var deviceId = $"Facebook:{socialMeediaUser.Id}";
            var pinResponse = await this.AuthenticationService.AcquireDevicePin(deviceId);
            var pin = Convert.ToString(pinResponse.pin);
            var consumer = await this.AuthenticationService.GetConsumer(user.Email);
            Settings.SetSetting("consumer_id", Convert.ToString(consumer._id));
            await this.AuthenticationService.LinkDevice(pin);
            var response = await this.AuthenticationService.LoginDevice(deviceId,pin);            
            var token = Convert.ToString(response.access_token);
            Settings.SetSetting("access_token", token);
            
            this.RaisePropertyChanged("AuthenticationState");            

        }
    }
}
