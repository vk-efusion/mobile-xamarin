﻿using Efusion.ZLiving.Go.UI.Common.Extensions;
using Efusion.ZLiving.Go.UI.Common.States;
using System;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Common.Behaviors
{
    public class PageVisualStateBehavior : Behavior<ContentPage>
    {
        public static readonly BindableProperty PageStateProperty =
        BindableProperty.CreateAttached("PageState", typeof(PageState), typeof(PageVisualStateBehavior), PageState.Success, propertyChanged: OnStateChanged);

        private static void OnStateChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var page = (bindable as Element).GetZLivingPage();
            BuildIt.Forms.VisualStateManager.GoToState(page ?? bindable as Element, Convert.ToString(newValue));
        }

        public static PageState GetPageState(BindableObject view)
        {
            return (PageState)view.GetValue(PageStateProperty);
        }

        public static void SetPageState(BindableObject view, PageState value)
        {
            view.SetValue(PageStateProperty, value);
        }
    }
}
