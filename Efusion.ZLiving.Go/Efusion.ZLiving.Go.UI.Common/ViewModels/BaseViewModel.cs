﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.States;
using Microsoft.AppCenter.Crashes;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace Efusion.ZLiving.Go.UI.Common.ViewModels
{
    public class BaseViewModel : BindableBase, INavigationAware
    {
        private bool isBusy;
        private string title;        
        private FooterViewModel footer;
        private PageState pageState;
        public ICommand SearchCommand { get; }
        public AuthenticationState AuthenticationState { get => this.SessionManager.AuthenticationState; }
        public PageState PageState { get => pageState; set => SetProperty(ref pageState, value); }
        public IBaseSettings Settings { get; set; }
        public INavigationService NavigationService { get; set; }
        public ISessionManager SessionManager { get; set; }

        public bool IsBusy { get => isBusy; set => SetProperty(ref isBusy, value); }        
        public string Title { get => title; set => SetProperty(ref title, value); }
        public virtual string ViewModelName => "BaseViewModel";
        public FooterViewModel Footer { get => footer; set => SetProperty(ref footer, value); }
      
        public BaseViewModel(FooterViewModel footerViewModel, ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : this(sessionManager, navigationService, settings)
        {
            this.Footer = footerViewModel;
        }

        public BaseViewModel(ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : this(navigationService, settings)
        {
            this.SessionManager = sessionManager;
          
        }

        public BaseViewModel(INavigationService navigationService, IBaseSettings settings)
        {
            NavigationService = navigationService;
            this.Settings = settings;
            this.isBusy = false;
            this.SearchCommand = new DelegateCommand<string>(Search);            
        }


        public virtual async void Search(string query) {
            await this.NavigationService.NavigateAsync("search", new NavigationParameters($"query={query}"));
        }


        public void TrackError(Exception ex, string where, string why, string parameters = "")
        {

            Crashes.TrackError(ex, new Dictionary<string, string>{
                        { "ViewModel", this.ViewModelName },
                        { "Where", where },
                        { "Why", why},
                        { "Parameters", parameters} });
        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
                
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {            
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {
            
        }        
    }

}
