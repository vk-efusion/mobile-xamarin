﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Prism.Commands;
using Prism.Navigation;
using System.Windows.Input;

namespace Efusion.ZLiving.Go.UI.Common.ViewModels
{
    public class FooterViewModel : BaseViewModel
    {
        public ICommand GoBackCommand { get; }        
        public ICommand GoToMenuCommand { get; }        

        public FooterViewModel(ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : base(sessionManager, navigationService, settings)
        {            
            this.GoBackCommand = new DelegateCommand(GoBack);
            this.GoToMenuCommand = new DelegateCommand(OpenMenu);
        }

        public async void OpenMenu()
        {
            await NavigationService.NavigateAsync("menu");
        }

        public async void GoBack()
        {          
            await NavigationService.GoBackAsync();
        }        
    }
}
