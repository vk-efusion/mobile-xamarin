﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Common.Effects
{
    public class TitleGradientBackgroundEffect: RoutingEffect
    {
        public TitleGradientBackgroundEffect() : base("Efusion.ZLiving.Go.UI.Common.Effects.TitleGradientBackgroundEffect")
        {

        }
    }
}
