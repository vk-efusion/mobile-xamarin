﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.UI.Common.Enums
{
    public enum VideoStatus
    {
        NotReady,
        Playing,
        Paused
    }
}
