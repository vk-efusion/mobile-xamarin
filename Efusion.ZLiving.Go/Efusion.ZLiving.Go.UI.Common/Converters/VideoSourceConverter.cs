﻿using Efusion.ZLiving.Go.UI.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Common.Converters
{
    public class VideoSourceConverter : TypeConverter
    {
        public override object ConvertFromInvariantString(string value)
        {
            if (!String.IsNullOrWhiteSpace(value))
            {
                return Uri.TryCreate(value, UriKind.Absolute, out Uri uri) && uri.Scheme != "file" ?
                                VideoSource.FromUri(value) : VideoSource.FromResource(value);
            }

            throw new InvalidOperationException("Cannot convert null or whitespace to ImageSource");
        }
    }
}
