﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.UI.Common.States
{
    public enum PageState
    {
        Success,
        Error,        
        UnexpectedException
    }
}
