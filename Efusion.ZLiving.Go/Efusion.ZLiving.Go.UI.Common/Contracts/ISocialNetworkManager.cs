﻿using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.Common.Contracts
{
    public interface ISocialNetworkManager
    {
        Task Login(Func<SocialNetworkUser, string, Task> onLoginComplete, Func<AuthenticationState> OnLogoutRequested , string clientId, string authoriseUrl, string redirectUrl);
        AuthenticationState Logout();
    }
}
