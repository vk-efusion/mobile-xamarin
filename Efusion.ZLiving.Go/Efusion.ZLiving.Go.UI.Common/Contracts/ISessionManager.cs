﻿using Efusion.ZLiving.Go.UI.Common.Enums;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Common.Contracts
{
    public interface ISessionManager
    {
        Task LinkDevice(string pin);
        Task<AuthenticationState> LogIn(string email, string password);
        Task<AuthenticationState> LogInWithFacebook();
        Task<AuthenticationState> LogInWithGoogle();
        AuthenticationState LogOut();
        AuthenticationState AuthenticationState { get; }
        string AccessToken { get; }
    }        
}
