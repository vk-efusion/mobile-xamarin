﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Efusion.ZLiving.Go.UI.Common.Models
{
    public interface IResource
    {
        string Id { get; set; }
        string Name { get; set; }        
        string ImageUrl { get; set; }
        ICommand ViewResourceCommannd { get; }
    }
}
