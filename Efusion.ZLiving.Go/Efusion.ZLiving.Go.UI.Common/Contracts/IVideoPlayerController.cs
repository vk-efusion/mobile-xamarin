﻿using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.UI.Common.Contracts
{
    public interface IVideoPlayerController
    {
        VideoStatus Status { set; get; }

        TimeSpan Duration { set; get; }
    }
}
