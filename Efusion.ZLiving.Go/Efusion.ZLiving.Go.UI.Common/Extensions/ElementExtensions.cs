﻿using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms;

namespace Efusion.ZLiving.Go.UI.Common.Extensions
{
    public static class ElementExtensions
    {
        public static ZLivingBasePage GetZLivingPage(this Element element)
        {            
            var elementAux = element as Element;

            while (elementAux != null)
            {
                if (elementAux is ZLivingBasePage)
                {
                    return elementAux as ZLivingBasePage;
                }

                elementAux = elementAux.Parent;         
            }
            
            return null;
        }

    }
}
