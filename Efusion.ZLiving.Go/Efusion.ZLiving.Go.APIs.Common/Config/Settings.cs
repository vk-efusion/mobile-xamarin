﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Config
{
    public class Settings: IBaseSettings
    {
        //Key - DefaultValue
        private IDictionary<string, string> DefaultValues { get; set; }

        public Settings(IGlobalDefaultSettings defaultSettings) {
            DefaultValues = new Dictionary<string, string>();
            foreach (string key in defaultSettings.DefaultSettings.Keys)
            {
                this.SetDefaultSetting(key, defaultSettings.DefaultSettings[key]);
            }
        }

        private ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        public string GetDefaultSetting(string key)
        {
            return  DefaultValues.ContainsKey(key) ? DefaultValues[key] : String.Empty;
        }

        public void SetDefaultSetting(string key, string value)
        {
            if (DefaultValues.ContainsKey(key))
            {
                DefaultValues[key] = value;
            }
            else {
                DefaultValues.Add(key, value);
            }               
        }

        public string GetSetting(string key)
        {
            return AppSettings.GetValueOrDefault(key, DefaultValues.ContainsKey(key) ? DefaultValues[key] : String.Empty);
        }

        public void SetSetting(string key, string value)
        {
            AppSettings.AddOrUpdateValue(key, value);
        }       
    }
}
