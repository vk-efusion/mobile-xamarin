﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Config
{
    public interface IGlobalDefaultSettings
    {
       IDictionary<string, string> DefaultSettings { get; set; }
    }
}
