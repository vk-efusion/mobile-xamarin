﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Config
{
    public interface IBaseSettings
    {
        string GetDefaultSetting(string key);
        void SetDefaultSetting(string key, string value);
        string GetSetting(string key);
        void SetSetting(string key, string value);
    }
}
