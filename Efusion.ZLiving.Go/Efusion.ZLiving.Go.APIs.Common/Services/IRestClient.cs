﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Common.Services
{
    public interface IRestClient
    {
        Task<dynamic> ExecuteHTTPRequestAsync(string baseEndpoint, string endpoint, Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters,string userAgent = "");
        dynamic ExecuteHTTPRequest(string baseEndpoint, string endpoint, Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters, string userAgent = "");
    }
}
