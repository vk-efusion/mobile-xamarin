﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Enums;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Services
{
    public class ZLivingGoBaseService: JsonBaseService
    {
        public DeviceType DeviceType { get; set; } = DeviceType.Android;

        public ZLivingGoBaseService(IRestClient jsonClient, IBaseSettings settings) : base(jsonClient, settings)
        {

        }

        public string UserAgent
        {
            get => (DeviceType == DeviceType.Android) ? "zype android" : "cfnetwork";
        }

        public string GetAppKey() {

            return Settings.GetSetting($"{DeviceType.ToString().ToLower()}_app_key");
        }

        public (string, string, ParameterType) GetAuthenticationParameter() {
            return String.IsNullOrWhiteSpace(Settings.GetSetting("access_token")) ? ("app_key", this.GetAppKey(), ParameterType.QueryString) : ("access_token", Settings.GetSetting("access_token"), ParameterType.QueryString);                     
        }
    }
}
