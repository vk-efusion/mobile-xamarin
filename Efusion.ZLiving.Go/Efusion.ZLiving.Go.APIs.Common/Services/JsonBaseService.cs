﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Enums;
using Microsoft.AppCenter.Crashes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Services
{
    public class JsonBaseService
    {        
        public IRestClient Client { get; set; }
        public IBaseSettings Settings { get; set; }
        public virtual string ServiceName { get; } = "base-service";

        public JsonBaseService(IRestClient jsonClient, IBaseSettings settings) {
            this.Client = jsonClient;
            this.Settings = settings;
        }

        public void TrackError(Exception ex,string where, string why, string parameters = "") {

            Crashes.TrackError(ex, new Dictionary<string, string>{
                        { "Service", this.ServiceName },
                        { "Where", where },
                        { "Why", why},
                        { "Parameters", parameters} });
        }
    }
}
