﻿using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Common.Services
{
    public class JsonRestClient : IRestClient
    {
        public virtual RestClient GetNewClient(string baseEndpoint) { return new RestClient(baseEndpoint); }

        public dynamic ExecuteHTTPRequest(string baseEndpoint, string endpoint, Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters, string userAgent = "")
        {
            var client = GetNewClient(baseEndpoint);
            
            if (!String.IsNullOrWhiteSpace(userAgent))
            {
                client.UserAgent = userAgent;
            }

            var request = new RestRequest(endpoint, method);

            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");

            if (headers != null)
            {
                foreach ((string Key, string Value) in headers)
                {
                    request.AddHeader(Key, Value);
                }
            }

            if (parameters != null)
            {
                foreach ((string Key, string Value, ParameterType ParamType) in parameters)
                {
                    request.AddParameter(Key, Value, ParamType);
                }
            }
            

            IRestResponse response = client.Execute(request);
            
            if (!response.IsSuccessful)
            {
                var ex = new RestHTTPException($"Error retrieving response from: {baseEndpoint}/{endpoint}  Check inner details for more info.", response);

                Crashes.TrackError(ex, new Dictionary<string, string>{
                        { "Service", "RestClient" },
                        { "Where", "ExecuteHTTPRequestAsync" },
                        { "Why", ex.Message}});

                throw ex;
            }

            return JsonConvert.DeserializeObject(response.Content);
        }

        
        public async Task<dynamic> ExecuteHTTPRequestAsync(string baseEndpoint, string endpoint,  Method method, IList<(string Key, string Value)> headers, IList<(string Key, string Value, ParameterType ParamType)> parameters, string userAgent = "")
        {
            var client = GetNewClient(baseEndpoint);            

            var request = new RestRequest(endpoint, method);

            if (!String.IsNullOrWhiteSpace(userAgent))
            {
                client.UserAgent = userAgent;
            }

            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");

            if (headers != null)
            {
                foreach ((string Key, string Value) in headers)

                {
                    request.AddHeader(Key, Value);
                }
            }

            if (parameters != null)
            {
                foreach ((string Key, string Value, ParameterType ParamType) in parameters)
                {
                    request.AddParameter(Key, Value, ParamType);
                }
            } 

            IRestResponse response = await client.ExecuteTaskAsync(request);
            
            if (!response.IsSuccessful) {
                var ex = new RestHTTPException($"Error retrieving response from: {baseEndpoint}/{endpoint}  Check inner details for more info.",response);

                Crashes.TrackError(ex, new Dictionary<string, string>{
                        { "Service", "RestClient" },
                        { "Where", "ExecuteHTTPRequestAsync" },
                        { "Why", ex.Message}});
                throw ex;
            }

            return  JsonConvert.DeserializeObject(response.Content);
        }
        
    }
}
