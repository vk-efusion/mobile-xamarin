﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Extensions
{
    public static class ListExtensions
    {
        public static bool ContainsType<T,W>(this IReadOnlyList<T> list, T excludeItem) {

            IEnumerator<T> enumerator =  list.GetEnumerator();

            while (enumerator.MoveNext()) {
                if ((enumerator.Current is W) & (!enumerator.Current.Equals(excludeItem))){
                    return true;
                }
            }
            return false;
        }

    }
}
