﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Exceptions
{
    public class ResourceNotFoundException: ApplicationException
    {
        public ResourceNotFoundException(string message = null) : base(message) {

        }
    }
}
