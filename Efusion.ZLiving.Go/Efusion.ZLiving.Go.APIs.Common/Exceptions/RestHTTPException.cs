﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efusion.ZLiving.Go.APIs.Common.Exceptions
{
    public class RestHTTPException : Exception
    {
        public IRestResponse Response { get; set; }

        public RestHTTPException(IRestResponse response) {
            this.Response = response;
        }

        public RestHTTPException(string message, IRestResponse response):base(message)
        {
            this.Response = response;
        }
    }
}
