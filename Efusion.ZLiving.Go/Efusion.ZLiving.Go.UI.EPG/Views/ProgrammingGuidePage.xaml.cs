﻿using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms.Xaml;

namespace Efusion.ZLiving.Go.UI.EPG.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProgrammingGuidePage : ZLivingBasePage
    {
        public ProgrammingGuidePage(ISessionManager sessionManager) : base(sessionManager)
        {
            InitializeComponent();
        }
	}
}