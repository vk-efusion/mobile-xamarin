﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.EPG.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.ViewModels;
using Efusion.ZLiving.Go.UI.VOD.Extensions;
using Efusion.ZLiving.Go.UI.VOD.Models;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.EPG.ViewModels
{
    public class ProgrammingGuidePageViewModel: BaseViewModel
    {        
        private Playlist programmingGuide;
        public Playlist ProgrammingGuide { get => programmingGuide; set => SetProperty(ref programmingGuide, value); }
        public IElectronicProgrammingGuideService ElectronicProgrammingGuideService { get; set; }
        
        public ProgrammingGuidePageViewModel(FooterViewModel footerViewModel, IElectronicProgrammingGuideService electronicProgrammingGuideService, ISessionManager sessionManager, INavigationService navigationService, IBaseSettings settings) : base(footerViewModel, sessionManager, navigationService, settings)
        {
            this.ElectronicProgrammingGuideService = electronicProgrammingGuideService;
            ProgrammingGuide = new Playlist(navigationService,null);
        }

        public async override void OnNavigatedTo(NavigationParameters parameters)
        {            
            if (ProgrammingGuide.Count == 0)
            {
                base.OnNavigatedTo(parameters);
                string pgId = String.Empty;

                if (!parameters.TryGetValue("id", out pgId))
                {
                    throw new KeyNotFoundException("To load a Programming Guide View Model the \"id\" is needed. Send it as a navigation parameter");
                }

                string title = String.Empty;

                if (!parameters.TryGetValue("title", out title))
                {
                    throw new KeyNotFoundException("Missing Programming Guide Name. Send it as \"title\" in the navigation parameter");
                }

                this.Title = title;
                this.ProgrammingGuide.Name = this.Title;

                await this.Load(pgId);
            }
        }

        
        public async Task Load(string pgId)
        {
            try
            {
                this.IsBusy = true;
                this.ProgrammingGuide.Clear();

                dynamic videos = await this.ElectronicProgrammingGuideService.GetProgrammingGuide(pgId);

                foreach (var video in videos) {
                    this.ProgrammingGuide.Add(new LiveVideo(this.NavigationService).Populate(this.ProgrammingGuide.Name, video as JObject));
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(new Exception("unexpected-exception", ex));
            }
            finally
            {

                this.IsBusy = false;
            }

        }

    }
}
