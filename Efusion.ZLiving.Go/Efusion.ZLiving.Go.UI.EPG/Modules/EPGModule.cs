﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.EPG.Services;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Managers;
using Efusion.ZLiving.Go.UI.EPG.Views;
using Prism.Ioc;
using Prism.Modularity;

namespace Efusion.ZLiving.Go.UI.EPG.Modules
{
    public class EPGModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IBaseSettings, Settings>();
            containerRegistry.RegisterSingleton<IRestClient, JsonRestClient>();
            containerRegistry.RegisterSingleton<IElectronicProgrammingGuideService, ElectronicProgrammingGuideService>();            
            containerRegistry.RegisterSingleton<ISessionManager, SessionManager>();
            containerRegistry.RegisterForNavigation<ProgrammingGuidePage>("programming-guide");
            

        }
    }
}
