using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.APIs.Content.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Efusion.ZLiving.Go.APIs.Content.Test.Services
{
    public class ContentServiceTest : IClassFixture<SettingsFixture>
    {
        public SettingsFixture Settings { get; set; }

        public ContentServiceTest(SettingsFixture settings)
        {
            this.Settings = settings;
        }

        [Fact(DisplayName = "Get Banner Content - Error Handling Test")]
        public async void GetBannerContent_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IContentService service = new ContentService(restClient.Object, Settings);
            //Will verify that if the MockExecuteHTTPRequestAsync throws an Exception, it is going to be re-throw and not encapsulated
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("zobject_endpoint"), HttpStatusCode.NotFound);
            Exception exception = await Record.ExceptionAsync(() => service.GetBannerContent());            
            Assert.IsType<RestHTTPException>(exception);

            //Will verify that if response.Count == 0, will throw an ResourceNotFoundException
            var parameters = new List<(string, string, ParameterType)>()
            {
                ("zobject_type","slider",ParameterType.QueryString),
                (service as ContentService).GetAuthenticationParameter()
            };

            dynamic contentResponse = new ExpandoObject();
            contentResponse.response = new List<ExpandoObject>();

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("zobject_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(contentResponse));
            exception = await Record.ExceptionAsync(() => service.GetBannerContent());
            Assert.IsType<ResourceNotFoundException>(exception);
        }

        [Fact(DisplayName = "Get Banner Content - Correct Settings Use Test")]
        public async void GetBannerContent_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IContentService service = new ContentService(restClient.Object, Settings);

            var parameters = new List<(string, string, ParameterType)>()
            {
                ("zobject_type","slider",ParameterType.QueryString),
                (service as ContentService).GetAuthenticationParameter()
            };

            dynamic contentResponse = new ExpandoObject();
            contentResponse.response = new List<ExpandoObject>() { new ExpandoObject() };

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("zobject_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(contentResponse));
            await service.GetBannerContent();
            
            //Verify that the MockExecuteHTTPRequestAsync was called using same parameters and endpoints as the test,
            restClient.VerifyAll();
        }


        [Fact(DisplayName = "Get Content - Error Handling Test")]
        public async void GeContent_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IContentService service = new ContentService(restClient.Object, Settings);
            //Will verify that if the MockExecuteHTTPRequestAsync throws an Exception, it is going to be re-throw and not encapsulated
            restClient.MockExecuteHTTPRequestException(Settings.GetSetting("zobject_endpoint"), HttpStatusCode.NotFound);
            Exception exception = Record.Exception(() => service.GetContent("about-us"));
            Assert.IsType<RestHTTPException>(exception);

            //Will verify that if response.Count == 0, will throw an ResourceNotFoundException
            var parameters = new List<(string, string, ParameterType)>()
            {
                ("zobject_type","content_pages",ParameterType.QueryString),
                (service as ContentService).GetAuthenticationParameter()
            };

            dynamic contentResponse = new ExpandoObject();
            contentResponse.response = new List<ExpandoObject>();

            restClient.MockExecuteHTTPRequest(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("zobject_endpoint"), Method.GET, null, parameters, contentResponse);
            exception = Record.Exception(() => service.GetContent("about-us"));
            Assert.IsType<ResourceNotFoundException>(exception);
        }

        [Fact(DisplayName = "Get Content - Correct Settings Use Test")]
        public void GetContent_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IContentService service = new ContentService(restClient.Object, Settings);

            var parameters = new List<(string, string, ParameterType)>()
            {
                ("zobject_type","content_pages",ParameterType.QueryString),
                 (service as ContentService).GetAuthenticationParameter()
            };

            string contentKey = String.Empty;

            dynamic aboutUscontent = new ExpandoObject();
            aboutUscontent.text = "About Us";
            aboutUscontent.title = "About Us Title";
            aboutUscontent.page_name = "about-us";

            dynamic contactUsContent = new ExpandoObject();
            contactUsContent.text = "Contact Us";
            contactUsContent.title = "Contact Us Title";
            contactUsContent.page_name = "contact-us";

            dynamic termsContent = new ExpandoObject();
            termsContent.text = "Terms";
            termsContent.title = "Terms Us Title";
            termsContent.page_name = "terms";

            dynamic contentResponse = new ExpandoObject();
            contentResponse.response = new List<ExpandoObject>()
            {
                aboutUscontent,
                contactUsContent,
                termsContent,                
            };            

            restClient.MockExecuteHTTPRequest(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("zobject_endpoint"), Method.GET, null, parameters, contentResponse);

            dynamic contentServiceResponse = service.GetContent("about-us");
            Assert.Equal(Convert.ToString(aboutUscontent.text), Convert.ToString(contentServiceResponse.Content));
            Assert.Equal(Convert.ToString(aboutUscontent.title), Convert.ToString(contentServiceResponse.Title));

            contentServiceResponse = service.GetContent("contact-us");
            Assert.Equal(Convert.ToString(contactUsContent.text), Convert.ToString(contentServiceResponse.Content));
            Assert.Equal(Convert.ToString(contactUsContent.title), Convert.ToString(contentServiceResponse.Title));

            contentServiceResponse = service.GetContent("terms");
            Assert.Equal(Convert.ToString(termsContent.text), Convert.ToString(contentServiceResponse.Content));
            Assert.Equal(Convert.ToString(termsContent.title), Convert.ToString(contentServiceResponse.Title));

            //Verify that the MockExecuteHTTPRequestAsync was called using same parameters and endpoints as the test,
            restClient.VerifyAll();
        }

    }
}
