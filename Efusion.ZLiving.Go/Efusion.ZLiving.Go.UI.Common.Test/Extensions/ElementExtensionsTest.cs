﻿using System;
using System.Collections.Generic;
using System.Text;
using Efusion.ZLiving.Go.UI.Common.Extensions;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms;
using Xunit;

namespace Efusion.ZLiving.Go.UI.Common.Test.Extensions
{
    public class ElementExtensionsTest
    {

        [Fact(DisplayName = "Get ZLiving Page - Test")]
        public void GetZLivingPage_Test()
        {

            Label label = new Label();
            Assert.Null(label.GetZLivingPage());

            StackLayout layout = new StackLayout();
            layout.Children.Add(label);

            Assert.Null(label.GetZLivingPage());

            ZLivingBasePage basePage = new ZLivingBasePage();

            ZLivingBasePage livingPage = basePage.GetZLivingPage();

            Assert.Same(basePage, livingPage);

            basePage.Content = layout;

            livingPage = label.GetZLivingPage();
            Assert.NotNull(livingPage);
            Assert.Same(basePage, livingPage);
        }

    }
}
