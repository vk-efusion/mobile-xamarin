﻿using Efusion.ZLiving.Go.UI.Common.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Efusion.ZLiving.Go.UI.Common.Test.Converters
{
    public class InvertBooleanConverterTest
    {

        [Fact(DisplayName = "Convert Test")]
        public void Convert_Test() {

            InvertBooleanConverter converter = new InvertBooleanConverter();
            bool result = (bool)converter.Convert(true, null, null, null);
            Assert.False(result);

            result = (bool)converter.Convert(false, null, null, null);
            Assert.True(result);
        }

        [Fact( DisplayName = "Convert Back Test")]
        public void ConvertBack_Test()
        {
            InvertBooleanConverter converter = new InvertBooleanConverter();
            bool result = (bool)converter.ConvertBack(true, null, null, null);
            Assert.False(result);

            result = (bool)converter.ConvertBack(false, null, null, null);
            Assert.True(result);
        }
    }
}
