﻿using Efusion.ZLiving.Go.UI.Common.Converters;
using Efusion.ZLiving.Go.UI.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Efusion.ZLiving.Go.UI.Common.Test.Converters
{
    public class VideoSourceConverterTest
    {

        [Fact(DisplayName = "ConvertFromInvariantString - Test Source Type")]
        public void ConvertFromInvariantString_TestSourceType()
        {

            VideoSourceConverter converter = new VideoSourceConverter();

            Exception ex = Record.Exception(() => converter.ConvertFromInvariantString(null));
            Assert.IsType<InvalidOperationException>(ex);

            ex = Record.Exception(() => converter.ConvertFromInvariantString(String.Empty));
            Assert.IsType<InvalidOperationException>(ex);
            
            var videoSource = converter.ConvertFromInvariantString("http://www.google.com");
            Assert.IsType<UriVideoSource>(videoSource);

            videoSource = converter.ConvertFromInvariantString("sjslshdenngo");
            Assert.IsType<ResourceVideoSource>(videoSource);

            videoSource = converter.ConvertFromInvariantString("file://efusion.zliving.go");
            Assert.IsType<ResourceVideoSource>(videoSource);
        }
    }
}
