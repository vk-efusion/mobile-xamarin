﻿using Moq;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Common.Test.Mocks
{
    public class MockINavigationService: Mock<INavigationService>
    {
        public void MockNavigateAsync(string page)
        {
            Setup(x => x.NavigateAsync(
            It.Is<string>(i => i == page)
            )).Returns(Task.CompletedTask);

        }


        public void MockNavigateAsync(string page,NavigationParameters parameters)
        {
            Setup(x => x.NavigateAsync(
            It.Is<string>(i => i == page),
            It.Is<NavigationParameters>(i => VerifyParameters(i, parameters))
            )).Returns(Task.CompletedTask);

        }


        private bool VerifyParameters(NavigationParameters used, NavigationParameters expected) {

            if ((used == null) || (expected == null)) {
                return (used == expected);
            }

            IEnumerable<KeyValuePair<string, object>> intersection = used.Intersect(expected, new KeyValuePairComparer());

            return (intersection.Count() == used.Count);
        }
    }


    public class KeyValuePairComparer : IEqualityComparer<KeyValuePair<string, object>>
    {    
        public bool Equals(KeyValuePair<string, object> x, KeyValuePair<string, object> y)
        {
            bool valueEquals = (x.Value == null)? (y.Value == null): x.Value.Equals(y.Value);
            return ((x.Key == y.Key) && (valueEquals));                                             
        }

        public int GetHashCode(KeyValuePair<string, object> obj)
        {
            int hash = 13;
             hash = (hash * 7) + ((obj.Key != null) ? obj.Key.GetHashCode() : 0);
             hash = (hash * 7) + ((obj.Value != null) ? obj.Value.GetHashCode() : 0);            
            return hash;
        }
    }

}
