﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.UI.Common.Test.Mocks
{
    public class MockSessionManager: Mock<ISessionManager>
    {        
        public void MockAuthenticationState(AuthenticationState authenticationState) {
                SetupGet(x => x.AuthenticationState).Returns(authenticationState);
        }

        public void MockLogin(string email, string password, Exception outputEx) {
            Setup(x => x.LogIn(
            It.Is<string>(i => i == email),
            It.Is<string>(i => i == password)
            )).Throws(outputEx);
        }

        public void MockLogin(string email,string password)
        {            
            Setup(x => x.LogIn(
            It.Is<string>(i => i == email),
            It.Is<string>(i => i == password)
            )).Returns(Task.FromResult(this.Object.AuthenticationState));

        }
        
        public void MockLogout() {            
            Setup(x => x.LogOut()).Returns(this.Object.AuthenticationState);
        }


        public void MockLinkDevice(string pin)
        {            
            Setup(x => x.LinkDevice(
            It.Is<string>(i => i == pin)
            )).Returns(Task.CompletedTask);
        }

        public void MockLinkDeviceException(string pin, Exception ex)
        {
            Setup(x => x.LinkDevice(
            It.Is<string>(i => i == pin)
            )).Throws(ex);

        }

    }
}
