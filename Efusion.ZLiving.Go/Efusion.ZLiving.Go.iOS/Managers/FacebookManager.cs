﻿using System;
using System.Threading.Tasks;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.Models;
using Newtonsoft.Json.Linq;
using UIKit;
using Xamarin.Auth;

namespace Efusion.ZLiving.Go.iOS.Managers
{
    public class FacebookManager : IFacebookManager
    {
        public Func<SocialNetworkUser, string, Task> OnLoginComplete { get; set; }
        
        public async Task Login(Func<SocialNetworkUser, string, Task> onLoginComplete, Func<AuthenticationState> OnLogoutRequested, string clientId, string authoriseUrl, string redirectUrl)
        {
            this.OnLoginComplete = onLoginComplete;

            var account = await FacebookManager.LoginAsync(clientId, "public_profile,email", new Uri(authoriseUrl), new Uri(redirectUrl), true);

            if (account != null)
            {
                string email = await GetFacebookUserInfoAsync(account);
                await OnCompleted(account, email);
            }

        }

        public AuthenticationState Logout()
        {
            throw new NotImplementedException();
        }


        public async Task<string> GetFacebookUserInfoAsync(Account account)
        {
            try
            {
                var request = new OAuth2Request("GET", new Uri("https://graph.facebook.com/me?fields=email"), null, account);
                var userInfoResult = await request.GetResponseAsync();
                var obj = JObject.Parse(userInfoResult.GetResponseText());

                if (obj != null)
                {
                    return obj.GetValue("email").ToString();
                }
            }
            catch (Exception ex)
            {
                // say what went wrong here.
            }
            return null;
        }

        public async Task OnCompleted(Account account, string email)
        {
            await OnLoginComplete?.Invoke(new SocialNetworkUser(account.Properties["state"], account.Properties["access_token"], String.Empty, String.Empty, email, String.Empty), string.Empty);
        }



        public static Task<Account> LoginAsync(string appId, string requestedScope, Uri authoriseUrl, Uri redirectUrl, bool allowCancel)
        {
            var tcs = new TaskCompletionSource<Account>();

            var auth = new OAuth2Authenticator(appId, requestedScope, authoriseUrl, redirectUrl)
            {
                AllowCancel = allowCancel,
            };

            auth.Completed += (sender, e) => tcs.SetResult(e.IsAuthenticated ? e.Account : null);

            auth.Error += (sender, e) => tcs.SetException(e.Exception);
            
            var ui = auth.GetUI();

            var window = UIApplication.SharedApplication.KeyWindow;
            UIViewController rootController = window.RootViewController;
            rootController?.PresentViewController(ui, true, null);

            return tcs.Task;
        }

    }
}