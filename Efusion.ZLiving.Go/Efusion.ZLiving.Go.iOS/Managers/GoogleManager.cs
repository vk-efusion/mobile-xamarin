﻿using System;
using System.Threading.Tasks;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.Models;

namespace Efusion.ZLiving.Go.iOS.Managers
{
    public class GoogleManager : IGoogleManager
    {
        public Task Login(Func<SocialNetworkUser, string, Task> onLoginComplete, Func<AuthenticationState> OnLogoutRequested, string clientId, string authoriseUrl, string redirectUrl)
        {
            throw new NotImplementedException();
        }
        
        public AuthenticationState Logout()
        {
            throw new NotImplementedException();
        }
    }
}