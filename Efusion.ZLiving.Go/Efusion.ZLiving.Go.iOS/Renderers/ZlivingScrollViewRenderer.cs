﻿using Efusion.ZLiving.Go.iOS.Renderers;
using Efusion.ZLiving.Go.UI.Common.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ZLivingScrollView), typeof(ZlivingScrollViewRenderer))]
namespace Efusion.ZLiving.Go.iOS.Renderers
{
    public class ZlivingScrollViewRenderer: ScrollViewRenderer
    {
        public ZlivingScrollViewRenderer() : base()
        {

        }

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            var element = e.NewElement as ZLivingScrollView;
            element?.Render();
        }

    }
}