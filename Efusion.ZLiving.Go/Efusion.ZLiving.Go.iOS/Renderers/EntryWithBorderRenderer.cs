﻿using Efusion.ZLiving.Go.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Entry), typeof(EntryWithBorderRenderer))]
namespace Efusion.ZLiving.Go.iOS.Renderers
{
    public class EntryWithBorderRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {

            base.OnElementChanged(e);
            if (e.OldElement == null)
            {                
                Control.Layer.CornerRadius = 12.0f;
                Control.BackgroundColor = UIColor.Black;
                Control.Layer.BorderColor = UIColor.White.CGColor;
                Control.Layer.BorderWidth = 1;
            }
        }
    }
}