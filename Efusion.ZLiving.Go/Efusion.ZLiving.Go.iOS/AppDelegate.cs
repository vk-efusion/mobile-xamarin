﻿using System;
using System.Threading.Tasks;
using CarouselView.FormsPlugin.iOS;
using Efusion.ZLiving.Go.iOS.Managers;
using Efusion.ZLiving.Go.UI.Common.Contracts;
using Foundation;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using Prism;
using Prism.Ioc;
using UIKit;

namespace Efusion.ZLiving.Go.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {

            this.InitNavBarAndTabBarAppearance();
            global::Xamarin.Forms.Forms.Init();

            CarouselViewRenderer.Init();
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init();            
            LoadApplication(new App(new IOSInitializer()));

            AppDomain.CurrentDomain.UnhandledException += (sender, e) => { Crashes.TrackError(new Exception("ios-app-domain-unhandled-exception", (e.ExceptionObject is Exception) ? (Exception)e.ExceptionObject : new Exception(JsonConvert.SerializeObject(e.ExceptionObject)))); };
            TaskScheduler.UnobservedTaskException += (sender, e) => { Crashes.TrackError(new Exception("ios-unobserved-task-exception", e.Exception)); };

            return base.FinishedLaunching(app, options);
        }


        public class IOSInitializer : IPlatformInitializer
        {
            public void RegisterTypes(IContainerRegistry containerRegistry)
            {
                containerRegistry.RegisterSingleton<IFacebookManager, FacebookManager>();
                containerRegistry.RegisterSingleton<IGoogleManager, GoogleManager>();
            }
        }


        private void InitNavBarAndTabBarAppearance()
        {
            var logo = UIImage.FromFile("logo.png").CreateResizableImage(new UIEdgeInsets(0,-150,0, -150), UIImageResizingMode.Stretch);

            UINavigationBar.Appearance.BarTintColor = UIColor.Black;
            UINavigationBar.Appearance.SetBackgroundImage(logo, UIBarMetrics.Default);
            UINavigationBar.Appearance.BackgroundColor = UIColor.Black;
            UITextAttributes titleAttrs = new UITextAttributes
            {
                TextColor = UIColor.Clear
            };

            UINavigationBar.Appearance.SetTitleTextAttributes(titleAttrs);
        }
    }
}
