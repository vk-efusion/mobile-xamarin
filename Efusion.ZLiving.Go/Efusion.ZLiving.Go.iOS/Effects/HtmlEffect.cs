﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Efusion.ZLiving.Go.iOS.Effects;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("Efusion.ZLiving.Go.UI.Common.Effects")]
[assembly: ExportEffect(typeof(HtmlEffect), "HtmlEffect")]
namespace Efusion.ZLiving.Go.iOS.Effects
{
    public class HtmlEffect : PlatformEffect
    {
        
        protected override void OnAttached()
        {
            Label label = Element as Label;
            String text = label.Text ?? String.Empty;

            var attr = new NSAttributedStringDocumentAttributes();
            var nsError = new NSError();
            attr.DocumentType = NSDocumentType.HTML;

            var myHtmlData = NSData.FromString(text, NSStringEncoding.Unicode);

            if(this.Control is UILabel uiLabel) { 

                uiLabel.AttributedText = new NSAttributedString(myHtmlData, attr, ref nsError);
            }
        }

        protected override void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnElementPropertyChanged(args);
        //    if (args.PropertyName == "Text") {
                this.OnAttached();
          //  }
        }

        protected override void OnDetached()
        {
            
        }
    }
}