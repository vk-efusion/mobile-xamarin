﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreAnimation;
using CoreGraphics;
using Efusion.ZLiving.Go.iOS.Effects;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportEffect(typeof(TitleGradientBackgroundEffect), "TitleGradientBackgroundEffect")]
namespace Efusion.ZLiving.Go.iOS.Effects
{
    public class TitleGradientBackgroundEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            if (this.Control is UILabel control)
            {
                var gradientLayer = new CAGradientLayer
                {
                    Frame = control.Bounds,
                    Colors = new CGColor[] { Color.FromHex("F37330").ToCGColor(), Color.FromHex("FCB537").ToCGColor() },
                    //StartPoint = new CGPoint(0.0, 0.5),
                    //EndPoint = new CGPoint(1.0, 0.5)
                };

                control.BackgroundColor = UIColor.Clear;
                control.Layer.InsertSublayer(gradientLayer, 0);
            }
        }

        protected override void OnDetached()
        {
            
        }
    }
}