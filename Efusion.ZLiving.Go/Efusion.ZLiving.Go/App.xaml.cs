using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.Config;
using Efusion.ZLiving.Go.UI.Auth.Modules;
using Efusion.ZLiving.Go.UI.Content.Modules;
using Efusion.ZLiving.Go.UI.EPG.Modules;
using Efusion.ZLiving.Go.UI.Home.Modules;
using Efusion.ZLiving.Go.UI.VOD.Modules;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Prism;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Unity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Efusion.ZLiving.Go
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnStart ()
		{            
            
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>("navigation");            
            containerRegistry.RegisterSingleton<IGlobalDefaultSettings, GlobalDefaultSettings>();            
            containerRegistry.RegisterSingleton<IBaseSettings, Settings>();
        }        

        protected override void OnInitialized()
        {
            this.InitializeComponent();
            AppCenter.Start("android=fb8ec16e-6381-4e46-ac88-7bf8c1ad1e84;ios=f06caa5d-eb39-438b-b948-294b64566b47", typeof(Analytics), typeof(Crashes));
            AppCenter.LogLevel = LogLevel.Verbose;
            NavigationService.NavigateAsync("navigation/menu");
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            base.ConfigureModuleCatalog(moduleCatalog);
            moduleCatalog.AddModule<AuthenticationModule>(InitializationMode.WhenAvailable);
            moduleCatalog.AddModule<ContentModule>(InitializationMode.WhenAvailable);
            moduleCatalog.AddModule<HomeModule>(InitializationMode.WhenAvailable);
            moduleCatalog.AddModule<VODModule>(InitializationMode.WhenAvailable);
            moduleCatalog.AddModule<EPGModule>(InitializationMode.WhenAvailable);
        }
       
    }
}
