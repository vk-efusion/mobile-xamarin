﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using System.Collections.Generic;

namespace Efusion.ZLiving.Go.Config
{
    public class GlobalDefaultSettings: IGlobalDefaultSettings
    {
        public IDictionary<string, string> DefaultSettings { get; set; }

        public GlobalDefaultSettings() {
            this.DefaultSettings = new Dictionary<string, string>
            {
                { "zliving_base_endpoint", "http://zlivinggo-api.azurewebsites.net/" },
                { "zliving_stg_base_endpoint", "http://zlivinggo-api-stg.azurewebsites.net/" },
                { "zype_base_endpoint", "https://api.zype.com/" },
                { "zype_player_base_endpoint", "https://player.zype.com/" },
                { "login_base_endpoint", "https://login.zype.com/" },
                { "facebook_base_endpoint", "https://www.facebook.com/" },                
                { "android_app_key", "mdCfpl4RmuD0vgOs7g_Rb55FU9ABqVe2zsJX3Kae_VFrnSfyhYYU_-1h86KiN8Gs" },
                { "ios_app_key", "8oWgLQQUcRX3ZIsNBqMIfDlkdBWBBh-b7ljRJD_Ww7EFalWnPnd4FpVqS6UfrZ9P" },
                { "api_key", "BOV2F4UPXvAG7f-Zf1nr0iAMScGLyu7gVA58atMX9T8veeziWfdkYzynqsMDvCn2" },
                { "root_playlist_id", "5a5d71b272289b154601c695" },
                { "recommendations_endpoint", "recommendations" },
                { "link_device_endpoint", "pin/link" },
                { "acquire_link_device_pin_endpoint", "pin/acquire" },                
                { "playlists_endpoint", "playlists" },
                { "player_endpoint", "embed" },
                { "videos_endpoint", "videos" },
                { "zobject_endpoint", "zobjects" },
                { "login_endpoint", "oauth/token" },
                { "facebook_login_endpoint", "dialog/oauth" },
                { "registration_endpoint", "prospect/register" },                
                { "registration_status_endpoint", "prospect/status" },
                { "resend_activation_endpoint", "prospect/register/resend" },
                { "forgot_password_endpoint", "forgot-password" },
                { "change_password_endpoint", "change-password" },
                { "continue_watching", "continue-watching" },
                { "consumer_endpoint", "consumers" },
                { "app_center_droid_key", "fb8ec16e-6381-4e46-ac88-7bf8c1ad1e84" },
                { "app_center_ios_key", "f06caa5d-eb39-438b-b948-294b64566b47" },
                { "client_id", "7e50c4fc642d211fc4f949e64a96f27349e5c35e2499a02f0b08129b2c491592" },
                { "client_secret", "ed539e83e775b7f187be8f9fb498ce1f1f6be33fc7dcd11481ec38af3afec0fe" },
                { "facebook_client_id", "644934455688098" },
                { "facebook_authorise_url", "https://m.facebook.com/dialog/oauth/" },
                { "facebook_redirect_url", "https://www.facebook.com/connect/login_success.html" },
                { "google_client_id", "459131839547-l4fk2anc1frlgsb6gcdael50cfuk1qtk.apps.googleusercontent.com"},
                { "google_authorise_url", "https://accounts.google.com/o/oauth2/auth" },
                { "google_redirect_url", "com.googleusercontent.apps.l4fk2anc1frlgsb6gcdael50cfuk1qtk-459131839547:/oauth2redirect" },
                { "google_profile_url", "https://www.googleapis.com/oauth2/v4/token" },
                
            };
        }

        
    }
}
