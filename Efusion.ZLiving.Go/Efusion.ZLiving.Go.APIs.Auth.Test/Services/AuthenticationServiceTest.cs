using Efusion.ZLiving.Go.APIs.Auth.Exceptions;
using Efusion.ZLiving.Go.APIs.Auth.Services;
using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Efusion.ZLiving.Go.APIs.Auth.Test.Services
{
    public class AuthenticationServiceTest : IClassFixture<SettingsFixture>
    {
        public SettingsFixture Settings { get; set; }

        public AuthenticationServiceTest(SettingsFixture settings)
        {
            this.Settings = settings;
        }
        
        [Fact(DisplayName = "Registraton - Exception Handling Test")]
        public async void Register_ExceptionHandling_Test() {

            MockRestClient restClient = new MockRestClient();                        
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            Assert.Equal("authentication", (service as JsonBaseService).ServiceName);

            //Conflict(409) 
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("registration_endpoint"),HttpStatusCode.Conflict);
            Exception registrationException = await Record.ExceptionAsync(() => service.Register("name", "email", "password"));
            Assert.IsType<RegistrationException>(registrationException);
            Assert.Equal("Customer already exists",registrationException.Message);
            
            //Bad request should return a RegistrationException with content message.
            dynamic content = new ExpandoObject();
            content.message = "Received Error Message";            
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("registration_endpoint"), HttpStatusCode.BadRequest,content);
            registrationException = await Record.ExceptionAsync(() => service.Register("name", "email", "password"));
            Assert.IsType<RegistrationException>(registrationException);
            Assert.Equal("Received Error Message", registrationException.Message);

            //Any other status code will return the RestHttpException as it was thrown by the ExecuteHTTPRequestAsync method
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("registration_endpoint"), HttpStatusCode.LengthRequired);
            registrationException = await Record.ExceptionAsync(() => service.Register("name", "email", "password"));
            Assert.IsType<RestHTTPException>(registrationException);

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Registraton - Correct Settings Use Test")]
        public async void Register_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();         
            //Will validate if it is being call with the correct parameters

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email", "email@email.com",ParameterType.GetOrPost),
              ("name","FirstName LastName",ParameterType.GetOrPost),
              ("password","mypassword",ParameterType.GetOrPost),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            dynamic settingsOk = new ExpandoObject();
            settingsOk.message = "correct_parameters_used";
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zliving_base_endpoint"), Settings.GetSetting("registration_endpoint"), Method.GET, headers, parameters, Task.FromResult<dynamic>(settingsOk));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            dynamic response = await service.Register("FirstName LastName", "email@email.com", "mypassword");

            Assert.NotNull(response);
            Assert.Equal(Convert.ToString(settingsOk.message), Convert.ToString(response.message));

        }
   
        [Fact(DisplayName = "Resend Activation - Correct Settings Use Test")]
        public void ResendActivation_SettingsValidation_Test() {
            MockRestClient restClient = new MockRestClient();

            string email = "email@email.com";
            //Will validate if it is being call with the correct parameters
            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email", email,ParameterType.GetOrPost),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zliving_base_endpoint"), Settings.GetSetting("resend_activation_endpoint"), Method.POST, headers, parameters, Task.FromResult<dynamic>(null));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            service.ResendActivation(email);
            restClient.VerifyAll();

        }

        [Fact(DisplayName = "Login - Exception Handling Test")]
        public async void Login_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);

            //If zype login gives a 401(Unauthorized) status code, will have to verify the registration status. If it is pending
            //activation, login has to throw a PendingActivationException
            
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("login_endpoint"), HttpStatusCode.Unauthorized);            
            Exception loginException = await Record.ExceptionAsync(() => service.Login("email", "password"));
            Assert.IsType<UnauthorizedAccessException>(loginException);
                        
            //If zype login gives a 404 (Not Found) status code, Login should throw an UserNotFoundException

            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("login_endpoint"), HttpStatusCode.NotFound);
            loginException = await Record.ExceptionAsync(() => service.Login("email", "password"));
            Assert.IsType<UserNotFoundException>(loginException);

            //Any other status code will return the RestHttpException as it was thrown by the ExecuteHTTPRequestAsync method
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("login_endpoint"), HttpStatusCode.LengthRequired);
            loginException = await Record.ExceptionAsync(() => service.Login("email", "password"));
            Assert.IsType<RestHTTPException>(loginException);
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Login - Correct Settings Use Test")]
        public async void Login_SettingsValidation_Test() {
            MockRestClient restClient = new MockRestClient();

            //Will validate if it is being call with the correct parameters

            string email = "mail@mail.com";
            string password = "mypassword";

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("client_id",Settings.GetSetting("client_id"),ParameterType.GetOrPost),
              ("client_secret",Settings.GetSetting("client_secret"),ParameterType.GetOrPost),
              ("username",email,ParameterType.GetOrPost),
              ("password",password,ParameterType.GetOrPost),
              ("grant_type","password",ParameterType.GetOrPost),
            };

            dynamic settingsOk = new ExpandoObject();
            settingsOk.message = "correct_parameters_used";
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("login_base_endpoint"), Settings.GetSetting("login_endpoint"), Method.POST, headers, parameters, Task.FromResult<dynamic>(settingsOk));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            dynamic response = await service.Login(email,password);
            restClient.VerifyAll();
            Assert.NotNull(response);
            Assert.Equal(Convert.ToString(settingsOk.message), Convert.ToString(response.message));

        }

        [Fact(DisplayName = "Change Password - Exception Handling Test")]
        public async void ChangePassword_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            MockIRestResponse restResponse = new MockIRestResponse();

            //If call to the change_password_endpoint returns a status code 400, should throw an InvalidPasswordException with the error message
            dynamic content = new ExpandoObject();
            content.message = "InvalidPassword";
            RestHTTPException exception = new RestHTTPException(restResponse.Object);
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("change_password_endpoint"), HttpStatusCode.BadRequest, content);
            Exception ex = await Record.ExceptionAsync(() => service.ChangePassword("accesstoken","password"));
            Assert.IsType<InvalidPasswordException>(ex);
            Assert.Equal(Convert.ToString(content.message) ,ex.Message);

            //If call to the change_password_endpoint returns a status code 401, should throw an UnauthorizedAccessException with the error message            
            content.message = "Not authorized";            
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("change_password_endpoint"), HttpStatusCode.Unauthorized, content);
            ex = await Record.ExceptionAsync(() => service.ChangePassword("accesstoken", "password"));
            Assert.IsType<UnauthorizedAccessException>(ex);
            Assert.Equal(Convert.ToString(content.message), ex.Message);

            //If call to the change_password_endpoint returns a any other status than 400 or 401, should throw the RestHTTPException.
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("change_password_endpoint"), HttpStatusCode.ExpectationFailed);
            ex = await Record.ExceptionAsync(() => service.ChangePassword("accesstoken", "password"));
            Assert.IsType<RestHTTPException>(ex);
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Change Password - Correct Settings Use Test")]
        public void ChangePassword_SettingsValidation_Test() {
            MockRestClient restClient = new MockRestClient();
            string password = "mypassword";
            string accessToken = "accessToken";

            //Will validate if it is being call with the correct parameters

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("access_token",accessToken,ParameterType.GetOrPost),
              ("password",password,ParameterType.GetOrPost)
            };

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zliving_stg_base_endpoint"), Settings.GetSetting("change_password_endpoint"), Method.GET, headers, parameters, Task.FromResult<dynamic>(null));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            service.ChangePassword(accessToken,password);
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Registration Status - Exception Handling Test")]
        public async void GetRegistrationStatus_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);            
            //If call to the change_password_endpoint returns a status code 404, should throw an UserNotFoundException with the error message                        
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("registration_status_endpoint"), HttpStatusCode.NotFound);
            Exception ex = await Record.ExceptionAsync(() => service.GetRegistrationStatus("mail"));
            Assert.IsType<UserNotFoundException>(ex);

            //If call to the change_password_endpoint returns a any other status than 404, should throw the RestHTTPException.
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("registration_status_endpoint"), HttpStatusCode.ExpectationFailed);
            ex = await Record.ExceptionAsync(() => service.GetRegistrationStatus("mail"));
            Assert.IsType<RestHTTPException>(ex);
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Registration Status - Correct Settings Use Test")]
        public void GetRegistrationStatus_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            string email = "email@email.com";
            //Will validate if it is being call with the correct parameters

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email", email,ParameterType.QueryString),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zliving_base_endpoint"), Settings.GetSetting("registration_status_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(null));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            service.GetRegistrationStatus(email);
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Send Forgotten Password - Exception Handling Test")]
        public async void SendForgottenPassword_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("forgot_password_endpoint"), HttpStatusCode.NotFound);
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            Exception ex = await Record.ExceptionAsync(() => service.SendForgottenPassword("email@email.com"));
            Assert.IsType<UserNotFoundException>(ex);
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Send Forgotten Password - Correct Settings Use Test")]
        public void SendForgottenPassword_SettingsValidation_Test() {
            MockRestClient restClient = new MockRestClient();

            //Will validate if it is being call with the correct parameters

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email", "email@email.com",ParameterType.GetOrPost),              
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zliving_stg_base_endpoint"), Settings.GetSetting("forgot_password_endpoint"), Method.POST, headers, parameters, Task.FromResult<dynamic>(null));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            service.SendForgottenPassword("email@email.com");
            restClient.VerifyAll();            
        }
        
        [Fact(DisplayName = "Link Device - Correct Settings Use Test")]
        public async void LinkDevice_SettingsValidation_Test() {
            MockRestClient restClient = new MockRestClient();
            string devicePIN = "pin";

            Settings.SetDefaultSetting("consumer_id", String.Empty);

            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            //Customer Id == null, so we have to receive an UserNotFoundException
            Exception exception = await Record.ExceptionAsync(() => service.LinkDevice(devicePIN));
            Assert.IsType<UserNotFoundException>(exception);

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            Settings.SetDefaultSetting("consumer_id", "id-value");

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("pin",devicePIN,ParameterType.QueryString),
              ("consumer_id",Settings.GetSetting("consumer_id"),ParameterType.QueryString),
              (service as AuthenticationService).GetAuthenticationParameter()
            };                        
            
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("link_device_endpoint"), Method.PUT, headers, parameters, Task.FromResult<dynamic>(null));
            await service.LinkDevice(devicePIN);
            restClient.VerifyAll();

            Settings.SetDefaultSetting("consumer_id", String.Empty);
        }

        [Fact(DisplayName = "Link Device - Exception Handling Test")]
        public async void LinkDevice_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);            

            //If consumer ID is Empty, should throw an UserNotFoundException
            Settings.SetSetting("consumer_id", String.Empty);                       
            Exception ex = await Record.ExceptionAsync(() => service.LinkDevice("pin"));
            Assert.IsType<UserNotFoundException>(ex);

            //If Consumer ID has value but the call to the link_device_endpoint returns a status code 404, should throw an UserNotFoundException
            Settings.SetSetting("consumer_id", "jkladjkladjaklskdjasl");
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("link_device_endpoint"), HttpStatusCode.NotFound);
            ex = await Record.ExceptionAsync(() => service.LinkDevice("pin"));
            Assert.IsType<UserNotFoundException>(ex);

            //Any other status code should throw the RestHTTPException
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("link_device_endpoint"), HttpStatusCode.NotImplemented);
            ex = await Record.ExceptionAsync(() => service.LinkDevice("pin"));
            Assert.IsType<RestHTTPException>(ex);
            restClient.VerifyAll();

            Settings.SetSetting("consumer_id", String.Empty);
        }
    
        [Fact(DisplayName ="Get Consumer - Correct Settings Use Test")]
        public async void GetConsumer_SettingsValidation_Test() {
            MockRestClient restClient = new MockRestClient();
            //Will validate if it is being call with the correct parameters            
            var parameters = new List<(string, string, ParameterType)>()
            {
              ("email", "email@email.com",ParameterType.QueryString),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };

            dynamic customer = new ExpandoObject();
            customer.name = "settingsok";
            dynamic settingsOk = new ExpandoObject();

            settingsOk.pagination =  new ExpandoObject();
            settingsOk.response = new List<ExpandoObject>() { customer };
            settingsOk.pagination.pages = 1;

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("consumer_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(settingsOk));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            dynamic customerResponse = await service.GetConsumer("email@email.com");
            restClient.VerifyAll();

            //If there is not any consumer with that email, will throw an UserNotFoundException
            settingsOk.pagination.pages = 0;
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("consumer_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(settingsOk));

            Exception exception = await Record.ExceptionAsync(() => service.GetConsumer("email@email.com"));
            Assert.IsType<UserNotFoundException>(exception);
        }


        [Fact(DisplayName = "Login Device- Exception Handling Test")]
        public async void LoginDevice_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);

            //If zype login gives a 401(Unauthorized) status code, will have to verify the registration status. If it is pending
            //activation, login has to throw a PendingActivationException

            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("login_endpoint"), HttpStatusCode.Unauthorized);
            Exception loginException = await Record.ExceptionAsync(() => service.LoginDevice("deviceId", "pin"));
            Assert.IsType<UnauthorizedAccessException>(loginException);

            //If zype login gives a 404 (Not Found) status code, Login should throw an UserNotFoundException

            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("login_endpoint"), HttpStatusCode.NotFound);
            loginException = await Record.ExceptionAsync(() => service.LoginDevice("deviceId", "pin"));
            Assert.IsType<UserNotFoundException>(loginException);

            //Any other status code will return the RestHttpException as it was thrown by the ExecuteHTTPRequestAsync method
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("login_endpoint"), HttpStatusCode.LengthRequired);
            loginException = await Record.ExceptionAsync(() => service.LoginDevice("deviceId", "pin"));
            Assert.IsType<RestHTTPException>(loginException);
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Login Device - Correct Settings Use Test")]
        public async void LoginDevice_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();

            //Will validate if it is being call with the correct parameters

            string linkedDeviceId = "facebook";
            string pin = "123456ww";

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("client_id",Settings.GetSetting("client_id"),ParameterType.GetOrPost),
              ("client_secret",Settings.GetSetting("client_secret"),ParameterType.GetOrPost),
              ("linked_device_id",linkedDeviceId,ParameterType.GetOrPost),
              ("pin",pin,ParameterType.GetOrPost),
              ("grant_type","password",ParameterType.GetOrPost),
            };

            dynamic settingsOk = new ExpandoObject();
            settingsOk.message = "correct_parameters_used";
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("login_base_endpoint"), Settings.GetSetting("login_endpoint"), Method.POST, headers, parameters, Task.FromResult<dynamic>(settingsOk));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            dynamic response = await service.LoginDevice(linkedDeviceId, pin);
            restClient.VerifyAll();
            Assert.NotNull(response);
            Assert.Equal(Convert.ToString(settingsOk.message), Convert.ToString(response.message));

        }


        [Fact(DisplayName = "Acquire Device Pin - Exception Handling Test")]
        public async void AcquireDevicePin_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);

            //If zype login gives a 401(Unauthorized) status code, will throw an UnauthorizedAccessException exception

            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("acquire_link_device_pin_endpoint"), HttpStatusCode.Unauthorized);
            Exception loginException = await Record.ExceptionAsync(() => service.AcquireDevicePin("deviceId"));
            Assert.IsType<UnauthorizedAccessException>(loginException);

            //If zype login gives a 404 (Not Found) status code,  AcquireDevicePin should throw an UserNotFoundException

            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("acquire_link_device_pin_endpoint"), HttpStatusCode.NotFound);
            loginException = await Record.ExceptionAsync(() => service.AcquireDevicePin("deviceId"));
            Assert.IsType<UserNotFoundException>(loginException);

            //Any other status code will return the RestHttpException as it was thrown by the ExecuteHTTPRequestAsync method
            restClient.MockExecuteHTTPRequestAsyncException(Settings.GetSetting("acquire_link_device_pin_endpoint"), HttpStatusCode.LengthRequired);
            loginException = await Record.ExceptionAsync(() => service.AcquireDevicePin("deviceId"));
            Assert.IsType<RestHTTPException>(loginException);
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "Acquire Device Pin - Correct Settings Use Test")]
        public async void AcquireDevicePin_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();

            //Will validate if it is being call with the correct parameters

            string linkedDeviceId = "facebook";            

            var headers = new List<(string, string)>() {
                ("Content-Type","application/x-www-form-urlencoded")
            };

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("linked_device_id",linkedDeviceId,ParameterType.GetOrPost),
              ("api_key",Settings.GetSetting("api_key"),ParameterType.QueryString)
            };


            dynamic settingsOk = new ExpandoObject();
            settingsOk.response = "correct_parameters_used";
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("acquire_link_device_pin_endpoint"), Method.POST, headers, parameters, Task.FromResult<dynamic>(settingsOk));
            IAuthenticationService service = new AuthenticationService(restClient.Object, Settings);
            dynamic response = await service.AcquireDevicePin(linkedDeviceId);
            restClient.VerifyAll();
            Assert.NotNull(response);
            Assert.Equal(Convert.ToString(settingsOk.response), Convert.ToString(Convert.ToString(response)));

        }

    }
}
