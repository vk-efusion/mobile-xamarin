﻿using Efusion.ZLiving.Go.APIs.Auth.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Auth.Test.Mocks
{
    public class MockAuthenticationService : Mock<IAuthenticationService>
    {
        public void MockLogin(string email,string password)
        {
            dynamic result = new ExpandoObject();
            Setup(x => x.Login(
            It.Is<string>(i => i == email),
            It.Is<string>(i => i == password)           
            )).Returns(Task.FromResult(result));
        }

        public void MockSendForgotPassword(string email)
        {
            dynamic result = new ExpandoObject();
            Setup(x => x.SendForgottenPassword(
            It.Is<string>(i => i == email)            
            )).Returns(Task.CompletedTask);
        }

        public void MockSendForgotPassword(string email, Exception outputEx)
        {
            dynamic result = new ExpandoObject();
            Setup(x => x.SendForgottenPassword(
            It.Is<string>(i => i == email)
            )).Throws(outputEx);
        }

    }
}
