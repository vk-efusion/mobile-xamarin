using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Services;
using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.APIs.VOD.Exceptions;
using Efusion.ZLiving.Go.APIs.VOD.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Efusion.ZLiving.Go.APIs.VOD.Test.Services
{
    public class VideoOnDemandServiceTest : IClassFixture<SettingsFixture>
    {
        public SettingsFixture Settings { get; set; }

        public VideoOnDemandServiceTest(SettingsFixture settings)
        {
            this.Settings = settings;
        }

        [Fact(DisplayName = "GetPlayer - Correct Settings Use Test")]
        public async void GetPlayer_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            var parameters = new List<(string, string, ParameterType)>()
             {
                     (service as VideoOnDemandService).GetAuthenticationParameter()
              };

            var videoId = "video-id";
            dynamic httpResponse = new ExpandoObject();
            httpResponse.response = new ExpandoObject();
            httpResponse.response.body = new ExpandoObject();
            httpResponse.response.body.outputs = new List<ExpandoObject>() { new ExpandoObject() };

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{Settings.GetSetting("player_endpoint")}/{videoId}.json", Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            
            Assert.Equal("video-on-demand", (service as JsonBaseService).ServiceName);

            dynamic serviceResponse = await service.GetPlayer(videoId);
            Assert.NotNull(serviceResponse);

            //If returns an empty list of players, will receive a Resource Not Found Exception
            httpResponse.response.body.outputs = new List<ExpandoObject>();
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{Settings.GetSetting("player_endpoint")}/{videoId}.json", Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            var ex = await Record.ExceptionAsync(() => service.GetPlayer("video-id"));
            Assert.IsType<ResourceNotFoundException>(ex);
            Assert.Equal($"Player Not Found for Video: {videoId}", ex.Message);

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetPlayer - Exception Handling Test")]
        public async void GetPlayer_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "content_rule_blackout";

            //Forbidden(433) 
            var endPoint = $"{Settings.GetSetting("player_endpoint")}/video-id.json";
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.Forbidden, Task.FromResult<dynamic>(response));
            Exception ex = await Record.ExceptionAsync(() => service.GetPlayer("video-id"));
            Assert.IsType<PlayerNotAllowedException>(ex);
            //   Assert.Contains("Cannot play video due to a content rule blackout",ex.InnerException.Message);

            response.message = "Any other message";
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.Forbidden, Task.FromResult<dynamic>(response));
            ex = await Record.ExceptionAsync(() => service.GetPlayer("video-id"));
            Assert.IsType<PlayerNotAllowedException>(ex);
            // Assert.Equal(Convert.ToString(response.message), ex.Message);

            //Null and empty video. We should receive an ArgumentNullException
            ex = await Record.ExceptionAsync(() => service.GetPlayer(String.Empty));
            Assert.IsType<ArgumentNullException>(ex);

            ex = await Record.ExceptionAsync(() => service.GetPlayer(null));
            Assert.IsType<ArgumentNullException>(ex);

            //Any other Status Code, should throw an httpRestException,
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.NotFound, Task.FromResult<dynamic>(response));
            ex = await Record.ExceptionAsync(() => service.GetPlayer("video-id"));
            Assert.IsType<RestHTTPException>(ex);

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetVideo - Correct Settings Use Test")]
        public async void GetVideo_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);

            var parametersPlayer = new List<(string, string, ParameterType)>()
            {
               (service as VideoOnDemandService).GetAuthenticationParameter()
            };


            var parameters = new List<(string, string, ParameterType)>()
            {
              ("active","true",ParameterType.QueryString),
               (service as VideoOnDemandService).GetAuthenticationParameter()
            };

            var videoId = "video-id";
            dynamic httpPlayerResponse = new ExpandoObject();
            httpPlayerResponse.response = new ExpandoObject();
            httpPlayerResponse.response.body = new ExpandoObject();
            httpPlayerResponse.response.body.outputs = new List<ExpandoObject>() { new ExpandoObject() };


            dynamic httpResponse = new ExpandoObject();
            httpResponse.response = new ExpandoObject();
            httpResponse.response.message = "ok";

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{Settings.GetSetting("player_endpoint")}/{videoId}.json", Method.GET, null, parametersPlayer, Task.FromResult<dynamic>(httpPlayerResponse));

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{Settings.GetSetting("videos_endpoint")}/{videoId}", Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            

            dynamic serviceResponse = await service.GetVideo(videoId);
            Assert.NotNull(serviceResponse);
            Assert.NotNull(serviceResponse.player);
            Assert.Equal("ok", Convert.ToString(serviceResponse.message));

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetVideo -  Exception Handling Test")]
        public async void GetVideo_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "exception";

            // For any status code should throw an RestHTTPException
            var endPoint = $"{Settings.GetSetting("videos_endpoint")}/video-id";
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.Forbidden, Task.FromResult<dynamic>(response));
            Exception ex = await Record.ExceptionAsync(() => service.GetVideo("video-id"));
            Assert.IsType<RestHTTPException>(ex);

            //Null and empty video. We should receive an ArgumentNullException
            ex = await Record.ExceptionAsync(() => service.GetVideo(String.Empty));
            Assert.IsType<ArgumentNullException>(ex);

            ex = await Record.ExceptionAsync(() => service.GetVideo(null));
            Assert.IsType<ArgumentNullException>(ex);
        }


        [Fact(DisplayName = "GetVideos - Correct Settings Use Test")]
        public async void GetVideos_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("per_page","20",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              (service as VideoOnDemandService).GetAuthenticationParameter()
            };

            var playListId = "playlist-id";
            dynamic httpResponse = new ExpandoObject();
            httpResponse.response = new ExpandoObject();
            httpResponse.response.message = "ok";

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), $"{Settings.GetSetting("playlists_endpoint")}/{playListId}/videos", Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            

            dynamic serviceResponse = await service.GetVideos(playListId);
            Assert.NotNull(serviceResponse);
            Assert.Equal("ok", Convert.ToString(serviceResponse.message));

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetVideos -  Exception Handling Test")]
        public async void GetVideos_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "exception";

            // For any status code should throw an RestHTTPException
            var endPoint = $"{Settings.GetSetting("playlists_endpoint")}/playlist-id/videos";
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.Forbidden, Task.FromResult<dynamic>(response));
            Exception ex = await Record.ExceptionAsync(() => service.GetVideos("playlist-id"));
            Assert.IsType<RestHTTPException>(ex);

            //Null and empty video. We should receive an ArgumentNullException
            ex = await Record.ExceptionAsync(() => service.GetVideos(String.Empty));
            Assert.IsType<ArgumentNullException>(ex);

            ex = await Record.ExceptionAsync(() => service.GetVideos(null));
            Assert.IsType<ArgumentNullException>(ex);
        }

        [Fact(DisplayName = "GetPlaylistsAsync - Correct Settings Use Test")]
        public async void GetPlaylistsAsync_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);

            string parentId = "parent-id";

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("parent_id",parentId,ParameterType.QueryString),
              ("sort","priority",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              ("per_page","20",ParameterType.QueryString),
              (service as VideoOnDemandService).GetAuthenticationParameter()
            };
            
            dynamic httpResponse = new ExpandoObject();
            httpResponse.response = new ExpandoObject();
            httpResponse.response.message = "ok";

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            
            dynamic serviceResponse = await service.GetPlaylistsAsync(parentId);
            Assert.NotNull(serviceResponse);
            Assert.Equal("ok", Convert.ToString(serviceResponse.message));

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetPlaylistsAsync -  Exception Handling Test")]
        public async void GetPlaylistsAsync_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "exception";
            
            // For any status code should throw an RestHTTPException
            var endPoint = Settings.GetSetting("playlists_endpoint");
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.Forbidden, Task.FromResult<dynamic>(response));
            Exception ex = await Record.ExceptionAsync(() => service.GetPlaylistsAsync("parent-id"));
            Assert.IsType<RestHTTPException>(ex);

            //Null and empty video. We should receive an ArgumentNullException
            ex = await Record.ExceptionAsync(() => service.GetPlaylistsAsync(String.Empty));
            Assert.IsType<ArgumentNullException>(ex);

            ex = await Record.ExceptionAsync(() => service.GetPlaylistsAsync(null));
            Assert.IsType<ArgumentNullException>(ex);

        }

        [Fact(DisplayName = "GetPlaylists - Correct Settings Use Test")]
        public void GetPlaylists_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);

            string parentId = "parent-id";

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("parent_id",parentId,ParameterType.QueryString),
              ("sort","priority",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              ("per_page","500",ParameterType.QueryString),
              (service as VideoOnDemandService).GetAuthenticationParameter()
            };

            dynamic httpResponse = new ExpandoObject();
            httpResponse.response = new ExpandoObject();
            httpResponse.response.message = "ok";

            restClient.MockExecuteHTTPRequest(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"), Method.GET, null, parameters, httpResponse);
            
            dynamic serviceResponse = service.GetPlaylists(parentId);
            Assert.NotNull(serviceResponse);
            Assert.Equal("ok", Convert.ToString(serviceResponse.message));

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetPlaylists -  Exception Handling Test")]
        public void GetPlaylists_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "exception";

            // For any status code should throw an RestHTTPException
            var endPoint = Settings.GetSetting("playlists_endpoint");
            restClient.MockExecuteHTTPRequestException(endPoint, HttpStatusCode.Forbidden, response);
            Exception ex = Record.Exception(() => service.GetPlaylists("parent-id"));
            Assert.IsType<RestHTTPException>(ex);

            //Null and empty video. We should receive an ArgumentNullException
            ex = Record.Exception(() => service.GetPlaylists(String.Empty));
            Assert.IsType<ArgumentNullException>(ex);

            ex = Record.Exception(() => service.GetPlaylists(null));
            Assert.IsType<ArgumentNullException>(ex);
        }

        [Fact(DisplayName = "GetRootPlaylistsAsync - Correct Settings Use Test")]
        public async void GetRootPlaylistsAsync_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("parent_id",Settings.GetSetting("root_playlist_id"),ParameterType.QueryString),
              ("sort","priority",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              ("per_page","20",ParameterType.QueryString),
              (service as VideoOnDemandService).GetAuthenticationParameter()
            };

            dynamic httpResponse = new ExpandoObject();
            httpResponse.response = new ExpandoObject();
            httpResponse.response.message = "ok";

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            
            dynamic serviceResponse = await service.GetRootPlaylistsAsync();
            Assert.NotNull(serviceResponse);
            Assert.Equal("ok", Convert.ToString(serviceResponse.message));

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetRootPlaylistsAsync -  Exception Handling Test")]
        public async void GetRootPlaylistsAsync_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "exception";

            // For any status code should throw an RestHTTPException
            var endPoint = Settings.GetSetting("playlists_endpoint");
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.Forbidden, Task.FromResult<dynamic>(response));
            Exception ex = await Record.ExceptionAsync(() => service.GetRootPlaylistsAsync());
            Assert.IsType<RestHTTPException>(ex);
        }

        [Fact(DisplayName = "GetRootPlaylists - Correct Settings Use Test")]
        public void GetRootPlaylists_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("parent_id",Settings.GetSetting("root_playlist_id"),ParameterType.QueryString),
              ("sort","priority",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              ("per_page","500",ParameterType.QueryString),
              (service as VideoOnDemandService).GetAuthenticationParameter()
            };

            dynamic httpResponse = new ExpandoObject();
            httpResponse.response = new ExpandoObject();
            httpResponse.response.message = "ok";

            restClient.MockExecuteHTTPRequest(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"), Method.GET, null, parameters, httpResponse);
            
            dynamic serviceResponse = service.GetRootPlaylists();
            Assert.NotNull(serviceResponse);
            Assert.Equal("ok", Convert.ToString(serviceResponse.message));

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetRootPlaylists -  Exception Handling Test")]
        public void GetRootPlaylists_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "exception";

            // For any status code should throw an RestHTTPException
            var endPoint = Settings.GetSetting("playlists_endpoint");
            restClient.MockExecuteHTTPRequestException(endPoint, HttpStatusCode.Forbidden, response);
            Exception ex = Record.Exception(() => service.GetRootPlaylists());
            Assert.IsType<RestHTTPException>(ex);
        }


        [Fact(DisplayName = "GetShow - Correct Settings Use Test")]
        public async void GetShow_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);

            var showId = "show-id";

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("id",showId,ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              (service as VideoOnDemandService).GetAuthenticationParameter()
            };
            
            dynamic httpResponse = new ExpandoObject();
            dynamic show = new ExpandoObject();
            show.Id = "show-id";
            httpResponse.response = new List<ExpandoObject>() { show };
                        
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"),  Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            

            dynamic serviceResponse = await service.GetShow(showId);
            Assert.NotNull(serviceResponse);
            Assert.Equal("show-id", Convert.ToString(serviceResponse.Id));

            //If returns an empty list of players, will receive a Resource Not Found Exception
            httpResponse.response = new List<ExpandoObject>();
            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("playlists_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            var ex = await Record.ExceptionAsync(() => service.GetShow(showId));
            Assert.IsType<ShowNotFoundException>(ex);
            Assert.Equal($"Show with ID: {showId} was not found", ex.Message);

            restClient.VerifyAll();
        }

        [Fact(DisplayName = "GetShow -  Exception Handling Test")]
        public async void GetShow_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "exception";

            // For any status code should throw an RestHTTPException
            var endPoint = Settings.GetSetting("playlists_endpoint");
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.Forbidden, Task.FromResult<dynamic>(response));
            Exception ex = await Record.ExceptionAsync(() => service.GetShow("show-id"));
            Assert.IsType<RestHTTPException>(ex);

            //Null and empty video. We should receive an ArgumentNullException
            ex = await Record.ExceptionAsync(() => service.GetShow(String.Empty));
            Assert.IsType<ArgumentNullException>(ex);

            ex = await Record.ExceptionAsync(() => service.GetShow(null));
            Assert.IsType<ArgumentNullException>(ex);
        }

        [Fact(DisplayName = "SearchVideos - Correct Settings Use Test")]
        public async void SearchVideos_SettingsValidation_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);

            var query = "query";

            var parameters = new List<(string, string, ParameterType)>()
            {
              ("per_page","30",ParameterType.QueryString),
              ("active","true",ParameterType.QueryString),
              ("q",query,ParameterType.QueryString),
              ("category[Video Type]","episode",ParameterType.QueryString),
              ("category![Plan]","free",ParameterType.QueryString),
              (service as VideoOnDemandService).GetAuthenticationParameter()
            };

            dynamic httpResponse = new ExpandoObject();
            dynamic videoSearch = new ExpandoObject();
            videoSearch.Id = "video-id";
            httpResponse.response = new List<ExpandoObject>() { videoSearch };

            restClient.MockExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("videos_endpoint"), Method.GET, null, parameters, Task.FromResult<dynamic>(httpResponse));
            

            dynamic serviceResponse = await service.SearchVideos(query);
            Assert.NotNull(serviceResponse);
            Assert.NotEqual(0,Convert.ToInt32(serviceResponse.Count));
            Assert.Equal("video-id", Convert.ToString(serviceResponse[0].Id));
            
            restClient.VerifyAll();
        }

        [Fact(DisplayName = "SearchVideos -  Exception Handling Test")]
        public async void SearchVideos_ExceptionHandling_Test()
        {
            MockRestClient restClient = new MockRestClient();
            IVideoOnDemandService service = new VideoOnDemandService(restClient.Object, Settings);
            dynamic response = new ExpandoObject();
            response.message = "exception";

            // For any status code should throw an RestHTTPException
            var endPoint = Settings.GetSetting("videos_endpoint");
            restClient.MockExecuteHTTPRequestAsyncException(endPoint, HttpStatusCode.Forbidden, Task.FromResult<dynamic>(response));
            Exception ex = await Record.ExceptionAsync(() => service.SearchVideos("query"));
            Assert.IsType<RestHTTPException>(ex);            
        }

    }
}
