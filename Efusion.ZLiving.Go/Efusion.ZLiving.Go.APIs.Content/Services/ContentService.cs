﻿using Efusion.ZLiving.Go.APIs.Common.Config;
using Efusion.ZLiving.Go.APIs.Common.Exceptions;
using Efusion.ZLiving.Go.APIs.Common.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Content.Services
{
    public class ContentService : ZLivingGoBaseService, IContentService
    {
        public override string ServiceName => "content";

        public ContentService(Common.Services.IRestClient jsonClient, IBaseSettings settings) : base(jsonClient, settings) {

        }
             
        public async Task<dynamic> GetBannerContent()
        {
            var parameters = new List<(string, string, ParameterType)>()
                        {
                           ("zobject_type","slider",ParameterType.QueryString),
                           this.GetAuthenticationParameter()
                        };

            dynamic httpResponse = null;

            try
            {
                httpResponse = await this.Client.ExecuteHTTPRequestAsync(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("zobject_endpoint"), Method.GET, null, parameters);
            }
            catch (RestHTTPException restException) {
                this.TrackError(restException, "get-banner-content", restException.Response.StatusCode.ToString());
                throw restException;
            }

            if (Convert.ToInt32(httpResponse.response.Count) == 0) {
                var ex = new ResourceNotFoundException("Banner Content Not Found");
                this.TrackError(ex, "get-banner-content", "Resource Not Found","zobject-type=slider");
                throw ex;
            }

            return httpResponse.response;
        }
      
        public dynamic GetContent(string contentKey)
        {
            var parameters = new List<(string, string, ParameterType)>()
            {
                ("zobject_type","content_pages",ParameterType.QueryString),
                this.GetAuthenticationParameter()
            };

            dynamic httpResponse = null;

            try { 
                httpResponse = this.Client.ExecuteHTTPRequest(Settings.GetSetting("zype_base_endpoint"), Settings.GetSetting("zobject_endpoint"),Method.GET, null, parameters);
             }
            catch (RestHTTPException restException) {
                this.TrackError(restException, "get-content", restException.Response.StatusCode.ToString(), $"zobject_type=content&content_key={contentKey}");
                throw restException;
            }

            if (Convert.ToInt32(httpResponse.response.Count) == 0) {
                var ex = new ResourceNotFoundException("Content Not Found");
                this.TrackError(ex, "get-content", "Resource Not Found", $"zobject_type=content&content_key={contentKey}");
                throw ex;
            }

            return this.GetContent(httpResponse, contentKey);
        }


        private dynamic GetContent(dynamic fullContent, string pageName)
        {
            dynamic specificContent = new ExpandoObject();

            foreach (dynamic content in fullContent.response)
            {
                if (Convert.ToString(content.page_name).Equals(pageName))
                {
                    specificContent.Content = content.text;
                    specificContent.Title = content.title;
                }
            }

            return specificContent;
        }

    }
}
