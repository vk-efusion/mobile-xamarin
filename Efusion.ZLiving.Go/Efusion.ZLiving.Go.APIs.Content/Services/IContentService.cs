﻿using System.Threading.Tasks;

namespace Efusion.ZLiving.Go.APIs.Content.Services
{
    public interface IContentService
    {
        dynamic GetContent(string contentKey);        
        Task<dynamic> GetBannerContent();
    }
}
