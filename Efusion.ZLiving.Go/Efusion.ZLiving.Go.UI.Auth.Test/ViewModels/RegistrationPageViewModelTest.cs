﻿using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.Auth.ViewModels;
using Efusion.ZLiving.Go.UI.Common.Test.Mocks;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Efusion.ZLiving.Go.UI.Auth.Test.ViewModels
{
    public class RegistrationPageViewModelTest : IClassFixture<SettingsFixture>
    {
        public SettingsFixture Settings { get; set; }

        public RegistrationPageViewModelTest(SettingsFixture settings)
        {
            this.Settings = settings;
        }

        [Fact(DisplayName = "Login - Navigate to Login Page Test")]
        public void Register_Navigation_Test()
        {
            MockINavigationService navServiceMock = new MockINavigationService();
            MockSessionManager smMock = new MockSessionManager();
            RegistrationPageViewModel viewModel = new RegistrationPageViewModel(null, null, navServiceMock.Object, smMock.Object, Settings);
            navServiceMock.MockNavigateAsync("login");
            Assert.NotNull(viewModel.LoginCommand);
            
            viewModel.Login();
            navServiceMock.VerifyAll();
        }

    }
}
