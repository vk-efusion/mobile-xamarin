﻿using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.Auth.States;
using Efusion.ZLiving.Go.UI.Auth.ViewModels;
using Efusion.ZLiving.Go.UI.Common.Test.Mocks;
using System;
using Xunit;

namespace Efusion.ZLiving.Go.UI.Auth.Test.ViewModels
{
    public class LinkDevicePageViewModelTest : IClassFixture<SettingsFixture>
    {        
        public SettingsFixture Settings { get; set; }

        public LinkDevicePageViewModelTest(SettingsFixture settings)
        {
            this.Settings = settings;
        }


        [Fact(DisplayName = "Link Device - Page State Test")]
        public void LinkDevice_Page_State_Test()
        {
            string pin = "123124r";
            MockSessionManager smMock = new MockSessionManager();
            smMock.MockLinkDevice(pin);

            LinkDevicePageViewModel viewModel = new LinkDevicePageViewModel(null,smMock.Object, null, Settings)
            {
                DevicePIN = pin
            };

            Assert.NotNull(viewModel.LinkDeviceCommand);
            Assert.Equal("link-device-page-vm", viewModel.ViewModelName);
            Assert.Equal(LinkDeviceState.NotLinked, viewModel.LinkDeviceState);
            viewModel.LinkDevice();
            Assert.Equal(pin, viewModel.DevicePIN);
            Assert.Equal(LinkDeviceState.Linked, viewModel.LinkDeviceState);

            smMock.MockLinkDeviceException(pin, new UnauthorizedAccessException());
            viewModel.LinkDevice();

            Assert.Equal(pin, viewModel.DevicePIN);
            Assert.Equal(LinkDeviceState.LinkError, viewModel.LinkDeviceState);

            smMock.VerifyAll();
        }
    }
}
