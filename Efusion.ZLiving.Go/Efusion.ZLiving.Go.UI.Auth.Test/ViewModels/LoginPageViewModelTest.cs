using Efusion.ZLiving.Go.APIs.Auth.Exceptions;
using Efusion.ZLiving.Go.APIs.Auth.Test.Mocks;
using Efusion.ZLiving.Go.APIs.Common.Test.Mocks;
using Efusion.ZLiving.Go.UI.Auth.States;
using Efusion.ZLiving.Go.UI.Auth.ViewModels;
using Efusion.ZLiving.Go.UI.Common.Enums;
using Efusion.ZLiving.Go.UI.Common.States;
using Efusion.ZLiving.Go.UI.Common.Test.Mocks;
using System;
using Xunit;

namespace Efusion.ZLiving.Go.UI.Auth.Test.ViewModels
{
    public class LoginPageViewModelTest : IClassFixture<SettingsFixture>
    {
        public SettingsFixture Settings { get; set; }

        public LoginPageViewModelTest(SettingsFixture settings)
        {
            this.Settings = settings;
        }

        [Fact(DisplayName = "ForgotPassword - Failure State Test")]
        public void ForgotPassword_Failure_State_Test()
        {
            string mail = "email@email.com";
            MockSessionManager smMock = new MockSessionManager();
            MockAuthenticationService authServiceMock = new MockAuthenticationService();
            MockINavigationService navServiceMock = new MockINavigationService();

            LoginPageViewModel viewModel = new LoginPageViewModel(null, authServiceMock.Object, smMock.Object, navServiceMock.Object, Settings)
            {
                Email = mail,
            };

            Assert.Equal(mail, viewModel.Email);
            authServiceMock.MockSendForgotPassword(mail, new UserNotFoundException());
            viewModel.ForgotPassword();
            Assert.Equal(LoginState.LoginError, viewModel.LoginState);
            Assert.Equal("User not found in our system. Please enter a valid email.", viewModel.Message);

            authServiceMock.MockSendForgotPassword(mail, new Exception());
            viewModel.ForgotPassword();
            Assert.Equal(LoginState.UnexpectedException, viewModel.LoginState);
            Assert.Equal("An unexpected exception has occured. Plese try again later.", viewModel.Message);

            navServiceMock.VerifyNoOtherCalls();
            authServiceMock.VerifyAll();
        }

        [Fact(DisplayName = "ForgotPassword - Page State Test")]
        public void ForgotPassword_Page_State_Test()
        {
            string mail = "email@email.com";            
            MockSessionManager smMock = new MockSessionManager();            
            MockAuthenticationService authServiceMock = new MockAuthenticationService();
            authServiceMock.MockSendForgotPassword(mail);

            LoginPageViewModel viewModel = new LoginPageViewModel(null, authServiceMock.Object, smMock.Object, null, Settings)
            {
                Email = mail,                
            };

            Assert.NotNull(viewModel.ForgotPasswordCommand);
            Assert.NotNull(viewModel.SessionManager);            
            Assert.NotNull(viewModel.AuthenticationService);            
            Assert.Equal(mail, viewModel.Email);            

            viewModel.ForgotPassword();

            Assert.Equal(LoginState.ForgottenPasswordSent, viewModel.LoginState);
            Assert.Equal("We sent you an email with a reset password link. Please find it in your inbox, don't forget to check Junk folder as well.", viewModel.Message);
            
            authServiceMock.VerifyAll();            
        }

        [Fact(DisplayName = "Register - Navigate to Register Page Test")]
        public void Register_Navigation_Test()
        {            
            MockINavigationService navServiceMock = new MockINavigationService();
            MockSessionManager smMock = new MockSessionManager();
            LoginPageViewModel viewModel = new LoginPageViewModel(null, null,smMock.Object, navServiceMock.Object, Settings);
            navServiceMock.MockNavigateAsync("registration");
            Assert.NotNull(viewModel.RegistrationCommand);

            viewModel.Register();

            navServiceMock.VerifyAll();
        }

        [Fact(DisplayName = "Login - Not Authenticated State Test")]
        public void Login_NotAuthenticated_State_Test() {
            string mail = "email@email.com";
            string password = "password";
            MockSessionManager smMock = new MockSessionManager();
            MockINavigationService navServiceMock = new MockINavigationService();

            LoginPageViewModel viewModel = new LoginPageViewModel(null, null, smMock.Object, navServiceMock.Object, Settings)
            {
                Email = mail,
                Password = password
            };

            smMock.MockAuthenticationState(AuthenticationState.Anonymous);
            smMock.MockLogout();
            smMock.MockLogin(mail, password, new UnauthorizedAccessException());
            viewModel.Login();
            Assert.Equal(LoginState.LoginError, viewModel.LoginState);
            Assert.Equal("Invalid username or password", viewModel.Message);

            smMock.MockLogin(mail, password, new PendingActivationException());
            viewModel.Login();
            Assert.Equal(LoginState.ActivationPending, viewModel.LoginState);
            Assert.Equal("Email already in our system, but not activated. We re-sent you an email with activation link. Please find it in your inbox, don't forget to check Junk folder as well.", viewModel.Message);

            smMock.MockLogin(mail, password, new UserNotFoundException());
            viewModel.Login();
            Assert.Equal(LoginState.LoginError, viewModel.LoginState);
            Assert.Equal("Invalid username or password", viewModel.Message);

            smMock.MockLogin(mail, password, new Exception());
            viewModel.Login();
            Assert.Equal(LoginState.UnexpectedException, viewModel.LoginState);
            Assert.Equal("An unexpected exception has occured. Plese try again later.", viewModel.Message);

            navServiceMock.VerifyNoOtherCalls();
            smMock.VerifyAll();
        }
        
       [Fact(DisplayName = "Login - Authenticated State Test")]
        public void Login_Authenticated_State_Test()
        {
            string mail = "email@email.com";
            string password = "password";
            MockSessionManager smMock = new MockSessionManager();            
            MockINavigationService navServiceMock = new MockINavigationService();
            navServiceMock.MockNavigateAsync("menu");
            smMock.MockAuthenticationState(AuthenticationState.Authenticated);
            smMock.MockLogin(mail, password);
            
            LoginPageViewModel viewModel = new LoginPageViewModel(null, null, smMock.Object, navServiceMock.Object, Settings)
            {
                Email = mail,
                Password = password
            };

            Assert.NotNull(viewModel.LoginCommand);
            Assert.NotNull(viewModel.SessionManager);
            Assert.NotNull(viewModel.NavigationService);
            Assert.Equal((smMock.Object.AuthenticationState == AuthenticationState.Authenticated) ? LoginState.Loged : LoginState.NotLoged, viewModel.LoginState);
            Assert.Equal(mail, viewModel.Email);
            Assert.Equal(password, viewModel.Password);

            viewModel.Login();

            Assert.Equal(LoginState.Loged, viewModel.LoginState);

           navServiceMock.VerifyAll();
            smMock.VerifyAll();            
        }
    }
}
